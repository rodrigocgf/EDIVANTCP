// ntservice.h
//
// Definitions for CNTService
//

#ifndef _NTSERVICE_H_
#define _NTSERVICE_H_

#include "NTServMsg.h" // Event message ids
#define SERVICE_CONTROL_USER 128

/** 
 * NTService.cpp
 * Implementa��o da classe CNTService
 * Framework para construir aplica��es NT Services
 * Veja: Creating a Simple Windows NT Service in C++, Nigel Thompson
 * (MSDN, Technical Articles, Platform Articles, Networking Articles)
 * O fonte original foi modificado.
 * @author Nigel Thompson
 * @version 1.0.0.0
 **/
class CNTService
{
public:
   /**
    * Construtor
    * @param szServiceName Nome do servi�o.
    **/
   CNTService(const char* szServiceName);
   /**
    * Destrutor
    **/
   virtual ~CNTService();
public:
   /** 
    * Retorna a representa��o string do estado do servi�o
    * @param dwCurrentState Uma das constantes SERVICE_STOPPED,
    * SERVICE_START_PENDING, SERVICE_STOP_PENDING, SERVICE_RUNNING,
    * SERVICE_CONTINUE_PENDING, SERVICE_PAUSE_PENDING, SERVICE_PAUSED.
    * @return A representa��o string.
    **/
	CString csServiceState (DWORD dwCurrentState);
   /**
    * Configura o m�todo de in�cio do servi�o (SERVICE_DEMAND_START, etc.) 
    * @param dwStartType Tipo de in�cio do servi�o.
    **/
	void SetStartType (DWORD dwStartType);
   /**
    * Interpreta argumentos da linha de comando.
    * @param argc __argc.
    * @param argv __argv.
    **/
   BOOL ParseStandardArgs(int argc, char* argv[]);
   BOOL ParseStandardArgs( LPSTR args );
   /**
    * Indica se o servi�o j� est� instalado.
    * @return TRUE se o servi�o estiver instalado.
    **/
   BOOL IsInstalled();
   /**
    * Instala o servi�o.
    * @return TRUE se conseguir instalar o servi�o, ou se j� estiver instalado.
    **/
   BOOL Install();
   /**
    * Desinstala o servi�o.
    * @return TRUE se conseguir desinstalar o servi�o.
    **/
   BOOL Uninstall();
   /**
    * This function makes an entry into the application event log
    * @param wTYpe Specifies the type of event being logged. 
    * This parameter can be one of the following values: 
    * Value Meaning 
    * EVENTLOG_ERROR_TYPE Error event 
    * EVENTLOG_WARNING_TYPE Warning event 
    * EVENTLOG_INFORMATION_TYPE Information event 
    * EVENTLOG_AUDIT_SUCCESS Success Audit event 
    * EVENTLOG_AUDIT_FAILURE Failure Audit event 
    * @param dwId Event identifier (see NTServMsg.h)
    * @param pszS1 First insertion string (%1) - can be omitted
    * @param pszS2 Second insertion string (%2) - can be omitted
    * @param pszS3 Third insertion string (%3) - can be omitted
    * @see ::ReportEvent, ::RegisterEventSource, Message Compiler (MC.EXE)
    * Exemplo: LogEvent (EVENTLOG_INFORMATION_TYPE, EVMSG_REMOVED, m_szServiceName);
    * imprime no Event Log, "The S_TM Service was removed".
    */
   void LogEvent(WORD wType, DWORD dwID,
      const char* pszS1 = NULL,
      const char* pszS2 = NULL,
      const char* pszS3 = NULL);
   /**
    * Inicia o servi�o.
    * @return TRUE Se o servi�o foi iniciado.
    **/
   BOOL _StartService();
   /**
    * Muda o status do servi�o (SERVICE_STOPPED, SERVICE_RUNNING etc.)
    * @param dwState Status do servi�o.
    **/
   void SetStatus(DWORD dwState);
   /**
    * Inicializa��o do servi�o.
    * @return TRUE se conseguiu inicializar o servi�o.
    **/
   BOOL Initialize();
   /**
    * Rotina principal do servi�o.
    **/
   virtual void Run();
   /**
    * Chamada quando da inicializa��o do servi�o.
    * @return TRUE se conseguir inicializar o servi�o.
    **/
   virtual BOOL OnInit();
   /**
    * Chamada quando do encerramento do servi�o.
    **/
   virtual void OnStop();
   /**
    * Chamada quando o servi�o recebe INTERROGATE.
    **/
   virtual void OnInterrogate();
   /**
    * Chamada quando o servi�o recebe PAUSE.
    **/
   virtual void OnPause();
   /**
    * Chamada quando o servi�o recebe CONTINUE.
    **/
   virtual void OnContinue();
   /**
    * Chamada quando o servi�o recebe SHUTDOWN.
    **/
   virtual void OnShutdown();
   /**
    * Chamada quando o servi�o recebe um controle definido pelo usu�rio.
    * @param dwOpcode C�digo do controle definido pelo usu�rio.
    * @return TRUE se OK.
    **/
   virtual BOOL OnUserControl(DWORD dwOpcode);
   /**
    * Mostra mensagem de depura��o via ::OutputDebugString, como printf
    * @param pszFormat Format estilo printf
    * @param ... Par�metros estilo printf
    * @see ::OutputDebugString, printf
    **/
   void DebugMsg(const char* pszFormat, ...);

   //-- Modifica par�metros de instala��o
   /** 
    * Muda o nome do servi�o (antes de efetivar a instala��o)
    * @param lpszServiceName Nome do servi�o 
    */
   void SetServiceName (LPCTSTR lpszServiceName);
   /** 
    * Muda o nome que � mostrado (display name) do servi�o 
    * (antes de efetivar a instala��o)
    * @param lpszDisplayName Nome do servi�o 
    **/
   void SetDisplayName (LPCTSTR lpszDisplayName);
   /** 
    * Muda o acesso desejado do servi�o (antes de efetivar a instala��o)
    * @param dwDesiredAccess Acesso desejado 
    **/
   void SetDesiredAccess (DWORD dwDesiredAccess);
   /** 
    * Muda o tipo do servi�o (antes de efetivar a instala��o)
    * @param dwServiceType Tipo do servi�o 
    **/
   void SetServiceType (DWORD dwServiceType);
   /** Muda o controle de erros (antes de efetivar a instala��o)
    * @param dwErrorControl */
   void SetErrorControl (DWORD dwErrorControl);
   /** 
    * Muda o nome do usu�rio
    * @param lpszAccount Nome do usu�rio 
    **/
   void SetAccount (LPCTSTR lpszAccount);
   /** 
    * Muda a senha do usu�rio
    * @param lpszPassword Senha do usu�rio 
    **/
   void SetPassword (LPCTSTR lpszPassword);
   /** Muda as depend�ncias do servi�o (servi�os que devem estar no
    * ar antes deste servi�o)
    * @param lpszDep1 Nome da 1� depend�ncia, opcional
    * @param lpszDep2 Nome da 2� depend�ncia, opcional
    * @param lpszDep3 Nome da 3� depend�ncia, opcional
    * @param lpszDep4 Nome da 4� depend�ncia, opcional
    **/
   void SetDependencies (LPCTSTR lpszDep1 = NULL, LPCTSTR lpszDep2 = NULL, LPCTSTR lpszDep3 = NULL, LPCTSTR lpszDep4 = NULL);
   
   // static member functions
   /**
    * Controlador do servi�o
    * @param dwArgc __argc
    * @param lpszArgv __argv
    **/
   static void WINAPI ServiceMain(DWORD dwArgc, LPTSTR* lpszArgv);
   /**
    * Handler do servi�o
    * @param dwOpcode C�digo do controle do servi�o.
    **/
   static void WINAPI Handler(DWORD dwOpcode);
   
   // data members
   /** Nome do servi�o */
   char m_szServiceName[64];
   /** Vers�o (major) */
   int m_iMajorVersion;
   /** Vers�o (minor) */
   int m_iMinorVersion;
   /** Handle do servi�o */
   SERVICE_STATUS_HANDLE m_hServiceStatus;
   /** Status do servi�o */
   SERVICE_STATUS m_Status;
   /** Indica se o servi�o est� no ar */
   BOOL m_bIsRunning;
   /** Indica o tipo de in�cio do servi�o */
	DWORD m_dwStartType;
   /** Depend�ncias (separadas por \0 e terminadas por \0\0) */
	TCHAR m_szDependencies [1024];
   /** Password */
	CString m_csPassword;
   /** usu�rio */
	CString m_csAccount;
   /** Tipo de controle de erros */
	DWORD m_dwErrorControl;
   /** Tipo do servi�o */
	DWORD m_dwServiceType;
   /** Tipo de acesso ao servi�o */
	DWORD m_dwDesiredAccess;
   /** Nome do servi�o (display name) */
	CString m_csDisplayName;

   // static data
   static CNTService* m_pThis; // nasty hack to get object ptr
   
private:
   /** Handle para event log */
   HANDLE m_hEventSource;

   LPCTSTR FindOneOf(LPCTSTR p1, LPCTSTR p2);
   
};

#endif // _NTSERVICE_H_
