#ifndef _SOCKET_
#define _SOCKET_

#include "StdAfx.h"

#define ERR_INT_DESCONEXAO     1
#define ERR_INT_MALLOC         2
#define ERR_INT_TERMINO_PROG   3

#define MAX_TAM_PACOTE              1500


//
// Constantes para a fun��o shutdown()
//
#ifndef SD_RECEIVE
#define SD_RECEIVE                  0x00
#endif
#ifndef SD_SEND
#define SD_SEND                     0x01
#endif
#ifndef SD_BOTH
#define SD_BOTH                     0x02
#endif

#define TIMEOUT_SENDRECEIVE         60 // Segundos
#define MAX_ENDERECO                64

#define EVENT_FINALIZA				"FinalizaVersao50"

#define SUCESSO_NA_OPERACAO        "000"
#define ERRO_NA_OPERACAO	        "001" 

#pragma pack(1)
typedef struct
{
   char Tamanho[15];
   char sBuffer[MAX_TAM_PACOTE];
} SBlocoArquivo;
#pragma pack()

class CTcpSocket
{
public:
   CTcpSocket();
   ~CTcpSocket();
   BOOL Create(UINT nPort = 0, int nType = SOCK_STREAM);
   BOOL CreateToConnect(int nType = SOCK_STREAM);
   void Config();
   BOOL WaitToSend(int nTimeOut = 0);
   virtual BOOL WaitToReceive(int nTimeOut = 0);
   int Send(const char *Buffer, int Tamanho);
   int Receive(char *Buffer, int Tamanho);
   SOCKET Accept(LPSTR Address);
   BOOL Listen(int nBackLog = 5);
   BOOL Connect(LPCSTR sAddress, UINT nPort);
   BOOL IOCtl(long lCommand, DWORD* lpArgument);
   BOOL SetSockOpt(int nOption, const void* lpValue, int nLen, int nLevel = SOL_SOCKET);
   BOOL GetSockOpt(int nOption, void* lpValue, int* nLen, int nLevel = SOL_SOCKET);
   void Close();
   BOOL Attach(SOCKET nSocket);
   int ObtemUltimoErro();
   operator SOCKET();
   BOOL IsSocketConnected();
   int ObtemNumeroSocket();
   BOOL RecebeArquivo(LPTSTR lpArqDestino, LONG lTamArq);
	int ReceiveCrypt(char *Buffer, int Tamanho);
	int SendCrypt(const char *Buffer, int Tamanho);
   char mClientAddress[MAX_ENDERECO];

private:
   SOCKET mSocket;
   bool mIsSocketOk;
   char mHostAddress[MAX_ENDERECO];
   int mError;
};

#endif // _SOCKET_