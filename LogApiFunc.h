#ifndef _CLOGAPIFUNC_H_
#define _CLOGAPIFUNC_H_

#include "StdAfx.h"

class CLogApiFunc  
{
public:
	void logtrc(const char *msg, ... );
	void logtrc(long log_level, const char *msg,...);
	long getlevel();
	long loglevel;
	void gravalog(const char *msg, char tipo );
	void logerr(const char *msg, ... );
	void logsys(const char *msg, ...);

	void hexdump(FILE *f, char * buff, int len);
	void logdump(char *title, char *buf, int siz);
	void printlin(FILE *f, char *buff, int offs, int len, int i_tambuff);

   CLogApiFunc();
	virtual ~CLogApiFunc();
	char m_dir[_MAX_PATH + 1];
	char m_file[_MAX_PATH + 1];
};

#endif
