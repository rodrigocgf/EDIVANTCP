// EDIVANTCP.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "EDIVANTCP.h"

#include "MainFrm.h"
#include "LogApiFunc.h"
#include "SrvService.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEDIVANTCPApp

BEGIN_MESSAGE_MAP(CEDIVANTCPApp, CWinApp)
	//{{AFX_MSG_MAP(CEDIVANTCPApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEDIVANTCPApp construction

CEDIVANTCPApp::CEDIVANTCPApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	pFrame = new CMainFrame;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CEDIVANTCPApp object

CEDIVANTCPApp theApp;
CLogApiFunc logapi;
CSrvService theSrv;

/////////////////////////////////////////////////////////////////////////////
// CEDIVANTCPApp initialization

BOOL CEDIVANTCPApp::InitInstance()
{
	// Inicializa as rotinas de servicos do NT
	theSrv.m_bService = TRUE;
	if ( theSrv.ParseStandardArgs( m_lpCmdLine ) )
		return FALSE;


	// Testa se a aplicacao vai executar como Servico ou uma aplicacao normal.
	TCHAR   szTokens[] = _T("-/");
	LPCTSTR lpszToken  = FindOneOf(m_lpCmdLine, szTokens);
	while (lpszToken != NULL)
	{
		if (lstrcmpi(lpszToken, _T("Local"))==0 || 
			lstrcmpi(lpszToken, _T("l"))==0)
		{
			theSrv.m_bService = FALSE;
			break;
		}
		lpszToken = FindOneOf(lpszToken, szTokens);
	}

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.
	if (!AfxSocketInit())
	{
		//AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		AfxMessageBox("Falha na inicializa��o AfxSocketInit().");
		return FALSE;
	}

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));


	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object.
	m_pMainWnd = pFrame;	

	if ( theSrv.m_bService == FALSE ) {
		IniciaAplicacao();
	} else {
		theSrv._StartService();
	}

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CEDIVANTCPApp message handlers


BOOL CEDIVANTCPApp::IniciaAplicacao()
{
	logapi.logtrc(LOG_LEVEL_DEFAULT,"\r\n\r\n");
	logapi.logtrc(LOG_LEVEL_DEFAULT,"================================" );
	logapi.logtrc(LOG_LEVEL_DEFAULT,"==       I N � C I O          ==" );
	logapi.logtrc(LOG_LEVEL_DEFAULT,"================================" );
	logapi.logtrc(LOG_LEVEL_DEFAULT, "*** Iniciando EDIVANTCP (NTService)  ***" );

	
	pFrame->LoadFrame(IDR_MAINFRAME,
		WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,
		NULL);

	pFrame->ShowWindow(SW_SHOW);
	//pFrame->ShowWindow(SW_HIDE);	

	pFrame->UpdateWindow();
	//pFrame->InicializaSocket7COMm();
	//  Inicializa o socket de LISTEN com o roteador
	pFrame->IniciaThreadListen();
	

	return TRUE;
}

BOOL CEDIVANTCPApp::FinalizaAplicacao()
{
	//if ( pFrame != NULL )
	//	pFrame->FinalizaSocket7COMm();

	logapi.logtrc(LOG_LEVEL_DEFAULT,"================================" );
	logapi.logtrc(LOG_LEVEL_DEFAULT,"==          F I M             ==" );
	logapi.logtrc(LOG_LEVEL_DEFAULT,"================================" );

	return TRUE;
}


LPCTSTR CEDIVANTCPApp::FindOneOf(LPCTSTR p1, LPCTSTR p2)
{
	while (*p1 != NULL)
	{
		LPCTSTR p = p2;
		while (*p != NULL)
		{
			if (*p1 == *p++)
				return p1+1;
		}
		p1++;
	}
	return NULL;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CEDIVANTCPApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CEDIVANTCPApp message handlers

