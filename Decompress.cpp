#include "StdAfx.h"
#include "Decompress.h"

CDecompress::CDecompress(bool permiteCompressao)
{
   Reset();
	mPermiteCompressao = permiteCompressao;
}

CDecompress::~CDecompress()
{
}

unsigned char* CDecompress::DecodeString(BYTE *Buffer, WORD Codigo)
{
   register int  i=0;                                                           

   while ( Codigo > 255 )                                                        
   {                                                                            
      *Buffer++ = m_CaracSfx[Codigo];
      Codigo    = m_CodPfx[Codigo];
      if ( i++ >= DE_STACK_SIZE )                                               
      {                                                                         
         m_Erro = LZC_ERRO_DESCOMPRESSAO;                                         
         return (BYTE *)Buffer;
      }                                                                         
   }                                                                            
   *Buffer = (BYTE)Codigo;

   return (BYTE *)Buffer;

}

WORD CDecompress::InputCode()
{
   WORD Rval;                                                           
   long ch=0;
                                                                                
                                                                                
   while ( m_InpBitCount <= (SIZEBITS(m_InputBitBuffer) - SIZEBITS(BYTE)) )
   {                                                                            
      if ( (ch = Read()) == LZC_EOF )                                          
         break;                                                                 
      m_InputBitBuffer |= ch << ((SIZEBITS(m_InputBitBuffer) - SIZEBITS(BYTE)) - m_InpBitCount);
      m_InpBitCount += SIZEBITS(BYTE);
   }                                                                            
                                                                                
   if (ch == LZC_EOF && m_InpBitCount < m_NumBits)
      return TERMINATOR;                                                        
   Rval = (int) ( m_InputBitBuffer >> (SIZEBITS(m_InputBitBuffer) - m_NumBits) );                           
   m_InputBitBuffer <<= m_NumBits;                                                  
   m_InpBitCount     -= m_NumBits;                                                  

   // Protecao para evitar erros (invasao de memoria e outros) quando estiver do
   //  dados errados. Pode ser retirado para agilizar a descompressao quando naa
   //  possibilidade                                                            
   if ( Rval > m_NextCode )                                                       
   {                                                                            
      m_Erro = LZC_ERRO_DESCOMPRESSAO;                                            
      Rval = TERMINATOR;                                                        
   }                                                                            
   return (WORD)Rval;                                                   

}

int CDecompress::Read()
{
   if (m_iPosBuffer >= m_nBytes)
      return LZC_EOF;

   return m_pBufferIn[m_iPosBuffer++];
}

int CDecompress::PutcNcr(WORD c)
{
   int Erro;
                                                                                
    switch ( m_State )                                                            
    {                                                                           
          case NOHIST:                                                          
               if ( c==DLE )                                                    
                  m_State = INREP;                                                
               else                                                             
                  if ((Erro=Write((BYTE)(m_Lastc=c))) < 0)
                     return Erro;
               break;                                                           
                                                                                
          case INREP:                                                           
               if ( c )                                                         
                  while ( --c )
                  {
                     if ((Erro=Write((BYTE)m_Lastc)) < 0)
                        return Erro;
                  }
               else                                                             
                  if ((Erro=Write( DLE )) < 0)
                     return Erro;
               m_State = NOHIST;                                                  
               break;                                                           
    }                                                                           
                                                                                
    return 0;                                                                   

}

int CDecompress::Decompress(Upload *up, int idArq, BYTE *BufferIn, DWORD nBytes)
{
	unsigned int NewCode;                                                 
	unsigned char *String;                                                 
	int Erro;
	
	m_pBufferIn = BufferIn;
	m_nBytes = nBytes;
	m_iPosBuffer = 0;
	m_BytesGravados = 0;
	m_nBytesInIOBuffer = 0;
	u = up;
	idarq = idArq;
	
	if(!mPermiteCompressao)
		return NoDecompress();
	
	while(1)                                                                    
	{
		if ( (NewCode = InputCode() ) == TERMINATOR)
			break;

		if(m_ClearFlag)                                                         
		{                                                                      
			m_ClearFlag = FALSE;
			m_OldCode   = NewCode;                                                			
			m_Char      = m_OldCode;                                                
			
			if( (Erro = PutcNcr(m_OldCode & 0xff) ) < 0)
				return Erro;

			continue;                                                           
		}   
		
		if ( NewCode == CLEAR_TABLE ) {                                                                      
			m_ClearFlag = TRUE;
			m_NumBits   = INIT_BITS;                                              
			m_NextCode  = FIRST_CODE;                                             
			m_MaxCode   = VAL_MAX(m_NumBits);                                       
			continue;                                                           
		}

		if ( NewCode >= m_NextCode) {                                                                      
			*m_DeStack = (char)m_Char;                                               

			String   = (unsigned char *)DecodeString( (unsigned char *)(m_DeStack +1),m_OldCode);
			if ( m_Erro )                                                         
				goto Fim;                                                        
		} else {                                                                      
			String   = (unsigned char *)DecodeString( (unsigned char *)m_DeStack,NewCode);

			if ( m_Erro )                                                         
				goto Fim;                                                        
		}   
		
		m_Char = *String;                                                         
		
		while ( String >= (unsigned char *)m_DeStack )                           
		{                                                                      
			if ((Erro=PutcNcr(*String-- & 0xff)) < 0)
				return Erro;
		}
		
		if ( m_NextCode <= (unsigned int)m_MaxCode ) {                                                                      
			m_CodPfx[m_NextCode]     = m_OldCode;
			m_CaracSfx[m_NextCode++] = (unsigned char)m_Char;
			if ( m_NextCode == (unsigned int)m_MaxCode && m_NumBits < MAX_BITS ) {                                                                   
				m_MaxCode = VAL_MAX(++m_NumBits);                                    
			}                                                                   
		}
		
		m_OldCode = NewCode;                                                     
	}                                                                            
	
Fim:                                                                            
	m_InputBitBuffer = 0L;                                                         
	m_InpBitCount = 0;                                                             
	
	if ( !m_Erro ) {
		// Se nao houve erro na descompressao, flush buffers
		if ( m_Erro = FlushIOBuffer() )
			return m_Erro;

		return m_BytesGravados;
	}

	return m_Erro;                                                                 
}

int CDecompress::Write(BYTE Codigo)
{

	m_BytesGravados++;

	m_IOBuffer[m_nBytesInIOBuffer++] = Codigo;
	if (m_nBytesInIOBuffer >= TAM_IO_BUFFER)
		return FlushIOBuffer();
   return 0;
}

int CDecompress::FlushIOBuffer()
{
	if ( m_nBytesInIOBuffer ) {
		if ( u->GravarBloco(idarq, (char *)m_IOBuffer, m_nBytesInIOBuffer) )
			return LZC_ERRO_GRAVACAO_ARQ;

		m_nBytesInIOBuffer = 0;
	}

	return 0;
}

void CDecompress::Reset()
{
   m_NumBits         = INIT_BITS;                                                 
   m_MaxCode         = VAL_MAX(INIT_BITS);                                        
   m_NextCode        = FIRST_CODE;                                                
   m_ClearFlag       = TRUE;                                                         
   m_State           = 0;                                                         
   m_InputBitBuffer  = 0;                                                         
   m_InpBitCount     = 0;                                                         
   m_Erro            = 0;                                                         
}

int CDecompress::NoDecompress()
{
	if ( u->GravarBloco(idarq, (char *)m_pBufferIn, m_nBytes) )
		return LZC_ERRO_GRAVACAO_ARQ;

   return m_nBytes;
}
