#ifndef _UTIL_H
#define _UTIL_H

#include "StdAfx.h"

#define CHAVE "\x45\x09\x59\x38\x12\xfe\xf9\xe6\xd0"
#define TAM_CHAVE strlen(CHAVE)
#define UTIL_TEMPFILE_MAX_TRYES  100
//#define MV7_NAO_RESTART

#define CTR_TAM_NOME_ORIGINAL       12
#define CTR_TAM_FILE_SIZE           10 // Aumentado de 7 p/ 10 JT 21.11.98
#define CTR_TAM_NOME_MV7            12
#define CTR_TAM_DATA                8
#define CTR_TAM_HORA                6
#define CTR_TAM_CXP_DESTINO         8

// woki 26.08.04 20:10
// - estruturas que viajam pela rede com pragma pack 1
// - mudan�a de BYTE p/ char p/ facilitar manipula��o posterior
#pragma pack(1)
typedef struct
{
   char sNomeOriginal[CTR_TAM_NOME_ORIGINAL];
   char sTamanho[CTR_TAM_FILE_SIZE];
   char sNomeMV7[CTR_TAM_NOME_MV7];
   char sData[CTR_TAM_DATA];
   char sHora[CTR_TAM_HORA];
   char sDestino[CTR_TAM_CXP_DESTINO];
} CTRSTRUCT;
#pragma pack()

// woki 26.08.04 20:10
typedef struct 
{
   CTRSTRUCT CtrStruct;
   char sNomeCentral[20 + 1];
	int idArquivo;
} CTRSTRUCTINFO;


void Encripta(char *Destino, const char *Origem, WORD nBytes);
void StringNCopy(char *Destino, char *Origem, DWORD nBytes);
int  StringToInt(char *String, WORD nBytes);
long StringToLong(char *String, WORD nBytes);
void RTrim(char *String);
void WINAPI ProcessMessages(void);
BOOL CriaTempFileName(char *sFileName,char *sPath);
int ByteArrayToHexString ( const BYTE *sByte, BYTE *sHexa, int lenori_ );
int HexStringToByteArray( BYTE *sHexa, BYTE *sByte, int oriLen_, int destLen_ );
void GravaLinha(HANDLE hArq, BYTE *sBuffer );
void GeraLog(char *Format, ... );

#endif // _UTIL_H