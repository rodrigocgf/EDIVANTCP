// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__C03DB7A1_97B7_477D_93E2_732C2820FA49__INCLUDED_)
#define AFX_STDAFX_H__C03DB7A1_97B7_477D_93E2_732C2820FA49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxsock.h>		// MFC socket extensions
#include <afxtempl.h>
#include <winsvc.h>
#include <atlconv.h>

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cstdarg>
#include <cctype>
#include <ctime>
#include <cerrno>

#include <direct.h>
#include <io.h>								// [A002]
#include <sys/stat.h>						// [A002]
#include <sys/types.h>
#include <fcntl.h>							// [A003]

using namespace std;

#define LOG_LEVEL_DEFAULT	3


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__C03DB7A1_97B7_477D_93E2_732C2820FA49__INCLUDED_)
