#include "StdAfx.h"
#include "Rapi.h"
#include "erros.h"

typedef long key_t;
typedef unsigned short ushort;

// Opera��es da RAPI
#define OPEN    0
#define READ    1
#define WRITE   2
#define CLOSE   3
#define LSEEK   4

#define LINK    5
#define UNLINK  6

#define STAT        7
#define CRIADIR    16
#define FTRUNCATE  31

#define EXEC_PGM   29 

#define CLIENT     1   // Host ser� cliente
#define SERVER     2   // Host � o pr�prio servidor



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Rapi::Rapi(char *Servidor, int porta)
{
	SOCKET soc;
	struct sockaddr_in sa;
	struct hostent *serv;
	struct WSAData vWSAData;
	
	Erro = 0;
	
	soc1 = 0;
	if(Servidor[0] == '\0')
	{
		host = SERVER;
	}
	else
	{
		host = CLIENT;
		
		if(WSAStartup(MAKEWORD(2, 2), &vWSAData) != 0)
		{
			Erro = EDI_MSG_WSASTARTUP;
			return;
		}
		
		memset((char *) &sa, 0, sizeof(sa));
		if((sa.sin_addr.s_addr = inet_addr(Servidor)) == INADDR_NONE)
		{
			serv = (struct hostent *)gethostbyname(Servidor);
			if(serv == 0)
			{
				Erro = EDI_MSG_GETHOSTBYNAME;
				return;
			}
			memcpy((char *)&sa.sin_addr, (char *)serv->h_addr_list[0], serv->h_length);
		}
		sa.sin_family = AF_INET;
		sa.sin_port = htons(porta);
		soc = socket(AF_INET, SOCK_STREAM, 0);
		if(soc < 0)
		{
			Erro = EDI_MSG_SOCKET;
			return;
		}
		if(connect(soc, (struct sockaddr *) &sa, sizeof(sa)) != 0) 
			Erro = EDI_MSG_CONNECT;
		soc1 = soc;
		return;
	}
}

Rapi::~Rapi()
{
	if(soc1)
	{
		shutdown(soc1, 2);  
		closesocket(soc1);
		soc1 = 0;
	}
}

int Rapi::open(char *fn, int modo, int atrib)
{
	if(host == SERVER)
	{
		ret.rc = _open(fn, modo, atrib);
		return ret.rc;
	}
	memset(&head1, 0, sizeof(head1));
	head1.cod = (char) OPEN;
	head1.tam2 = (u_long) modo;
	head1.fd = (u_long) atrib;
	head1.tam1 = (char) (strlen(fn) + 1);
	strcpy(head1.buf, fn);
	
	send_rapi(soc1, (char *) &head1,
		sizeof(head1) - sizeof(head1.buf) + (int) head1.tam1, 0);
	g_read();
	return ret.rc;
}

//-------------------------------------------------------------------------------------------

int Rapi::read(int fd, void *buf, unsigned siz)
{
   if(host == SERVER)
   {
		ret.rc = _read(fd, buf, siz);
		return ret.rc;
   }
   head1.cod = READ;
   head1.tam2 = siz;
   head1.fd = fd;
   send_rapi(soc1, (char*) &head1, sizeof(head1) - sizeof(head1.buf), 0);
   g_read();
   memcpy(buf, ret.buf, ret.rc_siz);
   return ret.rc;
}
//-------------------------------------------------------------------------------------------
int Rapi::write(int fd, char *buf, unsigned siz)
{
	if ( host == SERVER ) {
		ret.rc = _write(fd, buf, (size_t) siz);
		return ret.rc;
	}
	memset(&head1, 0, sizeof(head1));
	head1.cod = (char) WRITE;
	head1.tam2 = (u_long) siz;
	head1.fd = (u_long) fd;
	memcpy(head1.buf, buf, siz);
	send_rapi(soc1, (char *) &head1, sizeof(head1) - sizeof(head1.buf) + siz, 0);          
	g_read();
	return ret.rc;
}

//-------------------------------------------------------------------------------------------
int Rapi::close(int fd)
{
   int i;

   if(host == SERVER)
   {
		ret.rc = _close(fd);
		return ret.rc;
   }
   memset(&head1, 0, sizeof(head1));
   head1.cod = (char) CLOSE;
   head1.fd = (u_long) fd;
   i = send_rapi(soc1, (char *) &head1, sizeof(head1) - sizeof(head1.buf), 0);
   g_read();
   return ret.rc;
}
//-------------------------------------------------------------------------------------------
long Rapi::lseek(int fd, long ptr, int modo)
{
   if(host == SERVER)
   {
		ret.rc = _lseek(fd, ptr, modo);
		return ret.rc;
   }
   memset(&head1, 0, sizeof(head1));
   head1.cod = (char) LSEEK;
   head1.tam2 = (u_long) ptr;
   head1.fd = (u_long) fd;
   head1.tam1 = (char) modo;      
   send_rapi(soc1, (char *) &head1, sizeof(head1) - sizeof(head1.buf), 0);
   g_read();
   return ret.rc;
}
//-------------------------------------------------------------------------------------------
long Rapi::size(char *fn, long *size)
{
   int i;

   if(host == SERVER)
   {
		struct _stat st;
		ret.rc = _stat(fn, &st);
		*size = st.st_size;
		return ret.rc;
   }
	else
	{                  // a Rapi retorna uma 
		struct statMin
		{   // estrutura stat simplificada
	      long st_size;     
			time_t st_atime;    /* time of last access */
			time_t st_mtime;    /* time of last modification */
			time_t st_ctime;    /* time of last change */
	   } info;

	   head1.cod = STAT;
	   head1.tam1 = strlen(fn)+1;
	   strcpy(head1.buf, fn);
	   i = send_rapi(soc1, (char *) &head1,
			sizeof(head1) - sizeof(head1.buf) + head1.tam1, 0);
	   g_read();
	   memcpy(&info, ret.buf, ret.rc_siz);

	   *size = info.st_size;
	   return ret.rc;
   }
}

//-------------------------------------------------------------------------------------------
int Rapi::execpgm(char *pgm, char *pgm_display, char *cmd, char *fn_in,
						char *fn_out, char *TxtBin)
{
   if(host == SERVER)
   {
	   // No windows � executa prg (por enquanto)
	   return 0;
   }
   int tam_tot;
   head1.cod = EXEC_PGM;
   strcpy(head1.buf, pgm);
   tam_tot = strlen(pgm) + 1;

   strcpy(&head1.buf[tam_tot], pgm_display);
   tam_tot += strlen(pgm_display) + 1;

   strcpy(&head1.buf[tam_tot], cmd);
   tam_tot += strlen(cmd) + 1;

   strcpy(&head1.buf[tam_tot], fn_in);
   tam_tot += strlen(fn_in) + 1;

   strcpy(&head1.buf[tam_tot], fn_out);
   tam_tot += strlen(fn_out) + 1;

   strcpy(&head1.buf[tam_tot], TxtBin);
   tam_tot += strlen(TxtBin) + 1;

   head1.tam2 = tam_tot;

   send_rapi(soc1, (char *) &head1, sizeof(head1) - sizeof(head1.buf) + head1.tam2, 0);
   g_read();
   return ret.rc;
}
//-------------------------------------------------------------------------------------------

int Rapi::g_read()
{
	char *p;
	int in, n;

	p = (char *) &ret;

	for(in = 0, n = 0; in < sizeof(long) * 2; in += n)
	{
		n = recv(soc1, p + in, sizeof(long) * 2 - in, 0);
		if(n < 1)
		{
			ret.rc = -1;
			return 0;
		}
	}
	p = ret.buf;

	for(in = 0, n = 0; in < ret.rc_siz; in += n)  
	{
		n = recv(soc1, p + in, ret.rc_siz - in, 0);
		if(n < 1)
		{
			ret.rc = -1;
			return 0;
		}
	}
	return (0);
}

//-------------------------------------------------------------------------------------------
int Rapi::send_rapi(unsigned int s, const char *buf, int len, int flags)
{
	return send(s, buf, len, flags);
}


unsigned long Rapi::swap_long(unsigned long lptr)
{
    char *ptr;
    unsigned long lval = 0;
    char *ptr_val;

    ptr = (char *) &lptr;
    ptr_val = (char *) &lval + sizeof(long) - 1;
    for(int i = 0; i < sizeof(long); i++)
		*(ptr_val--) = *(ptr++);
    return lval;
}

int Rapi::mkdirs(char *path, int atrib)
{
   if(host == SERVER)
   {
       //TODO:
   }
   memset(&head1, 0, sizeof(head1));
   head1.cod = (char) CRIADIR;
   head1.tam1 = (char) strlen(path) + 1;
   head1.fd = (u_long) atrib;
   strcpy(head1.buf, path);
   send_rapi(soc1, (char *) &head1, sizeof(head1) - sizeof(head1.buf) + head1.tam1, 0);
   g_read();
   return (!ret.rc);
}

int Rapi::ftruncate(int fd, int size)
{
   if(host == SERVER)
   {
      return _chsize(fd, size);
   }
   memset(&head1, 0, sizeof(head1));
   head1.cod = (char) FTRUNCATE;
   head1.tam2 = (u_long) size;
   head1.fd = (u_long) fd;

   send_rapi(soc1, (char *) &head1, sizeof(head1) - sizeof(head1.buf), 0);
   g_read();
   return ret.rc;
}
