// EDIVANTCP.h : main header file for the EDIVANTCP application
//

#if !defined(AFX_EDIVANTCP_H__D90E6AA2_BA73_43AC_AD87_DDE1D4844F40__INCLUDED_)
#define AFX_EDIVANTCP_H__D90E6AA2_BA73_43AC_AD87_DDE1D4844F40__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "MainFrm.h"

/////////////////////////////////////////////////////////////////////////////
// CEDIVANTCPApp:
// See EDIVANTCP.cpp for the implementation of this class
//

class CEDIVANTCPApp : public CWinApp
{
public:
	CEDIVANTCPApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEDIVANTCPApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	CMainFrame *pFrame;

	BOOL IniciaAplicacao();
	BOOL FinalizaAplicacao();
	LPCTSTR FindOneOf(LPCTSTR p1, LPCTSTR p2);
public:
	//{{AFX_MSG(CEDIVANTCPApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDIVANTCP_H__D90E6AA2_BA73_43AC_AD87_DDE1D4844F40__INCLUDED_)
