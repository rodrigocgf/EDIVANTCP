#include "StdAfx.h"
#include "Upload.h"
#include "erros.h"
#include "Arquivo.h"
#include "LogApiFunc.h"

extern CLogApiFunc logapi;

Upload::Upload(CConfig* cfg) : Arquivo(cfg)
{
}

Upload::~Upload()
{
}

int Upload::getStatus(Parametros* Parm)
{
	int retVal = EDI_MSG_OK;

	TransmissaoNormal = strcmp(Parm->ModoOperacao, "Nor") == 0;
	Retransmissao = strcmp(Parm->ModoOperacao, "Ret") == 0;
	if (Parm->NomeDestino[0])
		strcpy(CodClienteDestino,Parm->NomeDestino);
	else
		memset(CodClienteDestino,0,sizeof(CodClienteDestino));
	
	if(jaTransmitido(Parm->CodDominio, Parm->CodCliente,
		Parm->CodFormato,	Parm->NomeOrigem,	Parm->DataHoraOrigem))
	{
		logapi.logtrc(">>Upload::getStatus jaTransmitido");
		// No MV7 sempre permite retransmissao
		if(Processado() || TotalmenteTransmitido())  
		{
			logapi.logtrc(">>Upload:: Retransmissao");
			retVal = ReCriar(idArquivo,
				Parm->CodDominio, Parm->CodCliente, 
				Parm->CodUsuario, Parm->CodFormato, 
				Parm->NomeOrigem, Parm->DataHoraOrigem,
				Parm->TamanhoOrigem,
				Parm->ModoOperacao, 'U');
		}
		else
		{
			logapi.logtrc(">>Upload::getStatus Restart");
			// Verificando consistencia com tamanho disco/bd
			if(TamanhoDisco() == -1)
			{
				retVal =  EDI_MSG_ERRO_TAMANHO_BD_DIFERE_FISICO;
			}
		}
	}
	else if(TransmissaoNormal) // Nao foi transmitido ainda
	{
		retVal = Criar(Parm->CodDominio, Parm->CodCliente, 
			Parm->CodUsuario, Parm->CodFormato, 
			Parm->NomeOrigem, Parm->DataHoraOrigem,
			Parm->TamanhoOrigem,
			Parm->ModoOperacao, 'U');
	}
	else
	{
		retVal =  EDI_MSG_ERRO_ARQUIVO_NAO_CADASTRADO;
	}
	return retVal;
}

int Upload::GravarBloco(int idArquivo, char *Bloco, int Size)
{
	long pos = 0, erro = 0; // posi��o no bloco
    
	if( Existe(idArquivo, 'U') ) {
		AbreArquivo(true, false);

		if ( !Aberto() )
			return EDI_MSG_ERRO_ABRINDO_ARQ_FISICO;
		
		PosicionaArquivo();
		
		erro = Gravar(Bloco, Size);
		
		if ( !erro ) {
			FechaArquivo(false);
			erro = salvarEstado();
		} else {
			FechaArquivo(false);
		}
	} else {
		erro = EDI_MSG_ERRO_ARQUIVO_NAO_CADASTRADO;
	}

	return erro;
}

int Upload::EncerraUpload()
{
	return Consistir();
/*	
	int i_ret;

	i_ret = Consistir();
	if ( i_ret == 0 ) { // StatusArquivo = 'T'
		salvarEstado();
	}

	return i_ret;
*/	
}