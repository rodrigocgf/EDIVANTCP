#ifndef _RAPI_H_
#define _RAPI_H_

#include "StdAfx.h"

class Rapi  
{
public:
	int mkdirs(char *path, int atrib);
	int ftruncate(int fd, int size);
	int Erro;
	Rapi(char *Servidor, int porta);
	virtual ~Rapi();
	int open(char *fn,int modo,int atrib);
	int read(int fd, void *buf, unsigned siz);
	int write(int fd, char *buf, unsigned siz);
	int close(int fd);
	long lseek(int fd, long ptr, int modo);
	long size(char *fn, long *size);
	int execpgm
		(
		char *pgm,
		char *pgm_display,
		char *cmd,
		char *fn_in,
		char *fn_out,
		char *TxtBin
		);
	
private:
	unsigned long swap_long(unsigned long lptr);
	int host;
	SOCKET soc1;
	struct hd1
	{
		char cod;
		char tam1;
		u_long fd;
		u_long tam2;
		char buf[100000];
	} head1;
	
	struct ret1
	{
		long rc;
		long rc_siz;
		char buf[100240]; 
	} ret;
	
	int g_read();
	int send_rapi(unsigned int s, const char *buf, int len, int flags);
};

#endif 
