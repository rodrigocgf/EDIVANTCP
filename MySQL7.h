#ifndef _MYSQL7_H_
#define _MYSQL7_H_

#include <mysql.h>

#define MAX_COL  300

class MySQL7
{
public:
	MySQL7(
		const char *host,
		const char *usuario,
		const char *senha,
		const char *bd
	);
	MySQL7();
	virtual ~MySQL7();

	int connect(
		const char *host,
		const char *usuario,
		const char *senha,
		const char *bd
	);
	bool isConnected() { return connected; }

	bool ExecSQL(const char *sqlCmd);
	int getRowCount() { return nRows; }
	my_ulonglong getInsertID();
	bool moveNext();

	char getChar(const char *Col);
	char getChar(int colIdx);
	const char *getString(const char *Col);
	const char *getString(int colIdx);
	int getInt(const char *Col);
	int getInt(int colIdx);
	my_ulonglong getLongLong(const char *Col);
	my_ulonglong getLongLong(int colIdx);
	double getDouble(const char *Col);
	double getDouble(int colIdx);
	
	const char *getError();
	int getErrNo() { return ErrNo; }

private:
	MYSQL *myData ;
   MYSQL_RES *res ;
   MYSQL_FIELD *fd ;
   MYSQL_ROW row ;
	char aszFlds[MAX_COL][50];
	int nCols;
	int nRows;
	int ErrNo;
	bool connected;
};

#endif
