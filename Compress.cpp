#include "StdAfx.h"
#include "Compress.h"

CCompress::CCompress(bool permiteCompressao)
{
   Reset();
	mPermiteCompressao = permiteCompressao;
}

CCompress::~CCompress()
{
}

// Reseta as variaveis ce compactacao
void CCompress::Reset()
{
   m_NumBits            = INIT_BITS;                                                 
   m_MaxCode            = VAL_MAX(INIT_BITS);                                        
   m_NextCode           = FIRST_CODE;                                                
   m_TxAnt              = 500;                                                       
   m_OutputBitBuffer    = 0L;                                                        
   m_aCin               = 0;                                                         
   m_acCod              = 0;                                                         
   m_State              = 0;                                                         
   m_RepCnt             = 0;                                                         
   m_OutBitCount        = 0;                                                         
   m_ChkPass            = 0;                                                         
   m_Erro               = 0;
   m_iPosIOBuffer       = 0;
   m_nBytesInIOBuffer   = 0;

	for(int i=0; i < TABLE_SIZE; i++)
		m_CpValCod[i] = -1;
}


void CCompress::OutputCod(unsigned int Codigo)
{
   m_OutputBitBuffer |= ((unsigned long) Codigo) << (SIZEBITS(m_OutputBitBuffer) - m_NumBits - m_OutBitCount);
   m_OutBitCount += m_NumBits;                                                      
                                                                                
   while(m_OutBitCount >= SIZEBITS(BYTE))
   {                                                                            
      Write((BYTE)(m_OutputBitBuffer >> 24));
      m_OutputBitBuffer <<= SIZEBITS(BYTE);                                                    
      m_OutBitCount -= SIZEBITS(BYTE);                                                         
   }                                                                            

}

int CCompress::GetcNcr()
{
   if (m_State==NOHIST)                                                           
   {                                                                            
      m_State=SENTCHAR;                                                           
      m_Lastc=Read();                                                       
      return m_Lastc;                                                             
   }                                                                            
   if (m_State==SENTCHAR)                                                         
   {                                                                            
      if (m_Lastc==DLE)                                                           
      {                                                                         
         m_State=NOHIST;                                                          
         return 0;                                                              
      }                                                                         
      for (m_RepCnt=1; (m_CharLido=Read())==m_Lastc && m_RepCnt<255 && m_CharLido >= 0; m_RepCnt++);          
      if (m_RepCnt==1)                                                            
      {                                                                         
         m_Lastc=m_CharLido;                                                               
         return m_CharLido;                                                              
      }                                                                         
      if (m_RepCnt==2)                                                            
      {                                                                         
         m_State=SENDNEWC;                                                        
         return m_Lastc;                                                          
      }                                                                         
      m_State=SENDCNT;                                                            
      return DLE;                                                               
   }                                                                            
   if (m_State==SENDNEWC)                                                         
   {                                                                            
      m_State=SENTCHAR;                                                           
      m_Lastc=m_CharLido;                                                                  
      return m_CharLido;                                                                 
   }                                                                            
   m_State=SENDNEWC;                                                              
                                                                                
   return m_RepCnt;                                                               

}

int CCompress::Compress
	(
		Download *dl,
		BYTE *pBufferOut,
		DWORD dwMaxTamBloco,
		DWORD *pBytesLidos
	)
{
	int                 CpOffset;                                                
	long                CpTx2;                                                   
	int                 CpIndex;                                                 
	
	if (dwMaxTamBloco < 10)
		return LZC_ERRO_BLOCK_TOO_SMALL;
	
	m_Down = dl;
	m_pBufferOut = pBufferOut;
	
	// Por seguranca, diminuimos 10 do tamanho maximo informado
	// pois a rotina de compressao precisa colocar o terminator
	// e eventuais bytes restantes no fim do bloco
	m_dwMaxTamBloco = dwMaxTamBloco - 10; 
	m_iPosBuffer = 0;
	m_pBytesLidos = pBytesLidos;
	*m_pBytesLidos = 0;
	
	
	if(!mPermiteCompressao)
	{
		m_dwMaxTamBloco = dwMaxTamBloco; 
		*pBytesLidos = NoCompress();
		return *pBytesLidos;
	}
	
	m_StringCode = GetcNcr();
	if (m_StringCode == LZC_EOF)
		return m_Erro;
	if (m_StringCode < 0)
		return m_StringCode; // Erro de leitura
	
	while(1)                                                                  
	{
		if ( (m_CpChr=GetcNcr()) == LZC_EOF )                                        
			break;                                                              
		
		if ( m_CpChr == LZC_ERRO_LEITURA_ARQ)
			return m_CpChr;
		
		CpIndex = (m_CpChr << N_SHIFTS) ^ (short int)m_StringCode;                       
		
		if (CpIndex == 0)                                                      
			CpOffset = 1;                                                       
		else                                                                   
			CpOffset = TABLE_SIZE - CpIndex;                                    
		
		while(1)                                                               
		{                                                                      
			if (m_CpValCod[CpIndex] == -1)                                        
				break;                                                           
			if ( m_CodPfx[CpIndex]   == (unsigned short int)m_StringCode  &&              
				m_CaracSfx[CpIndex] == (unsigned char)m_CpChr )                    
				break;                                                           
			
			CpIndex -= CpOffset;                                                
			if (CpIndex < 0)                                                    
				CpIndex += TABLE_SIZE;                                           
		}                                                                      
		
		if (m_CpValCod[CpIndex] != -1)                                           
			m_StringCode = m_CpValCod[CpIndex];                                     
		else                                                                   
		{                                                                      
			if (m_NextCode <= (unsigned int)m_MaxCode)                              
			{                                                                   
				m_CpValCod[CpIndex] = m_NextCode++;                                  
				m_CodPfx[CpIndex] = m_StringCode;                                    
				m_CaracSfx[CpIndex] = (unsigned char) m_CpChr;                       
			}                                                                   
			OutputCod(m_StringCode);                                              
			m_StringCode = m_CpChr;                                                 
			
			
			if ( m_ChkPass++ > 550)                                               
			{                                                                   
				m_ChkPass = 0;                                                     
				CpTx2   = (m_acCod*100)/m_aCin;                                      
				if (CpTx2  > m_TxAnt)                                              
				{                                                                
					m_aCin     =                                                    
						m_acCod    = 0;                                                 
					m_TxAnt    = 500;                                               
					OutputCod(CLEAR_TABLE);                                       
					m_NumBits  = INIT_BITS;                                         
					m_NextCode = FIRST_CODE;                                        
					m_MaxCode  = VAL_MAX(INIT_BITS);                                
					memset( m_CpValCod, -1, TABLE_SIZE * sizeof(short int) );       
					continue;                                                     
				}                                                                
				m_TxAnt=CpTx2;                                                     
			}                                                                   
			
			if ( m_NextCode > (unsigned short int)m_MaxCode  &&  m_NumBits < MAX_BITS )     
				m_MaxCode = VAL_MAX(++m_NumBits);                                    
		}                                                                      
	}
	
	if (m_NextCode <= (unsigned short int)m_MaxCode)                              
	{                                                                   
		m_CpValCod[CpIndex] = m_NextCode++;                                  
		m_CodPfx[CpIndex] = m_StringCode;                                    
		m_CaracSfx[CpIndex] = (unsigned char) m_CpChr;                       
	}                                                                   
	
	OutputCod(m_StringCode);                                                    
	if ( m_NextCode > (unsigned int)m_MaxCode  &&  m_NumBits < MAX_BITS )           
		m_MaxCode = VAL_MAX(++m_NumBits);
	
	OutputCod(TERMINATOR);                                                       
	m_OutputBitBuffer = 0L;                                                        
	m_OutBitCount = 0;                                                             
	
	if (!m_Erro)
		return m_iPosBuffer;
	return m_Erro;                                                                 

}

int CCompress::Read()
{
	// Se completou o tamanho do bloco, simula EOF
	m_aCin++;                                                                
	if(m_iPosBuffer >= m_dwMaxTamBloco)
		return LZC_EOF;

   if(m_iPosIOBuffer >= m_nBytesInIOBuffer)
   {
      if(m_Down->getBloco((char *) m_IOBuffer, sizeof(m_IOBuffer),
			(int *) &m_nBytesInIOBuffer) == -1)
		   return LZC_ERRO_LEITURA_ARQ;
	   if(!m_nBytesInIOBuffer) 
		   return LZC_EOF;
      m_iPosIOBuffer = 0;
   }

   (*m_pBytesLidos)++;
	return m_IOBuffer[m_iPosIOBuffer++];
}


void CCompress::Write(BYTE Codigo)
{
   m_acCod++;                                                                     
	m_pBufferOut[m_iPosBuffer++] = Codigo;
}


int CCompress::NoCompress()
{
   int bytesLidos;
   if(m_Down->getBloco((char *) m_pBufferOut, m_dwMaxTamBloco, &bytesLidos) == -1)
		return LZC_ERRO_LEITURA_ARQ;
   return bytesLidos;
}
