#ifndef _COMPRESS_H_
#define _COMPRESS_H_

#include "StdAfx.h"
#include "LzcDefs.h"
#include "Download.h"

class CCompress  
{
public:
	void Reset();
	int Compress(Download *dl, BYTE *pBufferOut, DWORD dwMaxTamBloco, DWORD *pBytesLidos);
	CCompress(bool permiteCompressao);
	virtual ~CCompress();

private:
	bool mPermiteCompressao;
	int NoCompress();
	DWORD *m_pBytesLidos;
	DWORD m_iPosIOBuffer;
	DWORD m_nBytesInIOBuffer;
	BYTE m_IOBuffer[TAM_IO_BUFFER];
	DWORD m_iPosBuffer;
	DWORD m_dwMaxTamBloco;
	BYTE * m_pBufferOut;
	Download *m_Down;
	unsigned short int m_NextCode;
	short int m_StringCode;
	short int m_CpChr;
	short int m_CharLido;
	int Read();
	short int m_Lastc;
	int GetcNcr();
	void Write(BYTE Codigo);
	void OutputCod(unsigned int Codigo);
	short int m_Erro;
	short int m_ChkPass;
	unsigned char m_OutBitCount;
	unsigned short int m_RepCnt;
	unsigned char m_State;
	long m_acCod;
	long m_aCin;
	long m_TxAnt;
	short int m_MaxCode;
	short int m_NumBits;
	unsigned long m_OutputBitBuffer;
	BYTE m_CaracSfx[TABLE_SIZE];
	unsigned short int m_CodPfx[TABLE_SIZE];
	short int m_CpValCod[TABLE_SIZE];
};

#endif // 
