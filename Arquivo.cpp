#include "StdAfx.h"
#include "Arquivo.h"
#include "erros.h"
#include "LogApiFunc.h"

extern CLogApiFunc logapi;

#define hashCod(Cod)   Cod%100

Arquivo::Arquivo(CConfig* cfg)
{
	sql7 = new MySQL7(cfg->BdServer, cfg->BdUsuario, cfg->BdSenha, cfg->BdName);
	strncpy(dirCaixaPostal, cfg->diretorioRaiz(), 101);
	strncpy(dirConsistencia, cfg->diretorioRaiz(), 101);
	strcat(dirConsistencia,"bin/");
	strcat(dirCaixaPostal,"cxp/%02d/%011d/"); 
	
	rapi = new Rapi(cfg->RapiServer, cfg->RapiPort);
    
	ErroRapi = rapi->Erro;
	fd = 0;
}

Arquivo::~Arquivo()
{
	if(sql7 != NULL)
		delete sql7;
	if(rapi != NULL)
		delete rapi;
}

// ----- Fun��es que alteram o estado do
// ----- Objeto Arquivo

// Somente arquivos de upload
int Arquivo::jaTransmitido(
									int CodDominio,
									char* CodCliente,
									char* CodFormato,
									char* NomeOrigem,
									struct tm DataHoraOrigem)
{

	_snprintf(cmd, MAX_CMD_LEN, 
		"select * from Arquivo where CodDominio=%d "
		"and CodCliente='%s' and CodFormato='%s' "
		"and nomeOrigem='%s' "
		"and DataHoraOrigem = '%s' "
		//		"and StatusArquivo <> 'P' "
		"and TipoArquivo='U'", // N�o pega arquivos j� retransmitidos
		CodDominio, CodCliente, CodFormato, NomeOrigem,
		datetm2string(DataHoraOrigem));
	sql7->ExecSQL(cmd);
	
	if(sql7->getRowCount() > 0)
	{
		atualizaEstado();
		return true; 
	}
	else
	{
		return false;
	}
}

int Arquivo::Existe(int id, char Tipo)
{
	// Busca arquivo pelo id desde que n�o seja retransmitido
	_snprintf(cmd, MAX_CMD_LEN, 
		"select * from Arquivo where idArquivo=%d "
		"and TipoArquivo='%c' and ", id, Tipo);
	
	if(Tipo == 'U') 
		// Upload n�o seleciona [O]-Sobrescrito,[D]-Disponivel nem [R]-Removido
		strncat(cmd, "StatusArquivo not in ('O', 'R')",
		MAX_CMD_LEN - strlen(cmd) - 1);
	else
		// Download seleciona apenas [P]-Parcial, [T]-Total e [D]-Disponivel
		strncat(cmd, "StatusArquivo in ('A', 'P', 'D', 'C')",
		MAX_CMD_LEN - strlen(cmd) - 1);
	
	sql7->ExecSQL(cmd);
	
	if(sql7->getRowCount() > 0)
	{
		atualizaEstado();
		return true;
	}
	else
	{
		return false;
	}


}

int Arquivo::Criar(
							int CodDominio,
							char *CodCliente, 
							char *CodUsuario,
							char *CodFormato, 
							char *NomeOrigem,
							struct tm DataHoraOrigem,
							int TamanhoOrigem,
							char *ModoOperacao,
							char TipoArquivo)
{

	_snprintf(cmd, MAX_CMD_LEN,
		"select C.CodEdivan, D.CodEdivan CodEdivanDominio "
		"from ClienteDominio C, Dominio D "
		"where C.CodDominio=D.CodDominio and "
		"C.CodCliente='%s' and C.CodDominio = %d",
		CodCliente, CodDominio);
	
	if(sql7->ExecSQL(cmd))
	{
		if(sql7->getRowCount() == 0)
			return EDI_MSG_ERRO_CLIENTE_DOMINIO;
	}
	else
	{
		return EDI_MSG_ERRO_COMANDO_SQL;
	}
	
	CodEdivan = sql7->getInt("CodEdivan");
	CodEdivanDominio = sql7->getInt("CodEdivanDominio");
	
	_snprintf(cmd, MAX_CMD_LEN, 
		"Select C.Producao, F.ConfereDestino, F.OffSetIdCliente, "
		"F.TamIdCliente from ClienteConfiguracao C, FormatoArquivo F "
		"Where C.CodDominio = F.CodDominio "
		"and C.CodFormato = F.CodFormato and C.CodDominio = %d "
		"and C.CodFormato = '%s' and C.CodCliente = '%s'",
		CodDominio, CodFormato, CodCliente);
	
	if(sql7->ExecSQL(cmd))
	{
		if(sql7->getRowCount() == 0)
			return EDI_MSG_ERRO_CLIENTE_PRODUTO_FORMATO;
	}
	else
	{
		return EDI_MSG_ERRO_COMANDO_SQL;
	}
	
	char Producao = sql7->getChar("Producao");
	char ConfDest = sql7->getChar("ConfereDestino");
	
	//Verifica se existe o Cliente Destino
	if(ConfDest == 'S' && CodEdivan == CodEdivanDominio)
	{
		long OffSetIdCliente = sql7->getInt("OffSetIdCliente");
		long TamIdCliente = sql7->getInt("TamIdCliente");
		
		int ret = ConfereDestino(CodDominio,NomeOrigem, CodFormato,
			OffSetIdCliente, TamIdCliente);
		if(ret != 0)
			return ret;
	}
	_snprintf(cmd, MAX_CMD_LEN, 
		"insert into Arquivo ("
		"CodEdivan,NomeOrigem,DataHoraOrigem,TamanhoOrigem,"
		"TipoArquivo,CodDominio,CodCliente,"
		"CodFormato,CodUsuario,ModoOperacao,"
		"StatusArquivo,ControleRestart,TamanhoDestino,Producao) "
		"values (%d,'%s','%s',%d,'%c',%d,'%s',"
		"'%s','%s','%s','P',0,0,'%c')",
		CodEdivan, NomeOrigem, datetm2string(DataHoraOrigem),
		TamanhoOrigem, TipoArquivo, CodDominio, CodCliente,
		CodFormato, CodUsuario, ModoOperacao, Producao);
	
	if(!sql7->ExecSQL(cmd))
		return EDI_MSG_ERRO_COMANDO_SQL;
	if(sql7->getRowCount() == 0)
		return EDI_MSG_ERRO_CRIANDO_ARQUIVO;
	
	idArquivo = (long) sql7->getInsertID();   
	
	time_t now;
	struct tm *today;
	time(&now);
	today = localtime(&now);
	
	sprintf(NomeDestino, "%c%s%.2d%.2d.%d",
		TipoArquivo, CodFormato, today->tm_mday, today->tm_mon + 1, idArquivo);
	int erro = 0;
	restartPt = 0;
	TamanhoDestino = 0;
	
	_snprintf(cmd, MAX_CMD_LEN, 
		"update Arquivo Set NomeDestino='%s' "
		"where idArquivo=%d",
		NomeDestino, idArquivo);
	if(!sql7->ExecSQL(cmd))
		return EDI_MSG_ERRO_COMANDO_SQL; 
	if(sql7->getRowCount() == 0)
		erro = EDI_MSG_ERRO_NO_CADASTRO_ARQUIVO;
	AbreArquivo(true, true);
	if(!Aberto())
	{
		erro = EDI_MSG_ERRO_ABRINDO_ARQ_FISICO;
	} else {
		FechaArquivo(false);
	}
	
	if(erro)
	{
		_snprintf(cmd, MAX_CMD_LEN, 
			"delete from Arquivo where idArquivo=%d", idArquivo);
		sql7->ExecSQL(cmd);
		return erro;
	}
	return 0;
}

int Arquivo::ReCriar(
							int id, 
							int CodDominio,
							char *CodCliente, 
							char *CodUsuario,
							char *CodFormato, 
							char *NomeOrigem,
							struct tm DataHoraOrigem,
							int TamanhoOrigem,
							char *ModoOperacao,
							char TipoArquivo)
{
	_snprintf(cmd, MAX_CMD_LEN, 
		"update Arquivo set StatusArquivo='O' "
		"where idArquivo = %d", id);
	if(!sql7->ExecSQL(cmd))
		return EDI_MSG_ERRO_COMANDO_SQL;
	if(sql7->getRowCount() == 0)
		return EDI_MSG_ERRO_NO_CADASTRO_ARQUIVO;
	return Criar(CodDominio, CodCliente, CodUsuario,CodFormato, 
		NomeOrigem, DataHoraOrigem, TamanhoOrigem, ModoOperacao, TipoArquivo);
}

int Arquivo::ConfereDestino(
					int  CodDominio,
					char *ArqOrigem, 
					char *CodFormato,
					long OffSetIdCliente,
					long TamIdCliente)
{
	char fnCli[300];
	char *pfnCli;
	char CodClienteDestino[50];
	char SqlCmd[500];

	memset(fnCli , 0x00 , sizeof(fnCli) );
	memset(CodClienteDestino , 0x00 , sizeof(CodClienteDestino) );
	memset(SqlCmd , 0x00 , sizeof(SqlCmd) );

	if (this->CodClienteDestino[0])
		strcpy(CodClienteDestino,this->CodClienteDestino);
	else
	{
		strcpy( fnCli, ArqOrigem );
		//procura a ultima ocorrencia do caractere '\'
		pfnCli = (char *) strrchr ( fnCli, '\\' );
		//Aponta para o primeiro caractere que compoe o CodClienteDestino (posAtual+OffSetIdCliente)
		pfnCli = ( pfnCli==NULL ? fnCli+OffSetIdCliente-1 : pfnCli+OffSetIdCliente );
		//Copia somente os caracteres que compoem o CodClienteDestino
		strncpy( CodClienteDestino, pfnCli, TamIdCliente );
	}

	// Verifica Existencia do Cliente
	sprintf(SqlCmd,
		"SELECT Producao FROM ClienteConfiguracao "
		"WHERE CodDominio = %d AND CodCliente = '%s' AND "
		"CodFormato = '%s'", CodDominio, CodClienteDestino, CodFormato);

	if(sql7->ExecSQL(SqlCmd))
	{
		if(sql7->getRowCount() == 0)
		{
			return EDI_MSG_ERRO_CLIENTE_DESTINO_NAO_ENCONTRADO;
		}
	}
	else
	{
		return EDI_MSG_ERRO_COMANDO_SQL;
	}
	return 0;
}

int Arquivo::Processado()
{
   return StatusArquivo == 'C' || StatusArquivo == 'S';
}

int Arquivo::TotalmenteTransmitido()
{
   return !(StatusArquivo == 'A' || StatusArquivo == 'P' || StatusArquivo == 'D');
}

int Arquivo::Parcial()
{
   return StatusArquivo=='P' || StatusArquivo=='A';
}

int Arquivo::getID()
{
   return idArquivo;
}

void Arquivo::atualizaEstado()
{
   idArquivo = sql7->getInt("IdArquivo");
   restartPt = sql7->getInt("ControleRestart");
   StatusArquivo = sql7->getChar("StatusArquivo");
   TipoArquivo = sql7->getChar("TipoArquivo");
   TamanhoDestino = sql7->getInt("TamanhoDestino");
   CodEdivan = sql7->getInt("CodEdivan");
   CodDominio = sql7->getInt("CodDominio");
   TamanhoOrigem  = sql7->getInt("TamanhoOrigem");
   strncpy(NomeDestino, sql7->getString("NomeDestino"), 101);
   strncpy(NomeOrigem, sql7->getString("NomeOrigem"), 101);
   strncpy(CodFormato, sql7->getString("CodFormato"), 11);
   strncpy(ModoOperacao, sql7->getString("ModoOperacao"), 4);
}

bool Arquivo::formatoBinario()
{
   _snprintf(cmd, MAX_CMD_LEN, 
		"select Binario from FormatoArquivo where "
		"Binario='N' and CodFormato='%s' "
		"and CodDominio=%d", CodFormato, CodDominio);
   sql7->ExecSQL(cmd);
   if(sql7->getRowCount() > 0) 
	   return false;
   else
		return true;
}

/* Fun��es que utilizam a RAPI */
int Arquivo::AbreArquivo(bool criar, bool truncar)
{
	if(ErroRapi)
		return (ErroRapi);
	
	char fullName[MAX_PATH + 1];
	char pathName[MAX_PATH + 1];

	memset(fullName , 0x00 , sizeof(fullName) );
	memset(pathName , 0x00 , sizeof(pathName) );
	
	int modo = O_RDWR;
	if(criar)
		modo = modo | O_CREAT;
	if(truncar)
		modo = modo | O_TRUNC;
	
	sprintf(pathName, dirCaixaPostal, hashCod(CodEdivan), CodEdivan);
	if(TipoArquivo == 'D')
		sprintf(fullName, "%s%s", pathName, NomeOrigem);
	else
		sprintf(fullName, "%s%s", pathName, NomeDestino);
	
	fd = rapi->open(fullName, modo, 0640);
	
	if(fd < 0 && criar)
	{
		//tenta de novo criando a caixa postal (diret�rio)
		int ret = rapi->mkdirs(pathName, 0740);
		if(ret)
		{
			fd = rapi->open(fullName, modo, 0640);
		}
	}
	return fd >= 0;
}

int Arquivo::Aberto()
{
	return fd >= 0;
}

int Arquivo::Gravar(char *Buffer, int size)
{
	if(ErroRapi)
		return(ErroRapi);

	TamanhoDestino += size;
	restartPt += size;

	if ( rapi->write(fd, Buffer, size) != size )
		return EDI_MSG_ERRO_GRAVANDO_ARQUIVO;

	return 0;
}

int Arquivo::Consistir()
{
	if ( ErroRapi ) 
		return ( ErroRapi );
    
	char prgName[MAX_PATH + 1];
	char prgConsiste[101 + 1];
	char cmd[260];
	int i;	
	char sId[21];
	
	memset(prgName,0x00,sizeof(prgName));
	memset(prgConsiste,0x00,sizeof(prgConsiste));
	memset(cmd,0x00,sizeof(cmd));
	
	_snprintf(cmd, 260,
		"select PgmConsistencia from FormatoArquivo "
		"where CodFormato='%s' and CodDominio =%d", CodFormato, CodDominio);
	logapi.logdump("Consistir >> COMANDO SQL : ",cmd,strlen(cmd) );

	if(sql7->ExecSQL(cmd)) {
		if(sql7->getRowCount() > 0) 
			strncpy(prgConsiste, sql7->getString("PgmConsistencia"), 101);
		strncpy(prgName, dirConsistencia, MAX_PATH);
		strncat(prgName, prgConsiste, MAX_PATH - strlen(dirConsistencia) - 1);
				
		memset(sId,0x00,sizeof(sId));
		i = 0;
		
		sprintf(sId, "%20d", idArquivo );
		logapi.logtrc("Consistir >> Chamando via RAPI o programa %s.\r\n", prgName);

		///////////////////////////////////////////////////////
		//
		//	SUBSTITUIR O C�DIGO COMENTADO ABAIXO
		//

		/*
		if ( atol(this->CodClienteDestino) != (long)1 ) {
			i = rapi->execpgm(prgName, "Consiste", sId, this->CodClienteDestino , "", "");
		} else {
			i = rapi->execpgm(prgName, "Consiste", sId, "" , "", "");
		}

		if(i) {
			if(i == 15)
				i = EDI_MSG_ERRO_PRG_CONSISTENCIA_NAO_ENCONTRADO; // Recodifica o erro. 
		}
		*/

		if(i = rapi->execpgm(prgName, "Consiste", sId, this->CodClienteDestino, "", ""))
		{
			if(i == 15)
				i = EDI_MSG_ERRO_PRG_CONSISTENCIA_NAO_ENCONTRADO; // Recodifica o erro. 
		}
		
		//
		//
		////////////////////////////////////////////////////////
		
		
		if(i) {
			logapi.logtrc("Consistir >> Erro do programa Consiste = %d.\r\n",i);
			StatusArquivo = 'E';
		} else {
			StatusArquivo = 'T';
			logapi.logtrc("Consistir >> StatusArquivo = T.\r\n");
		}
		return (i);
	} else {
		logapi.logtrc("Consistir >> Erro no comando SQL. StatusArquivo = E .\r\n");
		StatusArquivo = 'E';
		return 1; // Erro
	}
}

int Arquivo::FechaArquivo(bool truncar)
{
   int erro = 0;

   if ( truncar )
		erro = rapi->ftruncate(fd, TamanhoDestino);
   erro ^= rapi->close(fd);
   return erro;
}

int Arquivo::getSize(char * NomeHost)
{
   long size;
   char fullname[MAX_PATH + 1];

   memset(fullname , 0x00 , sizeof(fullname) );

   sprintf(fullname, dirCaixaPostal, hashCod(CodEdivan), CodEdivan);
   strcat(fullname, NomeHost);

   int i = rapi->size(fullname, &size); 
   if(i == 0)
		return (int) size;
   else
		return -1;
}

int Arquivo::TamanhoDisco()
{
	bool Ok = true;
	int tamDisco;
	if(TipoArquivo == 'U')
	{
		tamDisco = getSize(NomeDestino);
		// Upload se n�o existir, tamanho � zero
		if(tamDisco == -1)
			tamDisco = 0;
		Ok = tamDisco >= TamanhoDestino; 
		// N�o tem problema se no disco estiver maior
		// no final do upload o arquivo ser� truncado
	}
	else 
	{
		tamDisco = getSize(NomeOrigem);
		Ok =  tamDisco == TamanhoOrigem;
	}

	if(Ok)
		return tamDisco;
	else
		return -1;
}

int Arquivo::Read(char *BlocoOut, int size)
{
	if(ErroRapi)
		return (ErroRapi);

	// L� um bloco e, se for texto, rep�e os Carriage Return.
   int rc = rapi->read(fd, BlocoOut, size);	
	if(!rc)
		return 0;
	restartPt += rc; 
	TamanhoDestino += rc;
	return rc;
}

int Arquivo::PosicionaArquivo()
{                               
	if(TipoArquivo == 'U') 
	   return rapi->lseek(fd, TamanhoDestino, SEEK_SET); 
	else
	   return rapi->lseek(fd, restartPt, SEEK_SET); 
}
char Arquivo::GetStatusArquivo()
{
	return StatusArquivo;
}

int Arquivo::salvarEstado()
{
	_snprintf(cmd, MAX_CMD_LEN, 
		"update Arquivo set ControleRestart=%d, TamanhoDestino=%d, ModoOperacao='%s', "
		"StatusArquivo='%c', NomeDestino='%s', DataHoraDestino=now(), DataHoraRetirado=now() "
		"where idArquivo=%d", restartPt, TamanhoDestino, ModoOperacao,
		StatusArquivo, NomeDestino, idArquivo);

	if ( StatusArquivo == 'T' ) {
		//logapi.logtrc("salvarEstado >> StatusArquivo = T.\r\n");
		logapi.logdump("salvarEstado >> StatusArquivo = T : ",cmd,strlen(cmd));
	}
	
	if(!sql7->ExecSQL(cmd))
		return EDI_MSG_ERRO_NO_CADASTRO_ARQUIVO;

	// Atualiza o estado da origem na conclusao do download
	if(TipoArquivo == 'D' && StatusArquivo == 'C')
	{
		_snprintf(cmd, MAX_CMD_LEN, 
			"update Arquivo set "
			"StatusArquivo='%c', DataHoraRetirado=now() "
			"where idArquivoRef=%d", 'F', idArquivo);
		if(!sql7->ExecSQL(cmd))
    		return EDI_MSG_ERRO_NO_CADASTRO_ARQUIVO;

		_snprintf(cmd, MAX_CMD_LEN, 
			"update Arquivo set "
			"DataHoraRetirado=now() "
			"where idArquivo=%d", 'F', idArquivo);
    
		if(!sql7->ExecSQL(cmd))
			return EDI_MSG_ERRO_NO_CADASTRO_ARQUIVO;
	}
	return 0;
}

char* Arquivo::datetm2string(struct tm data)
{
   static char strData[40];

   memset(strData , 0x00 , sizeof(strData) );

   sprintf(strData, "%.4d-%.2d-%.2d %.2d:%.2d:%.2d",
		data.tm_year + 1900,
		data.tm_mon + 1,
		data.tm_mday,
		data.tm_hour,
		data.tm_min,
		data.tm_sec);
	return strData; 
}
