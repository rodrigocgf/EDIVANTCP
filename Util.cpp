#include "StdAfx.h"
#include "Util.h"

#define MAX_TAM_STRING_TO_NUM  100

void StringNCopy(char *Destino, char *Origem, DWORD nBytes)
{
	memcpy(Destino, Origem, nBytes);
	*(Destino + nBytes) = 0;
}

int StringToInt(char *String, WORD nBytes)
{
	char StringAux[MAX_TAM_STRING_TO_NUM + 1];


	if (nBytes > MAX_TAM_STRING_TO_NUM) // Erro de passagem de parametro
		return atoi(String);					// retorna atoi

	StringNCopy(StringAux, String, nBytes);
	return atoi(StringAux);

}

long StringToLong(char *String, WORD nBytes)
{
	char StringAux[MAX_TAM_STRING_TO_NUM + 1];


	if (nBytes > MAX_TAM_STRING_TO_NUM) // Erro de passagem de parametro
		return atoi(String);					// retorna atoi

	StringNCopy(StringAux, String, nBytes);
	return atol(StringAux);

}

void Encripta(char *Destino, const char *Origem, WORD nBytes)
{
   WORD j = 0;
   
   for (int i = 0 ; i < nBytes ; i++) {
      Destino[i] = Origem[i] ^ CHAVE[j];
      if ( ++j >= TAM_CHAVE )
         j = 0;
   }

}

void WINAPI ProcessMessages(void) {

	MSG msg;

	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
   {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return;
}

void RTrim(char *String)
{
   int i = strlen(String) - 1;

   while(i >= 0)
   {
      if(String[i] == ' ')
         String[i] = 0;
      i--;
   }
}

static char *Codigo="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

BOOL CriaTempFileName(char *sFileName,char *sPath)
{
   SYSTEMTIME SystemTime;
   int iVezes;
   int fd;


   // so para evitar loopings infinitos
   for(iVezes=0; iVezes < UTIL_TEMPFILE_MAX_TRYES; iVezes++)
   {
      GetLocalTime(&SystemTime);
      sprintf(sFileName,"%s\\%c%c%c%02X%02X%c.TMP",sPath,
         Codigo[SystemTime.wMonth - 1],
         Codigo[SystemTime.wDay - 1],
         Codigo[SystemTime.wHour],
         SystemTime.wMinute,
         SystemTime.wSecond,
         Codigo[SystemTime.wMilliseconds % strlen(Codigo)]);
      if (_access(sFileName,0) < 0) // Arquivo nao existe
      {
         if ((fd = _open(sFileName,_O_WRONLY|_O_CREAT|_O_EXCL|_O_BINARY,_S_IREAD|_S_IWRITE)) < 0)
            continue;
         _close(fd);
         break;
      }

   }
   if (iVezes >= UTIL_TEMPFILE_MAX_TRYES)
      return FALSE;

   return TRUE;
}


int ByteArrayToHexString (const BYTE *sByte, BYTE *sHexa, int lenori_)
{
	BYTE *sAuxi;

    //-- Tabela Hexa
    const static BYTE tsaHex[] = "0123456789ABCDEF";

	// Salvando Buffer Origem
	sAuxi = (BYTE *)malloc(lenori_+1);
	strncpy((char *)sAuxi, (char *)sByte, lenori_);
	sAuxi[lenori_] = 0x00;

    //-- Limpando a string de sa�da primeiro...
	memset(sHexa, 0x00, 2*lenori_);

	for (int i = 0; i < lenori_; i++)
    {
		sHexa[i*2]   = tsaHex [(sAuxi [i] >> 4) & 0x0F];
		sHexa[i*2+1] = tsaHex [sAuxi [i] & 0x0F];
    }

    //-- Retornando o comprimento da string (que deve ser
    //-- o dobro do tamanho do array original:)
    return strlen((char *)sHexa);
}

int HexStringToByteArray(BYTE *sHexa, BYTE *sByte, int oriLen_, int destLen_)
{
	BYTE *sAuxi;
    int srcLen;
    int i, j;
    int ch;
    int by;
    int val;
    BOOL f2ndChar = FALSE;

	// Salvando Buffer Origem

	srcLen = strlen((char *)sHexa);
	sAuxi = (BYTE *)malloc(oriLen_ +1);
	memset(sAuxi, 0x00, oriLen_ +1 );
	if (srcLen > oriLen_)
		srcLen = oriLen_;
	strncpy((char *)sAuxi, (char *)sHexa, srcLen);

    //-- Fazendo a gentileza de zerar o espa�o antes
    memset (sByte, '\0', destLen_);

    //-- Agora efetuando o processamento
    for (i = j = 0; i < destLen_ && j < srcLen; j++)
    {
		ch = sAuxi[j];

        if ( ( ch >= '0'  &&  ch <= '9' ) ||
			 ( ch >= 'A'  &&  ch <= 'Z' ) ||
			 ( ch >= 'a'  &&  ch <= 'z' ) )
        {
			switch (ch)
            {
				case '0': case '1': case '2': case '3':
                case '4': case '5': case '6': case '7':
                case '8': case '9':
                    val = ch - '0'; break;
                case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
                    val = ch - 'A' + 0x0A; break;
                case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
                    val = ch - 'a' + 0x0A; break;
            }
            if (!f2ndChar)
            {
				by = val;
            }
            else
            {
				by = (by << 4) | val;
                sByte [i++] = by;
            }
            f2ndChar = !f2ndChar;
        }
    }
    return i;
}


void GravaLinha( HANDLE hArq, BYTE *sBuffer )
{
	DWORD dwGravados;
	BYTE sBufWrite[1000];

	memset( sBufWrite, 0x00, sizeof(sBufWrite) );
 	ByteArrayToHexString( sBuffer, sBufWrite, strlen((char *)sBuffer ) );
	strcat((char *)sBufWrite, "\r\n");
	WriteFile(hArq, sBufWrite, strlen((char *)sBufWrite), &dwGravados, NULL );
}



void GeraLog(char * Format, ...)
{
	int fdLog;
	char str[2000];
	SYSTEMTIME LocalTime;
	va_list arg_ptr;

	GetLocalTime( &LocalTime );
	sprintf(str,"%02d/%02d/%04d %02d:%02d:%02d ",
		LocalTime.wDay, LocalTime.wMonth, LocalTime.wYear,
		LocalTime.wHour, LocalTime.wMinute, LocalTime.wSecond);

	va_start(arg_ptr,Format);

	vsprintf(&str[strlen(str)],Format,arg_ptr);

	if ((fdLog=_open("D:\\temp\\Mv7Srv.log",_O_WRONLY|_O_CREAT|_O_APPEND|_O_BINARY,0777)) < 0)
		return;
	strcat(str,"\r\n");
	_write(fdLog,str,strlen(str));
	_close(fdLog);

   va_end(arg_ptr);
}
