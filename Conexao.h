#ifndef _CONEXAO_CLIENTE_
#define _CONEXAO_CLIENTE_

#include "StdAfx.h"
#include "Config.h"
#include "Util.h"
#include "Socket32.h"
#include "MySQL7.h"

#define MENOR_VERSAO_COMPATIVEL     500 
#define VERSAO_CENTRAL              "0510"
#define CAIXA_PUBLICA               "00000000"
#define GRUPO_ACESSO                "AcessoTS"
#define GRUPO_TRANS                 "TransferenciaTS"
#define GRUPO_ATUALIZACAO           "AtualizacaoTS"

#define TAM_COMANDO           3
#define TAM_DOMINIO           10
#define TAM_USUARIO           20
#define TAM_CLIENTE           30
#define TAM_SENHA             15
#define TAM_VERSAO            8
#define TAM_RESERVADO         32
#define TAM_USUARIOSENHAVERSAO   (TAM_USUARIO + TAM_SENHA + TAM_VERSAO)
#define TAM_BLOCOTRANSACAO    1024
#define TAM_TIPOTRANSACAO     3


#define TIMEOUT_RECEIVE       30 // Segundos

// Comandos enviados para o cliente
#define SUCESSO_NA_OPERACAO              "000"
#define ERRO_NA_OPERACAO                 "001" 
#define USUARIO_INVALIDO                 "002"
#define SENHA_INVALIDA                   "003"
#define TRANSACAO_INVALIDA               "004"
#define ARQUIVO_NAO_EXISTE               "005"
#define CX_POSTAL_SEM_ACESSO             "006"
#define USUARIO_DUPLICADO                "007"

#define ERR_SOCKET                       "008"
#define ERR_CHAVEINVALIDA                "009"
#define ERR_ESTRUTURAINVALIDA            "010"
#define ERR_ABRINDOEVENTO                "011"
#define ERR_CRIANDOARQUIVO               "012"
#define ERR_GRAVANDOARQUIVO              "013"
#define ERR_ABRINDOARQUIVO               "014"
#define ERR_LENDOARQUIVO                 "015"
#define ERR_TIMEOUT                      "016"
#define ERR_JAEXISTELOGONUSUARIOSENHA    "017"
#define ERR_VERSAO_INCOMPATIVEL          "100"
#define ERR_INCLUINDO_REL_CXP            "101"
#define ERR_EXCLUINDO_REL_CXP            "102"

#define USUARIO_SISTEMA		"********"

#define MAX_ARQS        100

typedef struct
{
   char Tipo[TAM_TIPOTRANSACAO];
   char Buffer[TAM_BLOCOTRANSACAO - TAM_TIPOTRANSACAO];
} STransacao;

class CConexaoCliente
{
public:
	CConexaoCliente(SOCKET hClientSocket);
	~CConexaoCliente();
	
	bool ConsisteChaveEntrada();
	bool ConsisteUsuarioSenha();
	int AguardaTransacao(long lTimeOut);
	const char *ObtemUltimoErroCon();
	int ObtemErroSocket();
	const char *getUsuario() { return mUsuario; }
	bool RecebeArq();
	bool EnviaArquivo();
	// TODO!!
	bool RecebeTrn();
	bool EnviaTrn();
	bool AtualizaCxPostal();
	bool AtualizaCliente();
	bool Relaciona();
	bool DeletaItemLista(int NumeroConexao);
	bool AlteraSenha();
	bool AlteraCliente();
	bool ExcluiCliente();
	bool ExcluiRelacao();
	bool UsuarioBayer(const char *p_usuario , MySQL7 *p_mysql);
    bool GravaLogNavegacao(char *Grupo,char *Transacao,char *Data, char *Hora, int Status);
	bool GetDataHoraMySql(char *DataHora);
	CWinThread *p_Parent;

private:
	void setidSessao();
	void RESET_TIMER();
	bool verificaDominio(const char *dominio);
	bool enviaSucessoOperacao();
	bool enviaSucessoOperacao(DWORD RestartPoint);
	bool enviaErroOperacao();
	bool enviaCodigoErro(const char *sErro);
	int verificaLogin(const char *usr, const char *pwd);
	int listaDownload
		(
		const char *codCliente,
		int codDominio,
		const char *codFormato
		);
	
private:
	
	CConfig *p_Config;
	bool mCompressao;
	bool mRestart;
	char mMyHost[128 + 1];
	char mMyUser[128 + 1];
	char mMyPwd[64 + 1];
	char mMyDb[64 + 1];
	long mDominio;
	long mTamCodCliente;
	char mClienteDominio[TAM_CLIENTE + 1];
	char mCliente[TAM_CLIENTE + 1];
	char mUsuario[TAM_USUARIO + 1];
	char mErro[100];
	char midSessao[64];
	CTcpSocket *mClientSocket;
	bool mFirstTime;
	
	int mCodEdivan;
	char mCodCliente[30 + 1]; // 30: CodCliente em UsuarioPerfil definido varchar(30)
	
	CTRSTRUCTINFO mFiles[MAX_ARQS];
	int mFilesCnt;
	int mFilesIdx;
};

#endif

