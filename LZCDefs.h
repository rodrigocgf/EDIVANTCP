#ifndef _LZCDEFS_H
#define _LZCDEFS_H

#define  LZC_EOF						(-1)
#define  LZC_ERRO_LEITURA_ARQ	   (-2)
#define  LZC_ERRO_BLOCK_TOO_SMALL (-3)
#define  LZC_ERRO_DESCOMPRESSAO   (-4)
#define  LZC_ERRO_GRAVACAO_ARQ    (-5)

#define  SIZEBITS(X)     (sizeof(X) * 8)

#define  NOHIST          0                                                      
#define  INREP           1                                                      
#define  SENTCHAR        1                                                      
#define  SENDNEWC        2                                                      
#define  SENDCNT         3                                                      
#define  DLE             0x90                                                   

#define  N_SHIFTS        (MAX_BITS - 8)
#define  VAL_MAX(n)      (( 1 << (n)) -1)                                       

#define  TAM_IO_BUFFER   5120
#define  TABLE_SIZE      18041                                                  
#define  INIT_BITS       9                                                      
#define  MAX_BITS        14                                                     
#define  FIRST_CODE      258                                                    
#define  CLEAR_TABLE     256                                                    
#define  TERMINATOR      257                                                    
                                                                                
#define  DE_STACK_SIZE   16384 /*//4000 */                                      

#endif _LZCDEFS_H
