// Parm.h: interface da CGI
//
//	[002]  -  Cristiane Cohen		02/06/2003
//			  Implementa execu��o gen�rica de programa
//////////////////////////////////////////////////////////////////////

#if !defined(PARM_INCLUDED)
#define PARM_INCLUDED

#include "StdAfx.h"

struct Parametros {
      int             idArquivo;
      int             CodDominio;
      char            CodFormato[11];
      char            NomeOrigem[MAX_PATH];  /* nome do arquivo no Cliente Upload	*/
      char            NomeDestino[MAX_PATH]; /* nome do arquivo no Cliente Download	*/
      int             TamanhoOrigem;         /* Tamanho do arquivo no Cliente Upload */
      int             TamanhoDestino;        /* Tamanho do arquivo no Cliente Download */
      struct tm       DataHoraOrigem;        /* data do arquivo no Cliente */
      char            ModoOperacao[4];       /* Antigo TipoDeTransmissao;	Nor=Normal Res=Restart Ret=Retransmiss�o	*/
      char            CodCliente[50];        /* Cliente									*/
      char            Comando;               /* Comando recebido da applet. Ver fun��es abaixo.		*/
      char            CodUsuario[30];
      char            Dia[3];
      char            Sessao[100];
      char            Senha[50];
      int             Verifica;
      int             CRC;
      char            Programa[100];         // [002]
      char            Parametro1[100];       // [002]
      char            Parametro2[100];       // [002]
      char            Parametro3[100];       // [002]
      char            Parametro4[100];       // [002]
      char            IP_Client[16];
      char            Port[8];
};

#endif

