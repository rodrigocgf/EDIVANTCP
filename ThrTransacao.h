#if !defined(AFX_THRTRANSACAO_H__A74044C1_78C9_453D_A587_5C4CD0D5D017__INCLUDED_)
#define AFX_THRTRANSACAO_H__A74044C1_78C9_453D_A587_5C4CD0D5D017__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ThrTransacao.h : header file
//

#include "conexao.h"

// Comandos recebidos do cliente
//
#define CMD_RECEBEARQUIVO			1
#define CMD_ENVIAARQUIVO			2
#define CMD_TRANSACAO_RECEBE		3
#define CMD_TRANSACAO_ENVIA			4
#define CMD_ATUALIZACLIENTE			5
#define CMD_ALTERASENHA             6
#define CMD_EXCLUICLIENTE			7			
#define CMD_RELACIONA				8
#define CMD_EXCLUI_RELACAO 			9
#define CMD_ALTERACLIENTE			10
#define CMD_ATUALIZACXPOSTAL		500
#define CMD_FINALIZA_CONEXAO		999

/////////////////////////////////////////////////////////////////////////////
// CThrTransacao thread

class CThrTransacao : public CWinThread
{
	DECLARE_DYNCREATE(CThrTransacao)
public:
	CThrTransacao();           // protected constructor used by dynamic creation
	CThrTransacao(WPARAM wParam);
	virtual ~CThrTransacao();

	SOCKET sock_Trans;	

// Attributes
public:
	unsigned char *p_recv_buf_Trans;
	CConexaoCliente *p_ConexaoCliente;

	HANDLE h_FimThr;
	BYTE   Mensagem[200];
	BOOL Logged;
	CWinThread *p_Parent;	

// Operations
public:
	void TrataTransacoes();
	void FINALIZA();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThrTransacao)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	

	// Generated message map functions
	//{{AFX_MSG(CThrTransacao)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THRTRANSACAO_H__A74044C1_78C9_453D_A587_5C4CD0D5D017__INCLUDED_)
