#ifndef _DECOMPRESS_H_
#define _DECOMPRESS_H_

#include "StdAfx.h"
#include "Upload.h"
#include "LzcDefs.h"
                                                                                
class CDecompress  
{
public:
	int Decompress(Upload *up, int idArq, BYTE *BufferIn, DWORD nBytes);
	CDecompress(bool permiteCompressao);
	virtual ~CDecompress();

protected:
	void Reset();

	bool mPermiteCompressao;
	int NoDecompress();
	int FlushIOBuffer();
	DWORD m_nBytesInIOBuffer;
	BYTE m_IOBuffer[TAM_IO_BUFFER];
	DWORD m_BytesGravados;
	DWORD m_iPosBuffer;
	int Write(BYTE Codigo);
	DWORD m_nBytes;
	BYTE * m_pBufferIn;
	short int m_Char;
	WORD m_OldCode;
	int PutcNcr(WORD c);
	int Read();
	WORD InputCode();
	unsigned char* DecodeString(BYTE *Buffer, WORD Codigo);
	short int m_Lastc;
	unsigned short int m_NextCode;
	short int m_Erro;
	unsigned char m_InpBitCount;
	unsigned char m_State;
	short int m_MaxCode;
	short int m_NumBits;
	unsigned long m_InputBitBuffer;
	BOOL m_ClearFlag;
	BYTE m_CaracSfx[TABLE_SIZE];
	unsigned short int m_CodPfx[TABLE_SIZE];
	BYTE m_DeStack[DE_STACK_SIZE];
	Upload *u;
	int idarq;
};

#endif // 
