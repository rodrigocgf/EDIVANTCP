#ifndef _UPLOAD_H_
#define _UPLOAD_H_

#include "StdAfx.h"
#include "Parm.h"
#include "Config.h"
#include "MySQL7.h"
#include "Arquivo.h"

class Upload: public Arquivo  
{
public:
	int GravarBloco(int idArquivo, char *Bloco, int Size);
	int EncerraUpload();
	int getStatus(Parametros* Parm);
	Upload(CConfig* cfg);
	virtual ~Upload();

private:
	bool TransmissaoNormal;
	bool Retransmissao;
};

#endif // 
