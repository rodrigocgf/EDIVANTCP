#include "Conexao.h"
#include "LogApiFunc.h"
#include "BFCrypt.h"
#include "MySQL7.h"
#include "Upload.h"
#include "Download.h"
#include "Decompress.h"
#include "Compress.h"
#include "ThrTransacao.h"
#include "ThrListen.h"
#include "erros.h"

#define SENHA_DEFAULT	"EDIVAN_BAYER"

extern CLogApiFunc logapi;

CConexaoCliente::CConexaoCliente(SOCKET hClientSocket)
{
	logapi.logtrc("CConexaoCliente().\r\n"); 
	// Cria o objeto para a comunicacao com o cliente
	mClientSocket = new CTcpSocket();
	mClientSocket->Attach(hClientSocket);
	mClientSocket->Config();
	
	mFirstTime = TRUE;
	bool opt=TRUE;
	mClientSocket->SetSockOpt(TCP_NODELAY, &opt, sizeof(opt), IPPROTO_TCP);
	
	p_Config = new CConfig("EDIVANTCP");
	
	memset(mMyHost, 0, sizeof(mMyHost));
	memset(mMyUser, 0, sizeof(mMyUser));
	memset(mMyPwd, 0, sizeof(mMyPwd));
	memset(mMyDb, 0, sizeof(mMyDb));
	strcpy(mMyHost, p_Config->BdServer);
	strcpy(mMyDb, p_Config->BdName);
	strcpy(mMyUser, p_Config->BdUsuario);
	strcpy(mMyPwd, p_Config->BdSenha);
	
	mCompressao = p_Config->bPermiteCompressao;
	mRestart = p_Config->bPermiteRestart;
	setidSessao();
}

CConexaoCliente::~CConexaoCliente()
{
	if(p_Config)
		delete p_Config;
	if(mClientSocket != NULL)
		delete mClientSocket;

	logapi.logtrc("~CConexaoCliente().\r\n");
}

bool CConexaoCliente::ConsisteChaveEntrada()
{
	char sChaveEntrada[TAM_DOMINIO + 1];
	logapi.logtrc("CConexaoCliente >> Ip Cliente: [%s]", mClientSocket->mClientAddress);
	
	memset(sChaveEntrada,0x00,sizeof(sChaveEntrada) );
	if(mClientSocket->ReceiveCrypt(sChaveEntrada, TAM_DOMINIO) <= 0)
	{
		strcpy(mErro, ERR_SOCKET);
		if(mClientSocket->ObtemUltimoErro() != 999)
		{
			logapi.logtrc("CConexaoCliente >> Erro recebendo chave de entrada.\r\n");
		}
		logapi.logtrc("CConexaoCliente >> Erro de socket : %d", mClientSocket->ObtemUltimoErro());
		return false;
	}
	RTrim(sChaveEntrada);	
	logapi.logtrc("CConexaoCliente >> Chave de entrada recebida.\r\n");
	 
	RESET_TIMER();

	// Verifica se a chave esta correta
	if(!verificaDominio(sChaveEntrada))
	{
		strcpy(mErro, ERR_CHAVEINVALIDA);
		enviaCodigoErro(ERR_CHAVEINVALIDA);
		logapi.logtrc("%s Chave de entrada invalida: [%s]", USUARIO_SISTEMA,sChaveEntrada);
		return false;
	}
	
	// Avisa que a operacao foi bem sucedida
	enviaSucessoOperacao();
	if(mClientSocket->ObtemUltimoErro())
	{
		strcpy(mErro, ERR_SOCKET);
		if(mClientSocket->ObtemUltimoErro() != 999)
		{
			logapi.logtrc("CConexaoCliente >> %s Erro ao confirmar chave de entrada.", USUARIO_SISTEMA);
		}
		logapi.logtrc("CConexaoCliente >> %s %d", USUARIO_SISTEMA, mClientSocket->ObtemUltimoErro());
		return false;
	}
	return true;
}

bool CConexaoCliente::ConsisteUsuarioSenha()
{
	char sResposta[100];
	char sSenha[TAM_SENHA + 1];
	char sVersao[TAM_VERSAO + 1];
	char sBuffer[256];
	char now[30];

	memset( sResposta, 0x00 , sizeof(sResposta ) );
	memset( sSenha, 0x00 , sizeof(sSenha ) );
	memset( sVersao, 0x00 , sizeof(sVersao ) );
	memset( sBuffer, 0x00 , sizeof(sBuffer ) );
	
	memset(mUsuario, 0, sizeof(mUsuario));
	strcpy(mUsuario, USUARIO_SISTEMA);
	
	// Recebe usuario e senha
	if(mClientSocket->ReceiveCrypt(sBuffer, TAM_USUARIOSENHAVERSAO) <= 0)
	{
		strcpy(mErro, ERR_SOCKET);
		if(mClientSocket->ObtemUltimoErro() != 999)
			logapi.logtrc("%s Erro recebendo dados de logon.", USUARIO_SISTEMA);
		logapi.logtrc("%s %d", USUARIO_SISTEMA, mClientSocket->ObtemUltimoErro());
		return false;
	}

	RESET_TIMER();
	
	//Separa dados de usuario e senha e versao
	strncpy(mUsuario, sBuffer, TAM_USUARIO);
	strncpy(sSenha, &sBuffer[TAM_USUARIO], TAM_SENHA);
	RTrim(mUsuario);
	RTrim(sSenha);
	strlwr(sSenha);
	strcpy(mCliente,mUsuario);
	// Usuario = [CodUsuario@ClienteDominio] : para n�o repetir no edivan
	strcat(mUsuario,"@");
	strcat(mUsuario,mClienteDominio);
	strlwr(mUsuario);

	strncpy(sVersao, &sBuffer[TAM_USUARIO + TAM_SENHA], TAM_VERSAO);
	
	logapi.logtrc("Atendendo usuario %s.", mUsuario);
	logapi.logtrc("%s Versao do MV7 do usuario = %d.%02d.", mUsuario,
		StringToInt(sVersao, 2), StringToInt(&sVersao[2], 2));
	
	//Verifica integridade do usuario
	if(atol(sVersao) < MENOR_VERSAO_COMPATIVEL){
		strcpy(mErro, ERR_VERSAO_INCOMPATIVEL);
		enviaCodigoErro(ERR_VERSAO_INCOMPATIVEL);
		logapi.logtrc("%s Versao do usuario incompativel com a Central.", mUsuario);
		return false;
	}
    	
	memset( now , 0x00 , sizeof(now) );
	GetDataHoraMySql(now);
	GravaLogNavegacao(GRUPO_ACESSO,"Login",now,now,0);
	
	logapi.logtrc("Usuario = %s ; Senha = %s ",mUsuario,sSenha);
	switch(verificaLogin(mUsuario, sSenha))
	{
	case -2:
		strcpy(mErro, SENHA_INVALIDA);
		GravaLogNavegacao(GRUPO_ACESSO,"Logout",now,now,atoi(SENHA_INVALIDA));
		enviaCodigoErro(SENHA_INVALIDA);
		logapi.logtrc("%s Senha incorreta.", mUsuario);
		return FALSE;
	case -1:
		GravaLogNavegacao(GRUPO_ACESSO,"Logout",now,now,-1);
		logapi.logtrc("%s Erro de login com MySQL.", mUsuario);
	case 0:
		strcpy(mErro, USUARIO_INVALIDO);
		GravaLogNavegacao(GRUPO_ACESSO,"Logout",now,now,atoi(USUARIO_INVALIDO));
		enviaCodigoErro(USUARIO_INVALIDO);
		logapi.logtrc("%s Usuario invalido.", mUsuario);
		return FALSE;
	}
	// Avisa que a opera��o foi bem sucedida
	enviaSucessoOperacao();
	
	if(mClientSocket->ObtemUltimoErro())
	{
		strcpy(mErro, ERR_SOCKET);
		if(mClientSocket->ObtemUltimoErro() != 999)
			logapi.logtrc("%s Erro ao confirmar logon.", mUsuario);
		logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
		return false;
	}
	
	// Enviando versao e permissao para restart e compressao
	sprintf(sResposta, "%-*.*s%1d%1d%-*.*s", TAM_VERSAO, TAM_VERSAO, VERSAO_CENTRAL,
		mCompressao ? 1 : 0, mRestart ? 1 : 0,
		TAM_RESERVADO, TAM_RESERVADO, "");
	logapi.logtrc(">>> [%s]", sResposta);
	mClientSocket->Send(sResposta, strlen(sResposta));
	if(mClientSocket->ObtemUltimoErro())
	{
		strcpy(mErro, ERR_SOCKET);
		if(mClientSocket->ObtemUltimoErro() != 999)
			logapi.logtrc("%s Erro ao enviar numero da versao.", mUsuario);
		logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
		return false;
	}
	return true;
}

int CConexaoCliente::AguardaTransacao(long lTimeOut)
{
	char sTipoTransacao[TAM_TIPOTRANSACAO + 1];
	
	//Limpa Buffer de Erro	
	memset(mErro, 0, sizeof(mErro));
	memset(sTipoTransacao,0x00,sizeof(sTipoTransacao) );
	
	// Verifica se tem alguma coisa para receber
	/*
	if(!mClientSocket->WaitToReceive(lTimeOut))
	{
		switch(mClientSocket->ObtemUltimoErro())
		{
		case WSAETIMEDOUT:
			return 0;
		case 999:
			logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
			return -1;
		default:
			strcpy(mErro, ERR_SOCKET);
			logapi.logtrc("AguardaTransacao >> %s Erro esperando pelo codigo da transacao...", mUsuario);
			logapi.logtrc("AguardaTransacao >> %s %d", mUsuario, mClientSocket->ObtemUltimoErro());
			return -1;
		}
	}
	*/

	// Recebe a transacao
	STransacao Transacao;
	memset(&Transacao, 0, sizeof(Transacao));
	if(mClientSocket->Receive((char *)&Transacao, TAM_TIPOTRANSACAO) <= 0)
	{
		strcpy(mErro, ERR_SOCKET);
		if(mClientSocket->ObtemUltimoErro() != 999)
			logapi.logtrc("AguardaTransacao >> %s Erro ao receber codigo da transacao.", mUsuario);
		logapi.logtrc("AguardaTransacao >> %s %d", mUsuario, mClientSocket->ObtemUltimoErro());
		return -1;
	}
	// Avisa que a operacao foi bem sucedida
	enviaSucessoOperacao();
	if(mClientSocket->ObtemUltimoErro())
	{
		strcpy(mErro, ERR_SOCKET);
		if(mClientSocket->ObtemUltimoErro() != 999)
			logapi.logtrc("%s Erro ao confirmar recebimento do codigo da transacao.",
			mUsuario);
		logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
		return -1;
	}
	memcpy(sTipoTransacao, Transacao.Tipo, TAM_TIPOTRANSACAO);
	//logapi.logtrc("%s: trid: %d", mUsuario, atoi(sTipoTransacao));
	return atoi(sTipoTransacao);
}

//============================================================
// Retorna o codigo do ultimo erro ocorrido
//============================================================
const char *CConexaoCliente::ObtemUltimoErroCon()
{
	return mErro;
}

//============================================================
// Retorna o ultimo erro de Socket ocorrido
//============================================================
int CConexaoCliente::ObtemErroSocket()
{
	return mClientSocket->ObtemUltimoErro();
}

bool CConexaoCliente::verificaDominio(const char *sChaveEntrada)
{
	string qry = "SELECT CodDominio,TamCodCliente FROM Dominio WHERE CodClienteDominio='";
	qry += sChaveEntrada;
	qry += "'";
	logapi.logtrc("%s",qry.c_str());
	MySQL7 *my = new MySQL7(mMyHost, mMyUser, mMyPwd, mMyDb);
	if(!my->isConnected())
	{
		logapi.logtrc("%s Erro de conexao com MySQL.", USUARIO_SISTEMA);
		delete my;
		return false;
	}
	if(!my->ExecSQL(qry.c_str()))
	{
		logapi.logtrc("%s Erro de execucao de query MySQL:[%s].", USUARIO_SISTEMA,qry.c_str());
		delete my;
		return false;
	}
	if(my->getInt(0) <= 0)
	{
		logapi.logtrc("%s Dominio inexistente: %s.", USUARIO_SISTEMA,sChaveEntrada);
		delete my;
		return false;
	}
	mDominio=my->getInt("CodDominio");
	mTamCodCliente=my->getInt("TamCodCliente");
	strncpy(mClienteDominio,sChaveEntrada,sizeof(mClienteDominio));
	delete my;
	return true;
}

/*
int CConexaoCliente::verificaLogin(const char *usr, const char *pwd)
{
	char sDominio[11];
	char dig[2 + 1];
	char strNofTentativas[10 + 1];
	char sz_Query[600];

	sprintf(sDominio,"%10.10d",mDominio);
	string qry = 
		"SELECT a.CodEdivan, a.Senha, a.TentSenhaInv, a.Nome, a.FlgTrocaSenha, "
		"b.CodDominio, b.TipoUsuario, b.CodCliente, d.NomeCliente, "
		"c.ChaveDominio, a.Nome, c.NomeDominio, a.Bloqueado, a.MotivoBloqueio "
		"From Usuario a, UsuarioPerfil b , Dominio c, ClienteEdivan d "
		"WHERE a.CodUsuario = b.CodUsuario And a.CodUsuario = '";
	qry += usr;
	qry += "' ";
	qry +=
		"And b.CodDominio = c.CodDominio "
		"And d.CodEdivan = a.CodEdivan "
		"And b.CodDominio = ";
	qry += sDominio;
	

	memset(sz_Query, 0x00 , sizeof(sz_Query) );
	memcpy( (char *)sz_Query , qry.c_str() , strlen(qry.c_str()) );
	logapi.logdump("verificalogin >> QUERY SQL :", sz_Query , strlen(sz_Query) );

	MySQL7 *my = new MySQL7(mMyHost, mMyUser, mMyPwd, mMyDb);
	if(!my->isConnected())
	{
		logapi.logtrc("%s Erro de conexao com MySQL.", usr);
		delete my;
		return -1;
	}
	if(!my->ExecSQL(qry.c_str()))
	{
		logapi.logtrc("%s Erro de execucao de query MySQL:[%s].", usr,qry.c_str());
		delete my;
		return -1;
	}
	if(my->getRowCount() <= 0)
	{
		logapi.logtrc("%s Usuario inexistente.", usr);
		delete my;
		return -1;
	}

	const char *codCliente = my->getString("CodCliente");
	memset(mCodCliente, 0, sizeof(mCodCliente));
	strncpy(mCodCliente, codCliente, 30);

	const char *sBloqueado = my->getString("Bloqueado");
	bool Bloqueado = (!strncmp(sBloqueado,"Total",strlen(sBloqueado))) || 
					 (!strncmp(sBloqueado,"Terminal Server",strlen(sBloqueado)));

	const char *sMotivoBloqueio = my->getString("MotivoBloqueio");

	int codEdivan = my->getInt("CodEdivan");
	mCodEdivan = codEdivan;

	const char *senhaBase = my->getString("Senha");

	int nofTentativas = my->getInt("TentSenhaInv");
	if ( Bloqueado ) {
	    logapi.logtrc("Usuario %s  Bloqueado, Motivo: %s", usr,sMotivoBloqueio);
		delete my;
		return 0;
	} else {
		time_t now = time(NULL);
		struct tm *tmNow = localtime(&now);
		qry = "";
		
		CBFCrypt *bfc = new CBFCrypt();

		string key = usr;
		key += "<m5n&kh^jgfD7so+iTu6?4gfbv*lJG#FGHgnGGH=@1d";
		bfc->SetKey(key.c_str(), strlen(key.c_str()));

		char *sc = new char[strlen(pwd)];

		memset(sc, 0, strlen(pwd));
		bfc->Crypt(pwd, sc, strlen(pwd));
		string senhaCrypt = "0x";

		for ( int i = 0; i < strlen(pwd); i++ ) {			
			memset(dig , 0x00 , sizeof(dig) );
			sprintf(dig, "%2.2x", (unsigned char) sc[i]);
			senhaCrypt += dig;
		}

		logapi.logtrc("Senhas [BASE DE DADOS] [CRYPT]: [%s] [%s]",senhaBase, senhaCrypt.c_str());

		if ( !strcmp(senhaBase, senhaCrypt.c_str()) ) { // same		
			qry =  "Update Usuario set TentSenhaInv = 0";
			qry += "   where CodUsuario = '";
			qry += usr;
			qry += "'";

			if ( !my->ExecSQL(qry.c_str()) ) {
				logapi.logtrc("%s Erro de update(0.0) do MySQL.", usr);
				delete [] sc;
				delete bfc;
				delete my;
				return -1;
			}

			delete [] sc;
			delete bfc;
		} else {
			logapi.logtrc("(%s)Senha de acesso invalida, tentativas: %d", usr , (nofTentativas+1) );
			qry =  "Update Usuario set TentSenhaInv = ";
			nofTentativas++;
			
			memset(strNofTentativas , 0x00 , sizeof(strNofTentativas) );
			sprintf(strNofTentativas, "%d", nofTentativas);
			qry += strNofTentativas;

			if (nofTentativas >= 3) { // Bloqueia o usu�rio			
				qry += ", Bloqueado='Total', MotivoBloqueio='Senha', DataBloqueio=now() ";
			    logapi.logtrc("Usuario %s foi bloqueado por erros de senha" , usr );
			}

			qry += " where CodUsuario = '";
			qry += usr;
			qry += "'";

			if ( !my->ExecSQL(qry.c_str()) ) {
				logapi.logtrc("%s Erro de update(2.0) do MySQL.", usr);
			}

			if ( my->getRowCount() != 1 ) {
				logapi.logtrc("%s Erro de update(2.1) do MySQL.", usr);
			}

			delete [] sc;
			delete bfc;
			delete my;
			return -2;
		}
	}

	delete my;
	return 1;
}
*/

int CConexaoCliente::verificaLogin(const char *usr, const char *pwd)
{
	char sDominio[11];
	char dig[2 + 1];
	char strNofTentativas[10 + 1];
	char sz_Query[600];

	sprintf(sDominio,"%10.10d",mDominio);
	string qry = 
		"SELECT a.CodEdivan, a.Senha, a.TentSenhaInv, a.Nome, a.FlgTrocaSenha, "
		"b.CodDominio, b.TipoUsuario, b.CodCliente, d.NomeCliente, "
		"c.ChaveDominio, a.Nome, c.NomeDominio, a.Bloqueado, a.MotivoBloqueio "
		"From Usuario a, UsuarioPerfil b , Dominio c, ClienteEdivan d "
		"WHERE a.CodUsuario = b.CodUsuario And a.CodUsuario = '";
	qry += usr;
	qry += "' ";
	qry +=
		"And b.CodDominio = c.CodDominio "
		"And d.CodEdivan = a.CodEdivan "
		"And b.CodDominio = ";
	qry += sDominio;
	

	memset(sz_Query, 0x00 , sizeof(sz_Query) );
	memcpy( (char *)sz_Query , qry.c_str() , strlen(qry.c_str()) );
	logapi.logdump("verificalogin >> QUERY SQL :", sz_Query , strlen(sz_Query) );

	MySQL7 *my = new MySQL7(mMyHost, mMyUser, mMyPwd, mMyDb);
	if(!my->isConnected())
	{
		logapi.logtrc("%s Erro de conexao com MySQL.", usr);
		delete my;
		return -1;
	}
	if(!my->ExecSQL(qry.c_str()))
	{
		logapi.logtrc("%s Erro de execucao de query MySQL:[%s].", usr,qry.c_str());
		delete my;
		return -1;
	}
	if(my->getRowCount() <= 0)
	{
		logapi.logtrc("%s Usuario inexistente.", usr);
		delete my;
		return -1;
	}

	const char *codCliente = my->getString("CodCliente");
	memset(mCodCliente, 0, sizeof(mCodCliente));
	strncpy(mCodCliente, codCliente, 30);

	const char *sBloqueado = my->getString("Bloqueado");
	bool Bloqueado = (!strncmp(sBloqueado,"Total",strlen(sBloqueado))) || 
					 (!strncmp(sBloqueado,"Terminal Server",strlen(sBloqueado)));

	const char *sMotivoBloqueio = my->getString("MotivoBloqueio");

	int codEdivan = my->getInt("CodEdivan");
	mCodEdivan = codEdivan;

	const char *senhaBase = my->getString("Senha");

	int nofTentativas = my->getInt("TentSenhaInv");

	logapi.logtrc("verificalogin >> FIM DA QUERY SQL .\r\n");

	if ( Bloqueado ) {
	    logapi.logtrc("Usuario %s  Bloqueado, Motivo: %s", usr,sMotivoBloqueio);
		delete my;
		return 0;
	} else {
		time_t now = time(NULL);
		struct tm *tmNow = localtime(&now);
		qry = "";
		
		CBFCrypt *bfc = new CBFCrypt();

		string key = usr;
		key += "<m5n&kh^jgfD7so+iTu6?4gfbv*lJG#FGHgnGGH=@1d";
		bfc->SetKey(key.c_str(), strlen(key.c_str()));

		char *sc = new char[strlen(pwd)];

		memset(sc, 0, strlen(pwd));
		bfc->Crypt(pwd, sc, strlen(pwd));
		string senhaCrypt = "0x";

		for ( int i = 0; i < strlen(pwd); i++ ) {			
			memset(dig , 0x00 , sizeof(dig) );
			sprintf(dig, "%2.2x", (unsigned char) sc[i]);
			senhaCrypt += dig;
		}

		logapi.logtrc("Senhas [BASE DE DADOS] [CRYPT]: [%s] [%s]",senhaBase, senhaCrypt.c_str());

		// Verifica se o usu�rio � da Bayer ou n�o 
		if ( UsuarioBayer(usr, my) ) {
			if ( !strcmp(senhaBase,SENHA_DEFAULT) ) {
				// Se a senha na base � a DEFAULT , ent�o efetua-se o UPDATE
				char sz_cmd[MAX_CMD_LEN];
				
				memset(sz_cmd,0,sizeof(sz_cmd));
				sprintf(sz_cmd,
								"Update Usuario set Senha='%s', DataUltAlt=now() "
								"where CodUsuario='%s'",
								(char *)senhaCrypt.c_str(), usr);

				//logapi.logtrc("verificaLogin >> USUARIO BAYER . QUERY : %s\r\n",sz_cmd);
				logapi.logdump("verificaLogin >> USUARIO BAYER . QUERY : ",sz_cmd,strlen(sz_cmd) );

				if ( !my->ExecSQL(sz_cmd) ) {
					logapi.logtrc("verificaLogin >> Erro na atualiza��o da senha.");
					enviaErroOperacao();
					delete [] sc;
					delete bfc;
					delete my;
					return -1;
				}

				delete [] sc;
				delete bfc;
				//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
			} else {				
				// Verifica normalmente a senha

				if ( !strcmp(senhaBase, senhaCrypt.c_str()) ) { // same		
					qry =  "Update Usuario set TentSenhaInv = 0";
					qry += "   where CodUsuario = '";
					qry += usr;
					qry += "'";

					if ( !my->ExecSQL(qry.c_str()) ) {
						logapi.logtrc("%s Erro de update(0.0) do MySQL.", usr);
						delete [] sc;
						delete bfc;
						delete my;
						return -1;
					}

					delete [] sc;
					delete bfc;
				} else {
					logapi.logtrc("(%s)Senha de acesso invalida, tentativas: %d", usr , (nofTentativas+1) );
					qry =  "Update Usuario set TentSenhaInv = ";
					nofTentativas++;
					
					memset(strNofTentativas , 0x00 , sizeof(strNofTentativas) );
					sprintf(strNofTentativas, "%d", nofTentativas);
					qry += strNofTentativas;

					if (nofTentativas >= 3) { // Bloqueia o usu�rio			
						qry += ", Bloqueado='Total', MotivoBloqueio='Senha', DataBloqueio=now() ";
						logapi.logtrc("Usuario %s foi bloqueado por erros de senha" , usr );
					}

					qry += " where CodUsuario = '";
					qry += usr;
					qry += "'";

					if ( !my->ExecSQL(qry.c_str()) ) {
						logapi.logtrc("%s Erro de update(2.0) do MySQL.", usr);
					}

					if ( my->getRowCount() != 1 ) {
						logapi.logtrc("%s Erro de update(2.1) do MySQL.", usr);
					}

					delete [] sc;
					delete bfc;
					delete my;
					return -2;
				}
				
			}
		} else {
			if ( !strcmp(senhaBase, senhaCrypt.c_str()) ) { // same		
				qry =  "Update Usuario set TentSenhaInv = 0";
				qry += "   where CodUsuario = '";
				qry += usr;
				qry += "'";

				if ( !my->ExecSQL(qry.c_str()) ) {
					logapi.logtrc("%s Erro de update(0.0) do MySQL.", usr);
					delete [] sc;
					delete bfc;
					delete my;
					return -1;
				}

				delete [] sc;
				delete bfc;
			} else {
				logapi.logtrc("(%s)Senha de acesso invalida, tentativas: %d", usr , (nofTentativas+1) );
				qry =  "Update Usuario set TentSenhaInv = ";
				nofTentativas++;
				
				memset(strNofTentativas , 0x00 , sizeof(strNofTentativas) );
				sprintf(strNofTentativas, "%d", nofTentativas);
				qry += strNofTentativas;

				if (nofTentativas >= 3) { // Bloqueia o usu�rio			
					qry += ", Bloqueado='Total', MotivoBloqueio='Senha', DataBloqueio=now() ";
					logapi.logtrc("Usuario %s foi bloqueado por erros de senha" , usr );
				}

				qry += " where CodUsuario = '";
				qry += usr;
				qry += "'";

				if ( !my->ExecSQL(qry.c_str()) ) {
					logapi.logtrc("%s Erro de update(2.0) do MySQL.", usr);
				}

				if ( my->getRowCount() != 1 ) {
					logapi.logtrc("%s Erro de update(2.1) do MySQL.", usr);
				}

				delete [] sc;
				delete bfc;
				delete my;
				return -2;
			}
		} // fim do else do Usu�rio Bayer

	} // fim do else de usu�rio n�o bloqueado.

	delete my;
	return 1;
}

bool CConexaoCliente::UsuarioBayer(const char *p_usuario , MySQL7 *p_mysql)
{
	bool b_ret;

	b_ret = false;

	if ( p_usuario != NULL) {
		if ( strlen(p_usuario) > 0 ) {
			if ( strstr(p_usuario,"bayer") ) {
				b_ret = true;
			}
		}
	}

	return b_ret;
}

bool CConexaoCliente::enviaSucessoOperacao()
{
	// Avisa que a operacao foi bem sucedida
	mClientSocket->Send(SUCESSO_NA_OPERACAO, TAM_COMANDO);
	
	if ( mClientSocket->ObtemUltimoErro() )
		return false;

	return true;
}

bool CConexaoCliente::enviaErroOperacao()
{
	// Avisa que ocorreu erro na operacao
	mClientSocket->Send(ERRO_NA_OPERACAO, TAM_COMANDO);
	if(mClientSocket->ObtemUltimoErro())
		return false;
	return true;
}

bool CConexaoCliente::enviaSucessoOperacao(DWORD RestartPoint)
{
   char sResposta[20];

   memset(sResposta , 0x00 , sizeof(sResposta) );
   // Avisa que a operacao foi bem sucedida e indica o ponto de
   // restart do arquivo JT 23.11.98

   sprintf(sResposta, "%*.*s%-*lu", TAM_COMANDO, TAM_COMANDO, SUCESSO_NA_OPERACAO,
					CTR_TAM_FILE_SIZE, RestartPoint);

   mClientSocket->Send(sResposta, strlen(sResposta));
   if(mClientSocket->ObtemUltimoErro())
      return false;
   return true;
}

bool CConexaoCliente::enviaCodigoErro(const char *sCodigo)
{
   // Avisa que ocorreu erro na operacao
   mClientSocket->Send(sCodigo, TAM_COMANDO);
   if(mClientSocket->ObtemUltimoErro())
      return false;
   return true;
}

//------------------------------------------------------------------------------------------

bool CConexaoCliente::RecebeArq()
{
	int bytesGravados;
	char fileOrigem[CTR_TAM_NOME_ORIGINAL + 1];
	char fileDestino[CTR_TAM_CXP_DESTINO + 1];
	struct Parametros para;
	CTRSTRUCT ctrStruct;
	long restartPoint;
	long fileSize;
	char fileDate[8 + 1];
	char fileTime[6 + 1];
	int bytesTrafegados;
	SBlocoArquivo file;

	RESET_TIMER();

	memset(fileOrigem,0,sizeof(fileOrigem));
	memset(fileDestino,0,sizeof(fileDestino));
	memset(fileDate,0,sizeof(fileDate));
	memset(fileTime,0,sizeof(fileTime));

	logapi.logtrc("-- INICIO DE RECEBIMENTO DE ARQUIVO --");
	//Limpa Buffer de Erro	
	memset(mErro, 0, sizeof(mErro));
	// Verifica se tem alguma coisa para receber
	/*
	if( !mClientSocket->WaitToReceive(TIMEOUT_RECEIVE) ) {
		if ( mClientSocket->ObtemUltimoErro() )
			strcpy( mErro , ERR_SOCKET );
		else
			strcpy( mErro , ERR_TIMEOUT );
		if ( mClientSocket->ObtemUltimoErro() != 999 )
			logapi.logtrc("%s Erro aguardando dados do arquivo.", mUsuario);

		logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro() );
		return false;
	}
	*/

	// Recebe a estrutura	
	memset(&ctrStruct, 0, sizeof(ctrStruct));
	if ( mClientSocket->Receive( (char *)&ctrStruct , sizeof(ctrStruct) ) <= 0 ) {
		
		//logapi.logtrc("%s ClientSocket->Receive <= 0!!!", mUsuario);
		logapi.logtrc("RecebeArq >> Erro de socket %ld recebendo a estrutura.\r\n",WSAGetLastError() );
		
		strcpy( mErro , ERR_SOCKET );

		if ( mClientSocket->ObtemUltimoErro() != 999 )
			logapi.logtrc("%s Erro aguardando dados do arquivo.", mUsuario);

		logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
		return false;
	}
	logapi.logdump("RecebeArq >> Estrutura recebida : ",(char *)&ctrStruct,sizeof(ctrStruct));

	RESET_TIMER();

	// Preenche param	
	StringNCopy(fileOrigem, ctrStruct.sNomeOriginal, CTR_TAM_NOME_ORIGINAL);
	StringNCopy(fileDestino, ctrStruct.sDestino, CTR_TAM_CXP_DESTINO);

	fileSize = StringToLong(ctrStruct.sTamanho, CTR_TAM_FILE_SIZE);
	
	StringNCopy(fileDate, ctrStruct.sData, 8);
	StringNCopy(fileTime, ctrStruct.sHora, 6);
		
	// Limpa a estrutura de par�metros
	memset(&para, 0, sizeof(para));

	para.CodDominio = mDominio;
	if ( !strncmp( fileDestino , CAIXA_PUBLICA , CTR_TAM_CXP_DESTINO) ) // Publico
		strcpy(para.CodFormato, "MV7PUB");
	else {
		strcpy(para.CodFormato, "MV7BIN");
	 	if ( mTamCodCliente > 0 && mTamCodCliente < CTR_TAM_CXP_DESTINO + 1 )
	    	strncpy(para.NomeDestino, &fileDestino[8-mTamCodCliente],mTamCodCliente);
		else {
			logapi.logtrc(	"%s Erro no Upload: Tamanho do Codigo do Cliente invalido [%d]",\
							mUsuario,mTamCodCliente);
			return false;
		}
	}

	logapi.logtrc("Conexao::NomeDestino: [%s]",para.NomeDestino);
	strcpy(para.CodUsuario, mUsuario);
	sprintf(para.NomeOrigem,"%s\\%s", fileDestino,fileOrigem);
	para.TamanhoOrigem = (int) fileSize;
	strcpy(para.ModoOperacao, "Nor"); // normal a princ�pio?
	strcpy(para.CodCliente, mCodCliente);	

	para.DataHoraOrigem.tm_year = StringToInt(&fileDate[4], 4) - 1900;
	para.DataHoraOrigem.tm_mon = StringToInt(&fileDate[2], 2) - 1;
	para.DataHoraOrigem.tm_mday = StringToInt(fileDate, 2);
	para.DataHoraOrigem.tm_hour = StringToInt(fileTime, 2);
	para.DataHoraOrigem.tm_min = StringToInt(&fileTime[2], 2);
	para.DataHoraOrigem.tm_sec = StringToInt(&fileTime[4], 2);
	
	Upload up(p_Config);
	logapi.logtrc("Conexao::RecebeArq(): instanciou objeto Upload");
	CDecompress dz(mCompressao);
	logapi.logtrc("Conexao::RecebeArq(): instanciou objeto CDecompress(%d)", mCompressao);

	int erro = up.getStatus(&para);
	if ( erro ) {
		logapi.logtrc("%s Erro no Upload: %d",mUsuario,erro);
		enviaErroOperacao();
		strcpy(mErro, ERR_SOCKET);
		if( mClientSocket->ObtemUltimoErro() )
     		logapi.logtrc("Erro de socket %d", mClientSocket->ObtemUltimoErro());
		return false;
	}

	para.idArquivo = up.idArquivo;
	restartPoint = up.getRestartPoint();

	logapi.logtrc("RecebeArq >> restartPoint = %d ",restartPoint );
	logapi.logtrc("RecebeArq >> Arquivo a ser recebido (NomeOrigem): %s",para.NomeOrigem);
	logapi.logtrc("RecebeArq >> CAIXA POSTAL DE DESTINO = %s ",para.CodCliente );
	
	// Avisa que a operacao foi bem sucedida
	enviaSucessoOperacao(restartPoint);

	logapi.logtrc("CConexaoCliente >> Envia RESTART POINT %ld",restartPoint);

	if ( mClientSocket->ObtemUltimoErro() ) {
		strcpy(mErro, ERR_SOCKET);
		if( mClientSocket->ObtemUltimoErro() != 999 )
			logapi.logtrc("%s Erro ao enviar RESTART POINT do arquivo.", mUsuario);

		logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
		return false;
	}
	
	bytesTrafegados = restartPoint;	

	logapi.logtrc("CConexaoCliente >> Recebendo arquivo ...");
	while( bytesTrafegados < fileSize ) {
		RESET_TIMER();

		memset(&file, 0, sizeof(file));
		/*
		if( !mClientSocket->WaitToReceive( TIMEOUT_RECEIVE ) ) {
			enviaErroOperacao();
			if(mClientSocket->ObtemUltimoErro())
				strcpy(mErro, ERR_SOCKET);
			else
				strcpy(mErro, ERR_TIMEOUT);
			if ( mClientSocket->ObtemUltimoErro() != 999 ) {
				logapi.logtrc("CConexaoCliente::RecebeArq >> Erro na WaitToReceive.");
			}

			logapi.logtrc("Erro de socket  %d", mClientSocket->ObtemUltimoErro());			
			return false;
		}
		*/

		// Recebe o tamanho do bloco trafegado
		if( mClientSocket->Receive( file.Tamanho , sizeof(file.Tamanho) ) <= 0 ) {			
			strcpy(mErro, ERR_SOCKET);
			if ( mClientSocket->ObtemUltimoErro() != 999 ) 
				logapi.logtrc(	"CConexaoCliente::RecebeArq >> Erro na Receive ao receber "\
								"tamanho do bloco de dados.\r\n");				

			logapi.logtrc("Erro de socket %d", mClientSocket->ObtemUltimoErro());
			
			return false;
		}

		int blockSize = StringToInt(file.Tamanho, sizeof(file.Tamanho));

		if ( mClientSocket->Receive( file.sBuffer , blockSize ) <= 0 ) {
			
			strcpy(mErro, ERR_SOCKET);
			if(mClientSocket->ObtemUltimoErro() != 999)
				logapi.logtrc(	"CConexaoCliente::RecebeArq >> Erro na Receive "\
								"ao receber o arquivo.");								
			logapi.logtrc("Erro de socket %d", mClientSocket->ObtemUltimoErro());
			return false;
		}
		// Desencripta bloco do arquivo
		Encripta(file.sBuffer, file.sBuffer, blockSize);

		//Grava dados no arquivo
		if( (bytesGravados = dz.Decompress(	&up, 
											para.idArquivo, 
											(BYTE *) file.sBuffer, 
											blockSize)) < 0)
		{
			strcpy(mErro, ERR_GRAVANDOARQUIVO);
			enviaErroOperacao();
			logapi.logtrc("%s erro de gravacao; bytes gravados: %d", mUsuario, bytesGravados);
			return false;
		}

		bytesTrafegados += bytesGravados;
		//logapi.logtrc("%s Upload: bytesTrafegados: %7d", mUsuario, bytesTrafegados);
		logapi.logtrc("RecebeArq >> (%s) bytesRx: %7d", mUsuario, bytesTrafegados);
		//logapi.logtrc("EnviaArquivo >> (%s)  bytesTx: %7d", mUsuario, dl.getRestartPoint());

		if ( bytesTrafegados == fileSize ) {
			if ( up.GetStatusArquivo() == 'P' )
				logapi.logtrc("RecebeArq >> Antes de EncerraUpload() StatusArquivo = P.\r\n");
			logapi.logtrc("RecebeArq >> up.EncerraUpload().\r\n");
			erro = up.EncerraUpload();
			if ( erro ) {
				enviaErroOperacao();
				logapi.logtrc("%s erro na EncerraUpload: %d", mUsuario, erro);
				return false;
			}
			logapi.logtrc(	"%s Fim upload: %d, bytesTrafegados: %d", mUsuario, erro, \
							bytesTrafegados);
		}
				
		// Avisa que a operacao foi bem sucedida
		//logapi.logtrc("RecebeArq >> Envia SUCESSO_OPERACAO.\r\n");
		enviaSucessoOperacao();

		if ( mClientSocket->ObtemUltimoErro() ) {
			strcpy(mErro, ERR_SOCKET);
			if(mClientSocket->ObtemUltimoErro() != 999)
				logapi.logtrc(	"RecebeArq >> Erro %d no envio do pacote "\
								"SUCESSO_OPERACAO ap�s o recebimento do  "\
								"bloco de dados para o usu�rio %s.\r\n",\
								mClientSocket->ObtemUltimoErro() , mUsuario);
				//logapi.logtrc("RecebeArq >> Erro %d no envio do pacote SUCESSO_OPERACAO ap�s o recebimento do bloco de dados para o usu�rio %s.\r\n",mClientSocket->ObtemUltimoErro() , mUsuario);
				
			return false;
		}
	}

	logapi.logtrc("-- FIM DE RECEBIMENTO DE ARQUIVO --");
	return true;
}

//------------------------------------------------------------------------------------------


bool CConexaoCliente::EnviaArquivo()
{
	char rbuff[CTR_TAM_FILE_SIZE + 1];
	long restartPoint;
	int fileSize;
	char sNomeOriginal[CTR_TAM_NOME_ORIGINAL + 1];
	SBlocoArquivo fbuff;
	int nofcmp, rbyte;

	RESET_TIMER();

	logapi.logtrc("-- INICIO DE ENVIO DE ARQUIVO --");
	// Limpa Buffer de Erro	
	memset(mErro, 0, sizeof(mErro));
	// Lista download:
	if( mFirstTime ) {
		memset(&mFiles, 0, sizeof(mFiles));
		mFilesCnt = mFilesIdx = 0;
		
		// lista publicos:
		listaDownload(mCodCliente, mDominio, "MV7PUB");
		// lista privados:
		listaDownload(mCodCliente, mDominio, "MV7BIN");
		mFirstTime = false;
	} else {
		mFilesIdx++;
		if ( mFilesIdx == mFilesCnt - 1 )
			mFirstTime = true;
	}
	logapi.logtrc("mFileIdx:%d , mFilesCnt: %d",mFilesIdx,mFilesCnt);

	CTRSTRUCT filed = mFiles[mFilesIdx].CtrStruct;
	// envia estrutura:
	logapi.logdump(	"CConexaoCliente::EnviaArquivo >> Envia a estrutura ",\
					(char *)&filed, sizeof(filed));

	mClientSocket->Send( (const char *)&filed , sizeof(filed) );

	if ( mClientSocket->ObtemUltimoErro() ) {
		strcpy(mErro, ERR_SOCKET);
		if ( mClientSocket->ObtemUltimoErro() != 999 ) {
			logapi.logtrc("%s Erro ao enviar dados do arquivo.", mUsuario);
			logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
		} else {
			logapi.logtrc("%s Desconexao.", mUsuario);
		}
		return false;
	}

	RESET_TIMER();

	memset( rbuff,0,sizeof(rbuff) );	

	// recebe resposta:
	if ( mClientSocket->Receive(rbuff, 3) <= 0 ) {
		
		strcpy(mErro, ERR_SOCKET);
		if ( mClientSocket->ObtemUltimoErro() != 999 )
			logapi.logtrc("%s Erro ao receber confirmacao de dados do arquivo.", mUsuario);

		logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
		
		return false;
	}

	// Nao h� arquivos a enviar
	if ( !filed.sDestino[0] ) {
		logapi.logtrc("%s Nao ha arquivos a enviar.", mUsuario);
		return false;
	}

	// Verifica se a ponta recebeu a estrutura OK:
	if ( strncmp(rbuff, "000", 3) ) {
		enviaErroOperacao();
		logapi.logtrc("%s Operacao rejeitada: %s.", mUsuario, rbuff);
		return false;
	}

	// recebe restart point do pocket:
	memset(rbuff, 0, sizeof(rbuff));

	logapi.logtrc("CConexaoCliente::EnviaArquivo >> Espera receber o RESTART point do arquivo.");
	if ( mClientSocket->Receive(rbuff, CTR_TAM_FILE_SIZE) <= 0 ) {				
		strcpy(mErro, ERR_SOCKET);
		if ( mClientSocket->ObtemUltimoErro() != 999 ) {
			if ( mFilesCnt ) {
				logapi.logtrc("%s Erro ao receber ponto de restart do arquivo.", mUsuario);
				logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
			} else {
				logapi.logtrc("%s Nao ha arquivos a enviar.", mUsuario);
			}
		} else {
			logapi.logtrc("%s Desconexao.", mUsuario);
		}
		return false;
	}

	RESET_TIMER();
	
	restartPoint = StringToLong(rbuff, CTR_TAM_FILE_SIZE);
	fileSize = StringToInt(mFiles[mFilesIdx].CtrStruct.sTamanho, CTR_TAM_FILE_SIZE);
	Download dl(mFiles[mFilesIdx].idArquivo, p_Config);
	
	memset(sNomeOriginal,0,sizeof(sNomeOriginal) );
	StringNCopy(sNomeOriginal, mFiles[mFilesIdx].CtrStruct.sNomeOriginal, CTR_TAM_NOME_ORIGINAL);

	dl.getStatus("Nor", fileSize, sNomeOriginal);
	if ( dl.getRestartPoint() != restartPoint ) // alinhar conforme pocket
		dl.setRestartPoint(restartPoint);
	
	logapi.logtrc(	"CConexaoCliente::EnviaArquivo >> Arquivo a ser enviado (NomeDestino): %s",\
					mFiles[mFilesIdx].CtrStruct.sNomeOriginal);

	// Buffer de recepcao:	
	CCompress cmp( mCompressao );	
	
	logapi.logtrc("CConexaoCliente::EnviaArquivo >> Enviando arquivo ...");
	while ( true ) {
		memset(&fbuff, 0, sizeof(fbuff));
		// le bloco de download e comprime:
		if ( ( nofcmp = cmp.Compress(&dl, (BYTE *) fbuff.sBuffer,
			MAX_TAM_PACOTE, (unsigned long *) &rbyte)) < 0)
		{
			enviaErroOperacao();
			logapi.logtrc("%s Erro ao receber pacote do EDI.", mUsuario);
			return false;
		}

		if ( nofcmp == 0 ) {// EOF		
		    logapi.logtrc(	"%s Download concluido: bytesTrafegados: %7d", mUsuario, \
							dl.getRestartPoint());
			dl.termina();
			break;
		}
		
		// crypt:
		Encripta(fbuff.sBuffer, fbuff.sBuffer, nofcmp);
		sprintf(fbuff.Tamanho, "%d", nofcmp);
		
		//logapi.logtrc("%s Download: bytesTx: %7d", mUsuario, dl.getRestartPoint());
		logapi.logtrc("EnviaArquivo >> (%s)  bytesTx: %7d", mUsuario, dl.getRestartPoint());

		int sb = mClientSocket->Send((const char *) &fbuff, (15 + nofcmp) );

		if ( mClientSocket->ObtemUltimoErro() ) {
			strcpy(mErro, ERR_SOCKET);
			if ( mClientSocket->ObtemUltimoErro() != 999 ) {
				logapi.logtrc("EnviaArquivo >> %s Erro ao enviar pacote de dados do arquivo.", mUsuario);
				logapi.logtrc("EnviaArquivo >> %s %d", mUsuario, mClientSocket->ObtemUltimoErro());
			} else {
				logapi.logtrc("EnviaArquivo >> %s Desconexao.", mUsuario);
			}
			return false;
		}
		
		RESET_TIMER();

		memset(rbuff, 0, sizeof(rbuff));
		if ( mClientSocket->Receive(rbuff, 3) <= 0 ) {			
			strcpy(mErro, ERR_SOCKET);
			if ( mClientSocket->ObtemUltimoErro() != 999 ) {
				logapi.logtrc("EnviaArquivo >> %s Erro ao receber confirma��o de envio do pacote de dados do arquivo.", mUsuario);
//				logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
				logapi.logtrc("EnviaArquivo >> IP DO CLIENTE : [%s],%s %d", mClientSocket->mClientAddress, mUsuario, mClientSocket->ObtemUltimoErro());
			} else {
				logapi.logtrc("EnviaArquivo >> %s Desconexao.", mUsuario);
			}
			return false;
		}

		RESET_TIMER();

		// verifica resposta:
		if ( strncmp( rbuff , "000", 3 ) ) {
			enviaErroOperacao();
			strcpy(mErro, ERR_SOCKET);
			logapi.logtrc("EnviaArquivo >> %s Operacao rejeitada: %s.", mUsuario, rbuff);
			return false;
		}
	}

	logapi.logtrc("-- FIM DE ENVIO DE ARQUIVO --");
	return true;
}


//------------------------------------------------------------------------------------------


int CConexaoCliente::listaDownload
	(
		const char *codCliente,
		int codDominio,
		const char *codFormato
	)
{
	memset(mErro, 0, sizeof(mErro));
	char dominio[11 + 1];
	
	memset(dominio , 0x00 , sizeof(dominio) );
	_snprintf(dominio, sizeof(dominio), "%d", codDominio);
	
	MySQL7 my(p_Config->BdServer, p_Config->BdUsuario, p_Config->BdSenha, p_Config->BdName);
	string qry =
		"select A.IdArquivo, A.NomeOrigem, A.TamanhoOrigem, A.DataHoraOrigem, "
		"if(A.ModoOperacao='','Nor', A.ModoOperacao) as 'ModoOperacao', "
		"A.idArquivoRef "
		"from Arquivo A "
		"where A.CodDominio = ";
	qry += dominio;
	qry += " and A.CodCliente = '";
	qry += codCliente;
	qry += "' and A.CodFormato = '";
	qry += codFormato;
	qry += "' and A.TipoArquivo = 'D' and A.StatusArquivo in ('D', 'P', 'A') "
		   "  and Excluido='N' "; // N�o pega os arquivos excluidos
		   "LIMIT 0, 70";         // D� erro no client se n�o tiver limit...
	if(!my.ExecSQL(qry.c_str()))
	{
		strcpy(mErro, my.getError());
		return -1;
	}
	int rcnt = my.getRowCount();
	if((mFilesCnt + rcnt) > MAX_ARQS)
	{
		rcnt -= (mFilesCnt + rcnt) - MAX_ARQS;
	}
	logapi.logtrc("CConexaoCliente >> Lista de arquivos para download :");
	for(int i = 0; i < rcnt; i++)
	{
		strncpy(mFiles[mFilesCnt].CtrStruct.sNomeOriginal,
			my.getString("NomeOrigem"), CTR_TAM_NOME_ORIGINAL);

		logapi.logtrc("        NomeOrigem = %s",mFiles[mFilesCnt].CtrStruct.sNomeOriginal );

		strncpy(mFiles[mFilesCnt].CtrStruct.sTamanho,
			my.getString("TamanhoOrigem"), CTR_TAM_FILE_SIZE);
		strncpy(mFiles[mFilesCnt].CtrStruct.sNomeMV7,
			mFiles[mFilesCnt].CtrStruct.sNomeOriginal, CTR_TAM_NOME_MV7);
		string tm = my.getString("DataHoraOrigem");
		string data = tm.substr(8, 2);
		data += tm.substr(5, 2);
		data += tm.substr(0, 4);
		strncpy(mFiles[mFilesCnt].CtrStruct.sData, data.c_str(), CTR_TAM_DATA);
		string hora = tm.substr(11, 2);
		hora += tm.substr(14, 2);
		hora += tm.substr(17, 2);
		strncpy(mFiles[mFilesCnt].CtrStruct.sHora, hora.c_str(), CTR_TAM_HORA);
		strncpy(mFiles[mFilesCnt].CtrStruct.sDestino,
			mFiles[mFilesCnt].CtrStruct.sNomeOriginal, CTR_TAM_CXP_DESTINO);
		strncpy(mFiles[mFilesCnt].sNomeCentral,
			mFiles[mFilesCnt].CtrStruct.sNomeOriginal, 20);
		mFiles[mFilesCnt].idArquivo = my.getInt("IdArquivo");
		mFilesCnt++;
		my.moveNext();
	}
	return rcnt;
}

bool CConexaoCliente::RecebeTrn()
{
	return RecebeArq();
}
bool CConexaoCliente::EnviaTrn()
{
	return EnviaArquivo();
}
bool CConexaoCliente::AtualizaCxPostal()
{
	char sz_Buffer[60];
	char sz_CaixaPostal[8+1];
	char sz_NomeUsuario[51]; // De acordo com o CENTRALTCP
	char sz_cmd[MAX_CMD_LEN];
	MySQL7 *p_MySql;

	RESET_TIMER();

	p_MySql = new MySQL7(p_Config->BdServer, p_Config->BdUsuario, p_Config->BdSenha, p_Config->BdName);
	sprintf(sz_cmd,"SELECT Nome from Usuario where CodUsuario = '%s'",mUsuario);

	p_MySql->ExecSQL(sz_cmd);
   
	memset(sz_NomeUsuario,0,sizeof(sz_NomeUsuario));
	if(p_MySql->getRowCount() > 0) {
		strncpy(sz_NomeUsuario,p_MySql->getString("Nome"),sizeof(sz_NomeUsuario));
	}

	delete p_MySql;

	memset(sz_CaixaPostal,0,sizeof(sz_CaixaPostal));
	sprintf(sz_CaixaPostal,"%-8.8s",mCodCliente);
	sprintf(sz_Buffer,"%-8.8s%-51.51s%1.1s ",sz_CaixaPostal,sz_NomeUsuario,"A"); // Padr�o do CENTRALTCP
	
	mClientSocket->Send((LPTSTR)&sz_Buffer[0],strlen(sz_Buffer) );

	if (mClientSocket->ObtemUltimoErro()) {		
		strcpy(mErro, ERR_SOCKET);
		
		if (mClientSocket->ObtemUltimoErro() != 999) {
			logapi.logtrc("CConexaoCliente >> Erro ao enviar dados da caixa postal.");
			logapi.logtrc("CConexaoCliente >> Usuario = %s ; Erro = %d", mUsuario, mClientSocket->ObtemUltimoErro());
		} else {
			logapi.logtrc("%s Desconexao.", mUsuario);
		}
		return false;
	}	

	memset((LPTSTR)&sz_Buffer[0], '\0', sizeof(sz_Buffer));
	//Recebe Resposta	
	if (mClientSocket->Receive((LPTSTR)sz_Buffer, 3) <= 0) {		
		strcpy(mErro, ERR_SOCKET);
		
		if (mClientSocket->ObtemUltimoErro() != 999) {
			logapi.logtrc("CConexaoCliente >> Erro ao receber confirmacao dos dados da caixa postal.");
			logapi.logtrc("CConexaoCliente >> Usuario = %s ; Erro = %d", mUsuario, mClientSocket->ObtemUltimoErro());
		} else {
			logapi.logtrc("%s Desconexao.", mUsuario);
		}	
		
		return false;
	}

	if (strstr((LPTSTR)&sz_Buffer[0], "000")) {
		logapi.logtrc("CConexaoCliente >> Envio da Caixa Postal do usu�rio n�o realizado com sucesso  ");		
		enviaErroOperacao();		
		return false;
	}
		
	logapi.logtrc("CConexaoCliente >> Caixa Postal atualizada com sucesso  ");	
	return true;
}
/*
		Se o Usu�rio n�o existir ele � criado , sen�o ele � alterado

		Ao ser criado as tabelas Usuario, ClienteEdivan e ClienteDominio devem ser alteradas.
		Quando o usu�rio for alterado apenas as tabelas Usuario e ClienteDominio devem ser alteradas.
*/
bool CConexaoCliente::AtualizaCliente()
{
	char sz_Buffer[500];

	char sz_NomeUsu[60];
	char sz_FoneDadosUsu[30];
	char sz_EndUsu[60];
	char sz_BairroUsu[60];
	char sz_CidadeUsu[60];
	char sz_EstadoUsu[10];
	char sz_CepUsu[10];
	char sz_EndEleUsu[60];
	char sz_Login[100];
	char sz_CodCliente[100];
	char sz_SenhaUsu[20];
	bool b_UsuarioExiste;
	char sz_CaixaPostal[(8+1)];
	char dig[2 + 1];

	RESET_TIMER();

	//Recebe Dados de Cliente
	memset((LPTSTR)&sz_Buffer[0], '\0', sizeof(sz_Buffer));
	if ( mClientSocket->Receive((LPTSTR)sz_Buffer, 275 ) <= 0) {		
		strcpy(mErro, ERR_SOCKET);		
		if (mClientSocket->ObtemUltimoErro() != 999) {
			logapi.logtrc("CConexaoCliente >> Erro ao receber dados do usuario a ser incluido.");
			logapi.logtrc("CConexaoCliente >> Usuario = %s ; Erro = %d", mUsuario, mClientSocket->ObtemUltimoErro());
		} else {
			logapi.logtrc("%s Desconexao.", mUsuario);
		}
		return false;
	}	
	logapi.logtrc("CConexaoCliente >> AtualizaCliente ");	

	//Copia Dados para Buffer's
	StringNCopy((LPTSTR)&sz_NomeUsu[0],      (LPTSTR)&sz_Buffer[0],   50);
	StringNCopy((LPTSTR)&sz_FoneDadosUsu[0], (LPTSTR)&sz_Buffer[50],  20);
	StringNCopy((LPTSTR)&sz_EndUsu[0],       (LPTSTR)&sz_Buffer[70],  50);
	StringNCopy((LPTSTR)&sz_BairroUsu[0],    (LPTSTR)&sz_Buffer[120], 30);
	StringNCopy((LPTSTR)&sz_CidadeUsu[0],    (LPTSTR)&sz_Buffer[150], 30);
	StringNCopy((LPTSTR)&sz_EstadoUsu[0],    (LPTSTR)&sz_Buffer[180], 2);
	StringNCopy((LPTSTR)&sz_CepUsu[0],       (LPTSTR)&sz_Buffer[182], 8);
	StringNCopy((LPTSTR)&sz_EndEleUsu[0],    (LPTSTR)&sz_Buffer[190], 50);
	StringNCopy((LPTSTR)&sz_Login[0],        (LPTSTR)&sz_Buffer[240], 20);
	StringNCopy((LPTSTR)&sz_SenhaUsu[0],     (LPTSTR)&sz_Buffer[260], 15);
		
	RTrim((LPTSTR)&sz_Login[0]);   
	RTrim((LPTSTR)&sz_SenhaUsu[0]);
	// Usuario = [CodUsuario@ClienteDominio] : para n�o repetir no edivan
	strcpy(sz_CodCliente,sz_Login);
	strcat(sz_Login,"@");
	strcat(sz_Login,mClienteDominio);
	strlwr(sz_Login);
	logapi.logtrc("CConexaoCliente >> Usuario :%s ", sz_Login);		

	MySQL7 *p_MySql;
	char sz_cmd[MAX_CMD_LEN];

	p_MySql = new MySQL7(p_Config->BdServer, p_Config->BdUsuario, p_Config->BdSenha, p_Config->BdName);

	//-------------------------------------------------------
	// Verifica se o usu�rio j� existe

	sprintf(sz_cmd,"Select * from Usuario where CodUsuario='%s'",sz_Login);
	logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);
	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro ao verificar se o Usu�rio j� existe na tabela Usuario.");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	if ( p_MySql->getRowCount() == 0 ) {
		logapi.logtrc("CConexaoCliente >> Usuario %s ainda n�o cadastrado na base.", sz_Login);		
		b_UsuarioExiste = false;
	} else
		b_UsuarioExiste = true;

	// Criptografa senha antes de incluir na base
	CBFCrypt *bfc = new CBFCrypt();
	string key = sz_Login;
	key += "<m5n&kh^jgfD7so+iTu6?4gfbv*lJG#FGHgnGGH=@1d";
	bfc->SetKey(key.c_str(), strlen(key.c_str()));

	// converte para min�scula a senha do Usu�rio
	strlwr(sz_SenhaUsu);

	char *sc = new char[strlen( sz_SenhaUsu )];
	memset(sc, 0, strlen( sz_SenhaUsu ));
	bfc->Crypt(sz_SenhaUsu, sc, strlen(sz_SenhaUsu));

	string senhaCrypt = "0x";
	for(int i = 0; i < strlen(sz_SenhaUsu); i++)
	{		
		memset(dig , 0x00 , sizeof(dig) );
		sprintf(dig, "%2.2x", (unsigned char) sc[i]);
		senhaCrypt += dig;
	}
	//


	if ( b_UsuarioExiste ) {
		// sz_Login ser� igual a mUsuario
		// C�digo do Usu�rio = C�digo do Cliente
		sprintf(sz_cmd,
						"Update ClienteDominio set Endereco='%s' , Bairro='%s' , Cidade='%s' , Estado = '%s' ,"
						"Cep='%s', Telefone='%s' , DataUltAlt = now() where CodDominio=%d and CodCliente='%s' ",						
						sz_EndUsu,sz_BairroUsu,sz_CidadeUsu,sz_EstadoUsu,sz_CepUsu,sz_FoneDadosUsu,
						mDominio , sz_CodCliente );

		logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);

		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na atualiza��o da tabela ClienteDominio");
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}		

		logapi.logtrc("CConexaoCliente >> AtualizaCliente : Senha = %s",sz_SenhaUsu);
		memset(sz_cmd,0,sizeof(sz_cmd));
		sprintf(sz_cmd,
						"Update Usuario set Nome='%s' ,Email='%s', Senha='%s' , DataUltAlt=now() "
						"where CodUsuario='%s'",
						sz_NomeUsu,sz_EndEleUsu, (char *)senhaCrypt.c_str() , sz_Login );

		logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);

		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na atualiza��o da tabela Usuario");
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}
	} else { // Usu�rio ser� cadastrado		
		char sz_CNPJ[15];

		memset(sz_CNPJ,0,sizeof(sz_CNPJ));
		sprintf(sz_CNPJ,"%8.8d%6.6d",mDominio,atoi(sz_CodCliente));


		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		memset(sz_cmd,0,sizeof(sz_cmd));
		sprintf(sz_cmd,
						"Insert into ClienteEdivan "
						" (NomeCliente,Cnpj,Endereco,TipoInscricao, "
						" Bairro, Cidade, Estado, Cep, Telefone, DataCad , DataUltAlt ) "
						" values ('%s','%s','%s', 'L', "
						" '%s' , '%s' , '%s' , "
						" '%s' , '%s' , now() , now() )",sz_NomeUsu , sz_CNPJ,
						sz_EndUsu, sz_BairroUsu, sz_CidadeUsu, sz_EstadoUsu, sz_CepUsu,
						sz_FoneDadosUsu );

		logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);
		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na inser��o na tabela ClienteEdivan");
			logapi.logtrc("CConexaoCliente >> Erro : %s.\r\n",p_MySql->getError() );
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}
		
		// Pega o CodEdivan ( auto incrementado ) rec�m inserido na ClienteEdivan
		long i_CodEdivan = (long) p_MySql->getInsertID();   
	
		memset(sz_cmd,0,sizeof(sz_cmd));
		sprintf(sz_cmd,
						"Insert into ClienteDominio ( CodDominio , CodCliente , CodEdivan , "
						"NomeCliente , Endereco , Bairro , Cidade , Estado , Cep ,         "
						"Telefone , Email , DataCad, DataUltAlt , CodUsuario ) values ( %d , '%s' "
						" , %d , '%s'  , '%s', '%s' , '%s' , '%s' ,"
						" '%s' , '%s' , '%s' , now() , now() ,'%s')", mDominio , sz_CodCliente , i_CodEdivan , 
						sz_NomeUsu , sz_EndUsu , sz_BairroUsu , sz_CidadeUsu , 
						sz_EstadoUsu , sz_CepUsu , sz_FoneDadosUsu , sz_EndEleUsu ,sz_Login );

		logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);
		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na inser��o na tabela ClienteDominio");
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		// sz_Login dever� ser cadastrado
		memset(sz_cmd,0,sizeof(sz_cmd));
		sprintf(sz_cmd,
						"Insert into Usuario (Nome,Email,CodUsuario,Senha, CodEdivan , DataCad, "
						"DataUltAlt , DataSenha ) values    "
						"('%s','%s','%s','%s', %d , now() , now() , now() )",sz_NomeUsu,sz_EndEleUsu, sz_Login , 
						(char *)senhaCrypt.c_str() , i_CodEdivan );

		logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);
		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na inser��o na tabela Usuario");
			logapi.logtrc("CConexaoCliente >> Erro : %s.\r\n",p_MySql->getError() );
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}

		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>		
		memset(sz_cmd,0,sizeof(sz_cmd));
		sprintf(sz_cmd,
						"Insert into UsuarioPerfil (CodDominio, CodUsuario, CodCliente , TipoUsuario , "
						"DataCad, DataUltAlt ) values ( %d , '%s', '%s', '4', now() , now() )",
						mDominio , sz_Login , sz_CodCliente );

		logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);
		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na inser��o na tabela UsuarioPerfil.");
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}

		memset(sz_cmd,0,sizeof(sz_cmd));
		sprintf(sz_cmd,
						"Insert into UsuarioConfiguracao Select %d,'%s','%s', CodFormato, "
						" '\\MV7\\RECEBE','\\MV7\\ENVIA','\\MV7\\ENVIADOS','S','D'       "
						"from FormatoArquivo where CodDominio='%d'", mDominio , sz_Login ,
						sz_CodCliente , mDominio );
		logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);
		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na inser��o na tabela UsuarioConfiguracao.");
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>		
		memset(sz_cmd,0,sizeof(sz_cmd));
		sprintf(sz_cmd,
						"Insert into ClienteConfiguracao                                       "
						"Select CodDominio,'%s',CodFormato,ProducaoDefault ,                   "
						"ModoGravacaoDefault, PadraoNomeDestinoDefault , SentidoComunicacao ,  "
						"PadraoNomeOrigemDefault from FormatoArquivo where CodDominio=%d       ",
						sz_CodCliente,mDominio );

		logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);
		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na inser��o na tabela ClienteConfiguracao.");
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}

		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>		

		memset(sz_cmd,0,sizeof(sz_cmd));
		
		sprintf(sz_cmd,
						"Insert into UsuarioTransacao Select '%s',%d,Grupo,Transacao "
						"from PerfilTransacao where CodDominio=%d and TipoUsuario='4' ",
						sz_Login, mDominio , mDominio );

		logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);
		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na inser��o na tabela UsuarioTransacao.");
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}

		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>		

	}

	delete p_MySql;

	//-------------------------------------------------------------
	logapi.logtrc("CConexaoCliente::AtualizaCliente >> EnviaSucessoOperacao ap�s Insert ou Update.");
	enviaSucessoOperacao();
	if ( mClientSocket->ObtemUltimoErro() ) {
		
		if ( mClientSocket->ObtemUltimoErro() != 999 ) {
            logapi.logtrc("%s Erro ao enviar confirmacao do pacote de dados.", mUsuario);
			logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
		} 
		else
		{
			logapi.logtrc("%s Desconexao.", mUsuario);
		}
		
		return false;
	}

	// Envia a Caixa Postal do Usu�rio ( como no CENTRALTCP )
	memset(sz_CaixaPostal,0,sizeof(sz_CaixaPostal));
	sprintf(sz_CaixaPostal,"%-8.8s",sz_CodCliente);

	logapi.logtrc("CConexaoCliente::AtualizaCliente >> Envia a caixa postal %s",sz_CaixaPostal);

	mClientSocket->Send((LPTSTR)&sz_CaixaPostal[0],strlen(sz_CaixaPostal) );

	if (mClientSocket->ObtemUltimoErro()) {		
		strcpy(mErro, ERR_SOCKET);
		
		if (mClientSocket->ObtemUltimoErro() != 999) {
			logapi.logtrc("CConexaoCliente >> AtualizaCliente : Erro ao enviar dados da caixa postal.");
			logapi.logtrc("CConexaoCliente >> Usuario = %s ; Erro = %d", mUsuario, mClientSocket->ObtemUltimoErro());
		}

		return false;
	}
	
	memset((LPTSTR)sz_Buffer, '\0', sizeof(sz_Buffer));
	//Recebe Resposta	
	if ( mClientSocket->Receive((LPTSTR)sz_Buffer, 3) <= 0 ) {		
		strcpy(mErro, ERR_SOCKET);		
		if (mClientSocket->ObtemUltimoErro() != 999) {			
			logapi.logtrc("CConexaoCliente >> AtualizaCliente : Erro ao receber confirmacao da nova caixa postal.");
			logapi.logtrc("CConexaoCliente >> Usuario = %s ; Erro = %d", mUsuario, mClientSocket->ObtemUltimoErro());
		}
		return false;
	}
	
	if (strstr((LPTSTR)sz_Buffer, "000") == 0) {		
		enviaErroOperacao();		
		logapi.logtrc("CConexaoCliente >> AtualizaCliente : Erro na confirma��o do envio da caixa postal.");
		return false;
	}
	logapi.logtrc("CConexaoCliente >> Usu�rio cadastrado com sucesso.");

	return true;
}
bool CConexaoCliente::Relaciona()
{
	char sz_Buffer[300];

	RESET_TIMER();

	// Os procedimentos utilizados pelo CENTRALTCP tem que ser mantidos !
	// Recebe Dados da transacao, embora n�o os utilize para nada.
	memset((LPTSTR)sz_Buffer, '\0', sizeof(sz_Buffer));
	if ( mClientSocket->Receive((LPTSTR)sz_Buffer, 29 ) <= 0 ) {
		strcpy(mErro, ERR_SOCKET);		
		
		if ( mClientSocket->ObtemUltimoErro() != 999 ) {
			logapi.logtrc("CConexaoCliente >> Relaciona : Erro ao receber dados.");
		}		
		return false;
	}	
	
	enviaSucessoOperacao();
	if ( mClientSocket->ObtemUltimoErro() ) {		
		if (mClientSocket->ObtemUltimoErro() != 999) {
			logapi.logtrc("CConexaoCliente >> Erro ao enviar confirmacao da mudanca de permissao da caixa postal .");
		} 
		else
		{
			logapi.logtrc("%s Desconexao.", mUsuario);
		}
		return false;
	}
	return true;
}
bool CConexaoCliente::DeletaItemLista(int NumeroConexao)
{
	return false;
}
bool CConexaoCliente::AlteraSenha()
{
	char sz_Buffer[500];

	char sz_NomeUsu[60];
	char sz_FoneDadosUsu[30];
	char sz_EndUsu[60];
	char sz_BairroUsu[60];
	char sz_CidadeUsu[60];
	char sz_EstadoUsu[10];
	char sz_CepUsu[10];
	char sz_EndEleUsu[60];
	char sz_Login[30];
	char sz_SenhaUsu[20];	
	char sz_CodCliente[100];
	char dig[2 + 1];

	RESET_TIMER();

	//Recebe Dados de Cliente
	memset((LPTSTR)&sz_Buffer[0], '\0', sizeof(sz_Buffer));
	if ( mClientSocket->Receive((LPTSTR)sz_Buffer, 275 ) <= 0) {		
		strcpy(mErro, ERR_SOCKET);		
		if (mClientSocket->ObtemUltimoErro() != 999) {
			logapi.logtrc("AlteraSenha >> Erro ao receber dados do usuario a ser incluido.");
			logapi.logtrc("AlteraSenha >> Usuario = %s ; Erro = %d", mUsuario, mClientSocket->ObtemUltimoErro());
		}
		else
		{
			logapi.logtrc("%s AlteraSenha:Desconexao.", mUsuario);
		}
		return false;
	}	
	logapi.logtrc("CConexaoCliente >> AlteraSenha ");	

	//Copia Dados para Buffer's
	StringNCopy((LPTSTR)&sz_NomeUsu[0],      (LPTSTR)&sz_Buffer[0],   50);
	StringNCopy((LPTSTR)&sz_FoneDadosUsu[0], (LPTSTR)&sz_Buffer[50],  20);
	StringNCopy((LPTSTR)&sz_EndUsu[0],       (LPTSTR)&sz_Buffer[70],  50);
	StringNCopy((LPTSTR)&sz_BairroUsu[0],    (LPTSTR)&sz_Buffer[120], 30);
	StringNCopy((LPTSTR)&sz_CidadeUsu[0],    (LPTSTR)&sz_Buffer[150], 30);
	StringNCopy((LPTSTR)&sz_EstadoUsu[0],    (LPTSTR)&sz_Buffer[180], 2);
	StringNCopy((LPTSTR)&sz_CepUsu[0],       (LPTSTR)&sz_Buffer[182], 8);
	StringNCopy((LPTSTR)&sz_EndEleUsu[0],    (LPTSTR)&sz_Buffer[190], 50);
	StringNCopy((LPTSTR)&sz_Login[0],        (LPTSTR)&sz_Buffer[240], 20);
	StringNCopy((LPTSTR)&sz_SenhaUsu[0],     (LPTSTR)&sz_Buffer[260], 15);
		
	RTrim((LPTSTR)&sz_Login[0]);   
	RTrim((LPTSTR)&sz_SenhaUsu[0]);     
	// Usuario = [CodUsuario@ClienteDominio] : para n�o repetir no edivan
	strcpy(sz_CodCliente,sz_Login);
	strcat(sz_Login,"@");
	strcat(sz_Login,mClienteDominio);
	strlwr(sz_Login);
	logapi.logtrc("AlteraCliente >> Usuario :%s ", sz_Login);		

	MySQL7 *p_MySql;
	char sz_cmd[MAX_CMD_LEN];

	p_MySql = new MySQL7(p_Config->BdServer, p_Config->BdUsuario, p_Config->BdSenha, p_Config->BdName);
	
	strlwr(sz_SenhaUsu);

	// Criptografa senha antes de incluir na base
	CBFCrypt *bfc = new CBFCrypt();
	string key = mUsuario;
	key += "<m5n&kh^jgfD7so+iTu6?4gfbv*lJG#FGHgnGGH=@1d";
	bfc->SetKey(key.c_str(), strlen(key.c_str()));
	char *sc = new char[strlen( sz_SenhaUsu )];
	memset(sc, 0, strlen( sz_SenhaUsu ));
	bfc->Crypt(sz_SenhaUsu, sc, strlen(sz_SenhaUsu));

	string senhaCrypt = "0x";
	for(int i = 0; i < strlen(sz_SenhaUsu); i++)
	{		
		memset(dig , 0x00 , sizeof(dig) );
		sprintf(dig, "%2.2x", (unsigned char) sc[i]);
		senhaCrypt += dig;
	}
	//

	memset(sz_cmd,0,sizeof(sz_cmd));
	sprintf(sz_cmd,
					"Update Usuario set Senha='%s', DataUltAlt=now() "
					"where CodUsuario='%s'",
					(char *)senhaCrypt.c_str(), sz_Login);

	logapi.logtrc("CConexaoCliente >> AlteraCliente : SQL QUERY : \n%s",sz_cmd);

	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro na atualiza��o da tabela Usuario");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	delete p_MySql;

	enviaSucessoOperacao();
	if ( mClientSocket->ObtemUltimoErro() ) {
		
		if ( mClientSocket->ObtemUltimoErro() != 999 ) {
            logapi.logtrc("%s Erro ao enviar confirmacao do pacote de dados.", mUsuario);
			logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
		}
		
		return false;
	}	

	return true;
}
bool CConexaoCliente::AlteraCliente()
{
	char sz_Buffer[500];

	char sz_NomeUsu[60];
	char sz_FoneDadosUsu[30];
	char sz_EndUsu[60];
	char sz_BairroUsu[60];
	char sz_CidadeUsu[60];
	char sz_EstadoUsu[10];
	char sz_CepUsu[10];
	char sz_EndEleUsu[60];
	char sz_Login[100];
	char sz_CodCliente[100];
	char sz_SenhaUsu[20];
	bool b_UsuarioExiste;
	char dig[2 + 1];

	RESET_TIMER();

	//Recebe Dados de Cliente
	memset((LPTSTR)&sz_Buffer[0], '\0', sizeof(sz_Buffer));
	if ( mClientSocket->Receive((LPTSTR)sz_Buffer, 275 ) <= 0) {		
		strcpy(mErro, ERR_SOCKET);		
		if (mClientSocket->ObtemUltimoErro() != 999) {
			logapi.logtrc("AlteraCliente >> Erro ao receber dados do usuario a ser alterado.");
			logapi.logtrc("AlteraCliente >> Usuario = %s ; Erro = %d", mUsuario, mClientSocket->ObtemUltimoErro());
		} 
		else
		{
			logapi.logtrc("%s AlteraCliente:Desconexao.", mUsuario);
		}
		return false;
	}	
	logapi.logtrc("CConexaoCliente >> AlteraCliente ");	

	//Copia Dados para Buffer's
	StringNCopy((LPTSTR)&sz_NomeUsu[0],      (LPTSTR)&sz_Buffer[0],   50);
	StringNCopy((LPTSTR)&sz_FoneDadosUsu[0], (LPTSTR)&sz_Buffer[50],  20);
	StringNCopy((LPTSTR)&sz_EndUsu[0],       (LPTSTR)&sz_Buffer[70],  50);
	StringNCopy((LPTSTR)&sz_BairroUsu[0],    (LPTSTR)&sz_Buffer[120], 30);
	StringNCopy((LPTSTR)&sz_CidadeUsu[0],    (LPTSTR)&sz_Buffer[150], 30);
	StringNCopy((LPTSTR)&sz_EstadoUsu[0],    (LPTSTR)&sz_Buffer[180], 2);
	StringNCopy((LPTSTR)&sz_CepUsu[0],       (LPTSTR)&sz_Buffer[182], 8);
	StringNCopy((LPTSTR)&sz_EndEleUsu[0],    (LPTSTR)&sz_Buffer[190], 50);
	StringNCopy((LPTSTR)&sz_Login[0],        (LPTSTR)&sz_Buffer[240], 20);
	StringNCopy((LPTSTR)&sz_SenhaUsu[0],     (LPTSTR)&sz_Buffer[260], 15);
		
	RTrim((LPTSTR)&sz_Login[0]);   
	RTrim((LPTSTR)&sz_SenhaUsu[0]);
	// Usuario = [CodUsuario@ClienteDominio] : para n�o repetir no edivan
	strcpy(sz_CodCliente,sz_Login);
	strcat(sz_Login,"@");
	strcat(sz_Login,mClienteDominio);
	strlwr(sz_Login);
	logapi.logtrc("CConexaoCliente >> Usuario :%s ", sz_Login);		

	MySQL7 *p_MySql;
	char sz_cmd[MAX_CMD_LEN];

	p_MySql = new MySQL7(p_Config->BdServer, p_Config->BdUsuario, p_Config->BdSenha, p_Config->BdName);

	//-------------------------------------------------------
	// Verifica se o usu�rio j� existe

	sprintf(sz_cmd,"Select * from Usuario where CodUsuario='%s'",sz_Login);
	logapi.logtrc("CConexaoCliente >> AlteraCliente : SQL QUERY : \n%s",sz_cmd);
	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro ao verificar se o Usu�rio j� existe na tabela Usuario.");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	b_UsuarioExiste = p_MySql->getRowCount() != 0;
	int codEdivan = p_MySql->getInt("CodEdivan");

	// Criptografa senha antes de incluir na base
	CBFCrypt *bfc = new CBFCrypt();
	string key = sz_Login;
	key += "<m5n&kh^jgfD7so+iTu6?4gfbv*lJG#FGHgnGGH=@1d";
	bfc->SetKey(key.c_str(), strlen(key.c_str()));

	// converte para min�scula a senha do Usu�rio
	strlwr(sz_SenhaUsu);

	char *sc = new char[strlen( sz_SenhaUsu )];
	memset(sc, 0, strlen( sz_SenhaUsu ));
	bfc->Crypt(sz_SenhaUsu, sc, strlen(sz_SenhaUsu));

	string senhaCrypt = "0x";
	for(int i = 0; i < strlen(sz_SenhaUsu); i++)
	{		
		memset(dig , 0x00 , sizeof(dig) );
		sprintf(dig, "%2.2x", (unsigned char) sc[i]);
		senhaCrypt += dig;
	}
	//

	if ( b_UsuarioExiste ) {
		sprintf(sz_cmd, "Update ClienteDominio set " 
						" NomeCliente='%s', "  
			            " Endereco='%s' , Bairro='%s' , Cidade='%s' , Estado = '%s' ,"
						" Cep='%s', Telefone='%s' , "
						" DataUltAlt = now() "
						" where CodDominio=%d and CodCliente='%s' ",						
						sz_NomeUsu,
						sz_EndUsu,sz_BairroUsu,sz_CidadeUsu,sz_EstadoUsu,
						sz_CepUsu,sz_FoneDadosUsu,
						mDominio , sz_CodCliente );

		logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);

		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na atualiza��o da tabela ClienteDominio");
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}

		sprintf(sz_cmd, "Update ClienteEdivan set " 
						" NomeCliente='%s', "  
			            " Endereco='%s' , Bairro='%s' , Cidade='%s' , Estado = '%s' ,"
						" Cep='%s', Telefone='%s' , "
						" DataUltAlt = now() "
						" where CodEdivan=%d ",						
						sz_NomeUsu,
						sz_EndUsu,sz_BairroUsu,sz_CidadeUsu,sz_EstadoUsu,
						sz_CepUsu,sz_FoneDadosUsu,
						codEdivan );

		logapi.logtrc("CConexaoCliente >> AtualizaCliente : SQL QUERY : \n%s",sz_cmd);

		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na atualiza��o da tabela ClienteDominio");
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}

		logapi.logtrc("CConexaoCliente >> AlteraCliente : Senha = %s",sz_SenhaUsu);
		memset(sz_cmd,0,sizeof(sz_cmd));

		/*
		sprintf(sz_cmd, "Update Usuario set "
			            " Nome='%s' ,Email='%s', Senha='%s' , DataUltAlt=now() "
						" where CodUsuario='%s'",
						sz_NomeUsu,sz_EndEleUsu, (char *)senhaCrypt.c_str(),
						sz_Login );
		*/
		sprintf(sz_cmd, "Update Usuario set "
			            " Nome='%s' ,Email='%s', DataUltAlt=now() "
						" where CodUsuario='%s'",
						sz_NomeUsu,sz_EndEleUsu,
						sz_Login );

		logapi.logtrc("CConexaoCliente >> AlteraCliente : SQL QUERY : \n%s",sz_cmd);

		if ( !p_MySql->ExecSQL(sz_cmd) ) {
			logapi.logtrc("CConexaoCliente >> Erro na atualiza��o da tabela Usuario");
			enviaErroOperacao();
			delete p_MySql;
			return false;
		}
	} else { // Usu�rio n�o existe		
		logapi.logtrc("CConexaoCliente >> Erro: Usario n�o existe...");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	delete p_MySql;

	//-------------------------------------------------------------
	logapi.logtrc("CConexaoCliente::AlteraCliente >> EnviaSucessoOperacao ap�s Insert ou Update.");
	enviaSucessoOperacao();
	if ( mClientSocket->ObtemUltimoErro() ) {
		
		if ( mClientSocket->ObtemUltimoErro() != 999 ) {
            logapi.logtrc("%s Erro ao enviar confirmacao do pacote de dados.", mUsuario);
			logapi.logtrc("%s %d", mUsuario, mClientSocket->ObtemUltimoErro());
		} 
		else
		{
			logapi.logtrc("%s Desconexao.", mUsuario);
		}
		
		return false;
	}

	logapi.logtrc("CConexaoCliente >> Usu�rio alterado com sucesso.");

	return true;
}
bool CConexaoCliente::ExcluiCliente()
{
	char sz_Buffer[500];
	char sz_Login[30];
	int  i_CodEdivan;
	char sz_CodCliente[100];

	RESET_TIMER();

	// Recebe Dados de Cliente -> Modelo utilizado pelo CentralTCP 
	// Na verdade os dados recebidos do buffer 
	memset((LPTSTR)&sz_Buffer[0], '\0', sizeof(sz_Buffer));
	if ( mClientSocket->Receive((LPTSTR)sz_Buffer, 29 ) <= 0) {		
		strcpy(mErro, ERR_SOCKET);		
		if (mClientSocket->ObtemUltimoErro() != 999) {
			logapi.logtrc("CConexaoCliente >> Erro ao receber dados do usuario a ser exclu�do.");
			logapi.logtrc("CConexaoCliente >> Usuario = %s ; Erro = %d", mUsuario, mClientSocket->ObtemUltimoErro());
		}
		else
		{
			logapi.logtrc("%s Desconexao.", mUsuario);
		}
		return false;
	}	
	logapi.logtrc("CConexaoCliente >> ExcluiCliente ");	
	StringNCopy((LPTSTR)&sz_Login[0],        (LPTSTR)&sz_Buffer[0], 20);
	RTrim((LPTSTR)&sz_Login[0]);   
	// Usuario = [CodUsuario@ClienteDominio] : para n�o repetir no edivan
	strcpy(sz_CodCliente,sz_Login);
	strcat(sz_Login,"@");
	strcat(sz_Login,mClienteDominio);
	strlwr(sz_Login);
	logapi.logtrc("ExcluiCliente >> Usuario :%s ", sz_Login);		

	MySQL7 *p_MySql;
	char sz_cmd[MAX_CMD_LEN];

	p_MySql = new MySQL7(p_Config->BdServer, p_Config->BdUsuario, p_Config->BdSenha, p_Config->BdName);

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	sprintf(sz_cmd,"Select CodEdivan from Usuario where CodUsuario='%s'",sz_Login);
	logapi.logtrc("CConexaoCliente >> ExcluiCliente : SQL QUERY : \n%s",sz_cmd);

	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro na consulta da tabela Usuario");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}
	i_CodEdivan = p_MySql->getInt("CodEdivan");

	memset(sz_cmd,0,sizeof(sz_cmd));
	sprintf(sz_cmd,"Delete from ClienteDominio where CodDominio=%d and CodEdivan=%d and "
				   "CodCliente = '%s'" , 
					mDominio,i_CodEdivan,sz_CodCliente );
	logapi.logtrc("CConexaoCliente >> ExcluiCliente : SQL QUERY : \n%s",sz_cmd);

	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro na exclus�o da tabela ClienteDominio");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	memset(sz_cmd,0,sizeof(sz_cmd));
	sprintf(sz_cmd,"Delete from ClienteConfiguracao where CodDominio=%d and CodCliente = '%s'" , 
					mDominio,sz_CodCliente );
	logapi.logtrc("CConexaoCliente >> ExcluiCliente : SQL QUERY : \n%s",sz_cmd);

	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro na exclus�o da tabela ClienteDominio");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	memset(sz_cmd,0,sizeof(sz_cmd));
	sprintf(sz_cmd,"Delete from ClienteEdivan where CodEdivan=%d ",i_CodEdivan);
	logapi.logtrc("CConexaoCliente >> ExcluiCliente : SQL QUERY : \n%s",sz_cmd);

	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro na exclus�o da tabela ClienteDominio");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	sprintf(sz_cmd,"Delete from Usuario where CodUsuario='%s'",sz_Login);
	logapi.logtrc("CConexaoCliente >> ExcluiCliente : SQL QUERY : \n%s",sz_cmd);

	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro na exclus�o da tabela Usuario");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	memset(sz_cmd,0,sizeof(sz_cmd));
	sprintf(sz_cmd,"Delete from UsuarioPerfil where CodDominio=%d and CodUsuario='%s' ",
					mDominio,sz_Login);
	logapi.logtrc("CConexaoCliente >> ExcluiCliente : SQL QUERY : \n%s",sz_cmd);

	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro na exclus�o da tabela ClienteDominio");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	memset(sz_cmd,0,sizeof(sz_cmd));
	sprintf(sz_cmd,"Delete from UsuarioConfiguracao where CodDominio=%d and CodUsuario='%s' ",
					mDominio,sz_Login);
	logapi.logtrc("CConexaoCliente >> ExcluiCliente : SQL QUERY : \n%s",sz_cmd);

	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro na exclus�o da tabela ClienteDominio");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	memset(sz_cmd,0,sizeof(sz_cmd));
	sprintf(sz_cmd,"Delete from UsuarioTransacao where CodDominio=%d and CodUsuario='%s' ",
					mDominio,sz_Login);
	logapi.logtrc("CConexaoCliente >> ExcluiCliente : SQL QUERY : \n%s",sz_cmd);

	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro na exclus�o da tabela ClienteDominio");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	delete p_MySql;

	//--------------------------------------------------------------------------------
	enviaSucessoOperacao();
	
	if ( mClientSocket->ObtemUltimoErro() ) {		
		if ( mClientSocket->ObtemUltimoErro() != 999 ) {
			logapi.logtrc("CConexaoCliente >> Erro ao enviar confirmacao da exclusao do usuario.");
		}		
		return false;
	}
	return true;
}
bool CConexaoCliente::ExcluiRelacao()
{
	char sz_Buffer[300];

	RESET_TIMER();

	// Os procedimentos utilizados pelo CENTRALTCP tem que ser mantidos !
	// Recebe Dados da transacao, embora n�o os utilize para nada.
	memset((LPTSTR)sz_Buffer, '\0', sizeof(sz_Buffer));
	if ( mClientSocket->Receive((LPTSTR)sz_Buffer, 29 ) <= 0 ) {		
		strcpy(mErro, ERR_SOCKET);		
		if ( mClientSocket->ObtemUltimoErro() != 999 ) {
			logapi.logtrc("CConexaoCliente >> Relaciona : Erro ao receber dados.");
		}		
		return false;
	}

	enviaSucessoOperacao();
	if(mClientSocket->ObtemUltimoErro())
	{		
		if(mClientSocket->ObtemUltimoErro() != 999)
		{
			logapi.logtrc("CConexaoCliente >> Erro ao enviar confirmacao da exclus�o da rela��o.");
		}			
		return false;
	}
	return true;
}

void CConexaoCliente::RESET_TIMER()
{
	unsigned long l_TimerId;
	int i_c1;
	CThrTimer *p_Tim;
	// Procura o TIMER correspondente ao Thread de transa��o que instanciou esta classe.

	( (CThrListen *)((CThrTransacao *)p_Parent)->p_Parent)->m_MapConexoes.Lookup( ((CThrTransacao *)p_Parent)->m_nThreadID,l_TimerId);

	// Varre a lista de TIMERs para encontrar o ponteiro para CThrTimer correspondente ao
	// TimerId encontrado.

	for ( i_c1 = 0 ; i_c1 < ( (CThrListen *)((CThrTransacao *)p_Parent)->p_Parent)->m_TimerList.GetSize() ; i_c1++ ) {
		p_Tim = (CThrTimer *)( (CThrListen *)((CThrTransacao *)p_Parent)->p_Parent)->m_TimerList.GetAt(i_c1);
		if ( p_Tim ) {			
			if ( p_Tim->m_nThreadID == l_TimerId ) {
				p_Tim->Reset();				
				break;
			}
		}
	}

	return;
}
bool CConexaoCliente::GravaLogNavegacao(char *Grupo,char *Transacao,char *Data, char *Hora, int Status)
{

	MySQL7 *p_MySql;
	char sz_cmd[MAX_CMD_LEN];

	p_MySql = new MySQL7(p_Config->BdServer, p_Config->BdUsuario, p_Config->BdSenha, p_Config->BdName);

	memset(sz_cmd,0,sizeof(sz_cmd));
	sprintf(sz_cmd,"INSERT INTO LogNavegacao "
				   "       (CodDominio,CodUsuario,CodCliente, "
				   "        Grupo,Transacao,idSessao,Status, "
				   "        Data,Horario,DataFinal,HorarioFinal)  "
				   "VALUES (%d,'%s','%s','%s','%s','%s','%05d','%s','%s',now(),now())",
				   mDominio,mUsuario,mCliente,Grupo,Transacao,midSessao,Status,
				   Data,Hora);
	
	logapi.logdump("CConexaoCliente >> GravaLogNavegacao : SQL QUERY : ",sz_cmd , strlen(sz_cmd));

	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro ao gravar LogNavegacao");
		enviaErroOperacao();
		delete p_MySql;
		return false;
	}

	delete p_MySql;
	return true;
}

bool CConexaoCliente::GetDataHoraMySql(char *DataHora)
{

	MySQL7 *p_MySql;
	char sz_cmd[MAX_CMD_LEN];

	p_MySql = new MySQL7(p_Config->BdServer, p_Config->BdUsuario, p_Config->BdSenha, p_Config->BdName);

	memset(sz_cmd,0,sizeof(sz_cmd));
	sprintf(sz_cmd,"Select now() datahora");

	if ( !p_MySql->ExecSQL(sz_cmd) ) {
		logapi.logtrc("CConexaoCliente >> Erro ao pegar DataHora MySql");
		delete p_MySql;
		return false;
	}
	strncpy(DataHora,p_MySql->getString("datahora"), 20);

	delete p_MySql;
	return true;
}


void CConexaoCliente::setidSessao()
{
    char hstName[50];
    gethostname(hstName,50);
	RTrim(hstName);	
	SYSTEMTIME st;
	GetLocalTime (&st);

	sprintf(midSessao, "%04d%02d%02d %02d:%02d:%02d.%03d %5d %s",
	st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, 
	st.wMilliseconds,GetCurrentThreadId(),hstName);

}
