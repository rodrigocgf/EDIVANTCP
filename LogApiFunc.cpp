#include "StdAfx.h"
#include "LogApiFunc.h"

#define LOG_LEVEL_DEFAULT 3

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLogApiFunc::CLogApiFunc()
{
	char caminho[_MAX_PATH + 1];

	memset(caminho , 0x00 , sizeof(caminho) );
	
	GetPrivateProfileString("LOG", "CAMINHO", getenv("TEMP"), caminho, _MAX_PATH, "EDIVANTCP.INI");

	strcpy(m_dir, caminho);
	strcpy(m_file, "EDIVANTCP");

	loglevel = (long) GetPrivateProfileInt("LOG", "LOG_LEVEL", LOG_LEVEL_DEFAULT, "EDIVANTCP.INI");
}

CLogApiFunc::~CLogApiFunc()
{

}
//////////////////////////////////////////////////////////////////////
void CLogApiFunc::logsys(const char *msg, ...)
{
   char buffer[2048];

   va_list ArgPtr;
   va_start(ArgPtr, msg);
   vsprintf(buffer, msg, ArgPtr);
   va_end(ArgPtr);

   gravalog ( buffer, 'S' );
}

//////////////////////////////////////////////////////////////////////
void CLogApiFunc::logtrc(long log_level, const char *msg, ...)
{

   char buffer[2048];

   // Se nivel de log menor que o setado, entao nao grava
	if ( loglevel > log_level )
		return;

   va_list ArgPtr;
   va_start(ArgPtr, msg);
   vsprintf(buffer, msg, ArgPtr);
   va_end(ArgPtr);


	gravalog ( buffer, 'T' );
}

//////////////////////////////////////////////////////////////////////
void CLogApiFunc::logerr(const char *msg, ...)
{   
   char buffer[2048];

   va_list ArgPtr;
   va_start(ArgPtr, msg);
   vsprintf(buffer, msg, ArgPtr);
   va_end(ArgPtr);

	gravalog ( buffer, 'E' );
}

//////////////////////////////////////////////////////////////////////
void CLogApiFunc::gravalog(const char *buffer, char tipo)
{
	FILE * fd;
	char path[MAX_PATH];
	char ShortFilename[1024];
	char *ApChar;
	SYSTEMTIME st;

	GetLocalTime (&st);

	// Retira qualquer extensao que existir no nome do arquivo
	memcpy (ShortFilename,m_file,strlen(m_file));
	ShortFilename [strlen(m_file)]=0;
	if ( (ApChar=strchr(ShortFilename,'.')) != NULL )
	{
       *ApChar='\0';
	}     
	
	// Forca a extensao ".TXT" e tambem a gravacao de 
	// ano, mes e dia no nome do arquivo de log
	sprintf ( path, "%s\\%s%04d%02d%02d.TXT", m_dir, ShortFilename, st.wYear, 
					st.wMonth , st.wDay );
	if ( ( fd = fopen ( path, "a+" ) ) == NULL )
		return;

  	fprintf(fd, "%04d%02d%02d %02d:%02d:%02d.%03d %c %4d %s \r\n",
	st.wYear, st.wMonth, st.wDay, st.wHour , 
	st.wMinute, st.wSecond, st.wMilliseconds, tipo, 
    GetCurrentThreadId(), buffer);
	fclose(fd);	
}

// Metodo que verifica a existencia do arquivo de Log, de acordo com 
// m_dir e m_file. O nome do arquivo sera formado  o nome do m_file com a 
// extensao ".INI". 
// Este arquivo devera conter as seguintes linhas:
//			[LEVEL]
//			LOGLEVEL = 4

//////////////////////////////////////////////////////////////////////
long CLogApiFunc::getlevel()
{
	char cwd[_MAX_PATH + 1];

	memset(cwd , 0x00 , sizeof(cwd) );
	_getcwd(cwd, _MAX_PATH);
	strcat(cwd, "\\EDIVANTCP.INI");

	return (long) GetPrivateProfileInt("LOG", "LOG_LEVEL", LOG_LEVEL_DEFAULT, cwd);
}


//////////////////////////////////////////////////////////////////////
void CLogApiFunc::logtrc(const char *msg, ...)
{

   char buffer[2048];

    // Se nivel de log menor que o setado, entao nao grava
	if ( loglevel > LOG_LEVEL_DEFAULT )
		return;


   // Se nivel de log menor que o setado, entao nao grava
   va_list ArgPtr;
   va_start(ArgPtr, msg);
   vsprintf(buffer, msg, ArgPtr);
   va_end(ArgPtr);


	gravalog ( buffer, 'T' );
}

//////////////////////////////////////////////////////////////////////
void CLogApiFunc::logdump(char *title, char *buf, int siz)
{
	FILE * fd;
	char path[MAX_PATH];
	char ShortFilename[1024];
	char *ApChar;
	time_t now;
	struct tm* dt;


	    // Se nivel de log menor que o setado, entao nao grava
	if ( loglevel > LOG_LEVEL_DEFAULT )
		return;
	
	time(&now);
	dt = localtime(&now);
	
	// Retira qualquer extensao que existir no nome do arquivo
	memcpy (ShortFilename,m_file,strlen(m_file));
	ShortFilename [strlen(m_file)]=0;
	if ( (ApChar=strchr(ShortFilename,'.')) != NULL )
	{
            *ApChar='\0';
	}     
	
	// Forca a extensao ".TXT" e tambem a gravacao de 
	// ano, mes e dia no nome do arquivo de log
	sprintf ( path, "%s/%s%04d%02d%02d.TXT", m_dir, ShortFilename, 
	                                dt->tm_year+1900, 
					dt->tm_mon+1, dt->tm_mday);
	if ( ( fd = fopen ( path, "a+" ) ) == NULL )
		return;

   	fprintf(fd, "%04d%02d%02d %02d:%02d:%02d %4d %s \r\n",
		dt->tm_year+1900, dt->tm_mon+1, dt->tm_mday, dt->tm_hour, 
		dt->tm_min, dt->tm_sec, GetCurrentThreadId(), title );

	hexdump(fd, (char *) buf, siz);


	fclose(fd);	
}

//////////////////////////////////////////////////////////////////////
void CLogApiFunc::hexdump(FILE *f, char *buff, int len)
{

    char oldbuff [16];			// Para remover linhas repetidas
    int  nlin  = len >> 4;		// len / 16
    int  resto = len & 0x0F;	// len % 16 
    int  first = 1;				// Flag: indica a primeira linha.
    int  i;

    // Tratamento de linhas completas
    for (i = 0; i < nlin; i++)
    {
        if (i != 0 && memcmp (oldbuff, &buff[i << 4], 16) == 0)
        {
            if (first)
            {
                // Imprimiremos apenas na 1a. linha de repeticao.
                fprintf (f, "*\n");
                first = 0;
            }
        }
        else
        {
            first = 1;
            printlin (f, buff, i << 4, 16,len);
        }
        memcpy (oldbuff, &buff[i << 4], 16);
    }
    // Tratamento do resto
    i = nlin;
    if (resto)
        printlin (f, buff, i << 4, resto,len);
    fflush (f);

}

/*---------------------------------- hexdump ----------------------------------
** Dump no formato hd -A, incluindo a operacao de remover linhas repetidas.
** Exemplo: hexdump (stdout, buff, sizeof buff);
** Exemplo da saida:
0000    7c 2a 2d 2d 2d 2d 2d 2d  2d 2d 2d 2d 2d 2d 2d 2d   |*--------------
0010    2d 2d 2d 2d 2d 2d 2d 2d  2d 2d 2d 2d 2d 2d 2d 2d   ----------------
0020    2d 2d 2d 20 44 52 49 56  45 54 43 50 2e 43 20 2d   --- DRIVETCP.C -
0030    2d 2d 2d 2d 2d 2d 2d 2d  2d 2d 2d 2d 2d 2d 2d 2d   ----------------
*
0050    0a 2a 2a 20 50 72 6f 6a  65 74 6f 20 41 75 74 6f   .** Projeto Auto
0060    72 69 7a 61 64 6f 72 20  54 65 63 42 61 6e 0a 2a   rizador TecBan.*
** Usa a rotina auxiliar printlin().
**------------------------------------------------------------------------------
*/

//////////////////////////////////////////////////////////////////////
void CLogApiFunc::printlin(FILE *f, char * buff, int offs, int len , int i_tambuff)
{

    int i,i_aux;
	BYTE b_val;	

    // Imprimindo o offset
    fprintf (f, "%04x\t", offs);
    // Imprimindo os caracteres em hexa
    for ( i = 0 ; i < 16 ; i++ ) {
		i_aux = offs + i;

		if ( i_aux < i_tambuff ) {
			b_val = (0xff & buff[offs+i]);
			// "0xfF &" remove o sinal 
			if (i < len)
				fprintf (f, "%02x ", b_val );
			else
				fprintf (f, "   ");
		} else {
			fprintf (f, "   ");
		}

        if (i == 7)     // espaco adicional entre colunas
            fprintf (f, " ");

    }

    // Imprimindo . para caracteres nao-imprimiveis
    fprintf (f, "  ");
    for (i = 0; i < len; i++)
        if (isprint (buff [offs+i]))
            putc (buff [offs+i], f);
        else
            putc ('.', f);
    putc ('\n', f);
}

