#ifndef _ARQUIVO_H_
#define _ARQUIVO_H_

#include "MySQL7.h"
#include "Config.h"
#include "Rapi.h"

#define  MAX_CMD_LEN 5000
#define	MAX_BUFFER_IN 100000               
#define	MAX_BUFFER_OUT MAX_BUFFER_IN

class Arquivo  
{
public:
	int Read(char* Buffer, int size);
	int TamanhoDisco();
	int Consistir();
	int ReCriar(
		int id,
		int CodDominio, char *CodCliente, 
		char *CodUsuario, char *CodFormato, 
		char *NomeOrigem, struct tm DataHoraOrigem,
		int TamanhoOrigem, char *ModoOperacao, char TipoArquivo);
	int Criar(
		int CodDominio, char *CodCliente, 
		char *CodUsuario, char *CodFormato, 
		char *NomeOrigem, struct tm DataHoraOrigem,
		int TamanhoOrigem, char *ModoOperacao, char TipoArquivo);
	int getID();
	int Parcial();
	int TotalmenteTransmitido();
	int Processado();
	//long Arquivo::RestartPt();
	long getRestartPoint() { return restartPt; }
	void setRestartPoint(long pt) { restartPt = pt; }

	Arquivo(CConfig* cfg);
	
	int jaTransmitido(
		int CodDominio, char *CodCliente,  
		char *CodFormato, char *NomeOrigem, struct tm DataHoraOrigem);
	int Existe(int id, char Tipo);
	virtual ~Arquivo();
	int AbreArquivo(bool criar, bool truncar);
	int Aberto();
	int PosicionaArquivo();
	int Gravar(char *Buffer, int size);
	int FechaArquivo(bool truncar);
	long idArquivo;
	char GetStatusArquivo();

protected:
	int salvarEstado();
	long restartPt;
	int TamanhoDestino;
	char StatusArquivo;
	char ModoOperacao[4];
	char NomeDestino[101];
	char CodClienteDestino[50];

	int ConfereDestino
	(
		int  CodDominio,
		char *ArqOrigem,
		char *CodFormato, 
		long OffSetIdCliente,
		long TamIdCliente
	);

private:
	char *datetm2string(struct tm data);
	char cmd[MAX_CMD_LEN];
	int CodDominio;
	Rapi *rapi;
	bool formatoBinario();
	int getSize(char *NomeHost);
	int CodEdivan;
	int CodEdivanDominio;
	int TamanhoOrigem;
	int ErroRapi;
	char dirCaixaPostal[101];
	char dirConsistencia[101];
	int fd;
	void atualizaEstado();
	
	MySQL7 *sql7;

	char TipoArquivo;
	char CodFormato[11];
	char NomeOrigem[101];
};

#endif // !defined(AFX_ARQUIVO_H__10A636B1_639A_4D08_A5BF_C9D3597A1716__INCLUDED_)
