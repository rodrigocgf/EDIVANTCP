// SrvService.h: interface for the CSrvService class.
//
//////////////////////////////////////////////////////////////////////


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "MIDLETSRVRWnd.h"
#include "NTService.h"

class CSrvService : public CNTService  
{
public:
	CSrvService();
	virtual ~CSrvService();

	BOOL OnInit();
	void Run();
	void OnStop();

	BOOL m_bService;

	//CMIDLETSRVRWnd *p_Wnd;

};


