// ThrTimer.cpp : implementation file
//

#include "stdafx.h"
#include "EDIVANTCP.h"
#include "ThrTimer.h"
#include "ThrListen.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CThrTimer

IMPLEMENT_DYNCREATE(CThrTimer, CWinThread)

CThrTimer::CThrTimer(CWinThread *p_Transacao)
{
	p_Thr = p_Transacao;
	l_Counter = TIMEOUT;
	h_EvtKill = CreateEvent(NULL,TRUE,FALSE,NULL);
}

CThrTimer::CThrTimer()
{
}

CThrTimer::~CThrTimer()
{
	CloseHandle(h_EvtKill);
}

BOOL CThrTimer::InitInstance()
{
	Loop();
	return true;	
}

void CThrTimer::Loop()
{
	do {
		l_Counter--;
		Sleep(1000);
	} while ( (l_Counter > 0) && (  WaitForSingleObject(h_EvtKill,0)!=WAIT_OBJECT_0  ) );

	if ( WaitForSingleObject(h_EvtKill,0) != WAIT_OBJECT_0 )
		((CThrListen *)p_Thr)->OnTimeout(this->m_nThreadID);

	PostThreadMessage(WM_QUIT,0,0);
	return;
}


int CThrTimer::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

void CThrTimer::Reset()
{
	l_Counter = TIMEOUT;
	return;
}

void CThrTimer::Kill()
{
	SetEvent(h_EvtKill);
	return;
}



BEGIN_MESSAGE_MAP(CThrTimer, CWinThread)
	//{{AFX_MSG_MAP(CThrTimer)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThrTimer message handlers
