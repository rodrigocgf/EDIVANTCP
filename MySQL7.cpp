#include "StdAfx.h"
#include "MySQL7.h"

MySQL7::MySQL7(
					const char *host,
					const char *usuario,
					const char *senha,
					const char *bd
					)
{
  res = NULL;
  row = NULL;
  nRows = 0;

  connect(host, usuario, senha, bd);

  nCols = 0;
}

MySQL7::MySQL7()
{
  res = NULL;
  row = NULL;
  nRows = 0;
  connected = false;
  nCols = 0;
}

MySQL7::~MySQL7()
{
	if(res)
		mysql_free_result(res);
	if(myData)
		mysql_close(myData);
}

bool MySQL7::ExecSQL(const char *sqlCmd)
{
	if(res)
		mysql_free_result(res);

	row = NULL;
	res = NULL;
	nRows = 0;

	char *sqlBarra;
	sqlBarra = (char *) malloc(strlen(sqlCmd) * 2 + 1); // str de barras
	sqlBarra[0] = 0;
	char *posBarra;
	char *pos = (char *) sqlCmd;
	posBarra = strstr(sqlCmd, "\\");
	while(posBarra)
	{
		strncat(sqlBarra, pos, posBarra - pos + 1);
	   strcat(sqlBarra, "\\");
	   pos = ++posBarra;
	   posBarra = strstr(pos, "\\");
	}
	strcat(sqlBarra, pos);
	if(!myData)
		return false;

	if(!mysql_query(myData, sqlBarra))
	{
		free(sqlBarra);
		res = mysql_store_result(myData);
		if(res)
		{
			nRows = (int) mysql_num_rows(res); 
        
			if(nRows)
			{
			   for(nCols = 0 ; (fd = mysql_fetch_field(res)) && 
						nCols < MAX_COL; nCols++)
				   strcpy(aszFlds[nCols], fd->name);
			   row = mysql_fetch_row(res);
			}
			else
			{
			   row = NULL;
			}

		}
		else
		{
			nRows =  (int) mysql_affected_rows(myData);
		}
		ErrNo = mysql_errno(myData);
		return (true);
	}
	else
	{
		ErrNo = mysql_errno(myData);
		free(sqlBarra);
		return (false);
	}
}


int MySQL7::getInt(const char *Col)
{
	return atoi(getString(Col));
}
int MySQL7::getInt(int colIdx)
{
	return atoi(getString(aszFlds[colIdx]));
}

my_ulonglong MySQL7::getLongLong(const char *Col)
{
   #ifdef WIN32
	   return _atoi64(getString(Col));
   #else
      return atoll(getString(Col));
   #endif
}
my_ulonglong MySQL7::getLongLong(int colIdx)
{
   #ifdef WIN32
	   return _atoi64(getString(aszFlds[colIdx]));
   #else
      return atoll(getString(aszFlds[colIdx]));
   #endif
}

double MySQL7::getDouble(const char *Col)
{
	return atof(getString(Col));
}
double MySQL7::getDouble(int colIdx)
{
	return atof(getString(aszFlds[colIdx]));
}


const char *MySQL7::getString(const char *Col)
{
	int i, j;

	if(!row)
		return "";  // TODO: Erro, tratar
	for(i = 0; (j = strcmp(Col, aszFlds[i])) && i < nCols; i++);

	if(j == 0)
	{
	   return((row[i] == NULL) ? "" : row[i]);
	}
	else
	{
	   return "";  // TODO: Erro, tratar
	}
}
const char *MySQL7::getString(int colIdx)
{
	return getString(aszFlds[colIdx]);
}

const char *MySQL7::getError(void)
{
	return mysql_error(myData);
}

bool MySQL7::moveNext()
{
	if(res && row)
	{
		row = mysql_fetch_row( res );
		ErrNo = mysql_errno(myData);
		return row != NULL;
	}
	else
	{
		ErrNo = mysql_errno(myData);
		row = NULL;
		return false;
	}
}

my_ulonglong MySQL7::getInsertID()
{
	return (myData ? mysql_insert_id(myData) : 0);
}

int MySQL7::connect(
						  const char *host, 
						  const char *usuario,
						  const char *senha,
						  const char *bd
						  )
{
	connected = false;
	if(((myData = mysql_init((MYSQL*) 0)) && 
	  mysql_real_connect(myData, host, usuario, senha, NULL, MYSQL_PORT, NULL, 0)))
	{
		if(mysql_select_db(myData, bd) < 0)
		{
			ErrNo = mysql_errno(myData);
			return false;
		}
		else
		{
			connected = true;
			return true;
		}
	}
	else
	{
		ErrNo = mysql_errno(myData);
		return false;
	}
}

char MySQL7::getChar(const char *Col)
{
	return getString(Col)[0];
}
char MySQL7::getChar(int colIdx)
{
	return getString(aszFlds[colIdx])[0];
}
