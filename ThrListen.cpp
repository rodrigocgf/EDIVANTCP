// ThrListen.cpp : implementation file
//

#include "StdAfx.h"
#include "EDIVANTCP.h"
#include "ThrListen.h"
#include "LogApiFunc.h"
#include "Config.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CLogApiFunc logapi;

/////////////////////////////////////////////////////////////////////////////
// CThrListen

IMPLEMENT_DYNCREATE(CThrListen, CWinThread)

CThrListen::CThrListen()
{
	InitializeCriticalSection(&m_CriticalSection);
	InitializeCriticalSection(&m_CS);
}

CThrListen::~CThrListen()
{
	DeleteCriticalSection(&m_CriticalSection);
	DeleteCriticalSection(&m_CS);
}

BOOL CThrListen::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	InicializaPortListen();
	return TRUE;
}

int CThrListen::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CThrListen, CWinThread)
	//{{AFX_MSG_MAP(CThrListen)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThrListen message handlers

void CThrListen::InicializaPortListen()
{
	SOCKADDR_IN addr_tmp;
	int addr_len;

	logapi.logtrc(LOG_LEVEL_DEFAULT,"IniciaPortListen().\r\n");

	sock_LISTEN = socket(AF_INET,SOCK_STREAM,0);	 
	DWORD dwReUse = 1;
	
	if(sock_LISTEN != INVALID_SOCKET) {
		addr_LISTEN.sin_family = AF_INET;

		unsigned short us_PORT_LISTEN;		
		us_PORT_LISTEN = (short) GetPrivateProfileInt("7COMm", "PORT_LISTEN",PORT_LISTEN, "EDIVANTCP.INI");
		addr_LISTEN.sin_port = htons(us_PORT_LISTEN);

		logapi.logtrc(LOG_LEVEL_DEFAULT, "ch_PORT_LISTEN = %d", us_PORT_LISTEN);

		addr_LISTEN.sin_addr.s_addr = htonl(INADDR_ANY);
		
		//Atribui o socket ao seu endere�o
		if (bind(sock_LISTEN,(LPSOCKADDR)&addr_LISTEN,sizeof(addr_LISTEN)) != SOCKET_ERROR) {
			// aguarda conex�es gerando fila de at� 5
			if ( listen(sock_LISTEN,5) != SOCKET_ERROR ) {				
				// fica em looping esperando por accept's
				while( true ) {
					addr_len = sizeof(addr_tmp);
					sock_ACCEPTED = accept(sock_LISTEN,(SOCKADDR *)&addr_tmp,&addr_len);					
					if (sock_ACCEPTED != INVALID_SOCKET ) {
						// D� in�cio � transa��o
						p_Trans = new CThrTransacao(sock_ACCEPTED);
						p_Trans->CreateThread(CREATE_SUSPENDED);
						p_Trans->m_bAutoDelete = true;
						p_Trans->p_Parent = this;
						p_Trans->ResumeThread();
						

						// Inicializa o TIMER correspondente a esta transa��o
						
						p_Timer = new CThrTimer(p_Trans);
						p_Timer->CreateThread(CREATE_SUSPENDED);
						p_Timer->m_bAutoDelete = true;
						p_Timer->p_Thr = (this);
						p_Timer->ResumeThread();
						
						m_MapConexoes.SetAt(p_Trans->m_nThreadID,p_Timer->m_nThreadID);
						logapi.logtrc("CThrListen::InicializaPortListen >> m_MapConexoes : TRANSA��O (TID) %ld <--> TIMER (TID) %ld)",
							p_Trans->m_nThreadID,p_Timer->m_nThreadID);
						

						m_MapSOCKET.SetAt(p_Trans->m_nThreadID,sock_ACCEPTED);
						logapi.logtrc("CThrListen::InicializaPortListen >> m_MapSOCKET : TRANSA��O (TID) %ld <--> SOCKET %d",
							p_Trans->m_nThreadID,sock_ACCEPTED);

						m_ThrList.Add(p_Trans);
						logapi.logtrc("CThrListen::InicializaPortListen >> m_ThrList : Adiciona (TID) %ld",p_Trans->m_nThreadID);

						m_TimerList.Add(p_Timer);
						logapi.logtrc("CThrListen::InicializaPortListen >> m_TimerList : Adiciona (TID ) %ld",p_Timer->m_nThreadID);
						
					}
				}
			} else {
				logapi.logtrc(LOG_LEVEL_DEFAULT,"Erro na fun��o listen");				
			}
		} else {
			logapi.logtrc(LOG_LEVEL_DEFAULT,"Erro na fun��o bind");			
		}
		
	} else {		
		status_sock_LISTEN.Format(_T("SOCKET ERROR"));		 
	}

	return;
}

void CThrListen::OnTimeout(unsigned long ul_Timer)
{	
	logapi.logtrc("CThrListen TIMEOUT TIMER %ld",ul_Timer);

	UnMAP(ul_Timer);
	return;
}

void CThrListen::UnMAP(unsigned long ul_Timer)
{
	unsigned long ul_Trans,ul_Timer1;
	POSITION pCurVal;
	WPARAM wp_SOCKET;
	CThrTransacao *p_Transac;
	CThrTimer *p_Timer;

	logapi.logtrc("CThrListen::UnMAP >> UnMap TIMER %ld",ul_Timer);

	EnterCriticalSection( &m_CriticalSection);

	int i_c1 = 0;

	pCurVal = m_MapConexoes.GetStartPosition( );
	while (pCurVal != NULL) {			   
	   m_MapConexoes.GetNextAssoc(pCurVal,ul_Trans, ul_Timer1);
	   if ( ul_Timer1 == ul_Timer )
		   break;			   
	}
	

	for ( i_c1 = 0 ; i_c1 < m_ThrList.GetSize() ; i_c1++ ) {
		p_Transac = (CThrTransacao *)m_ThrList.GetAt(i_c1);
		if ( p_Transac ) {
			if (  ((CThrTransacao *)p_Transac)->m_nThreadID == ul_Trans ) {
				logapi.logtrc("CThrListen::UnMAP >> m_ThrList : Remove (TID) %ld" , 
					((CThrTransacao *)m_ThrList.GetAt(i_c1))->m_nThreadID );	
				m_ThrList.RemoveAt(i_c1);				
				break;
			}
		}
	}
	
	for ( i_c1 = 0 ; i_c1 < m_TimerList.GetSize() ; i_c1++ ) {
		p_Timer = (CThrTimer *)m_TimerList.GetAt(i_c1);
		if ( p_Timer ) {
			if (  ((CThrTimer *)p_Timer)->m_nThreadID == ul_Timer ) {
				logapi.logtrc("CThrListen::UnMAP >> m_TimerList : Remove (TID) %ld" , 
					((CThrTimer *)m_TimerList.GetAt(i_c1))->m_nThreadID );	
				m_TimerList.RemoveAt(i_c1);				
				break;
			}
		}
	}
	
	m_MapConexoes.RemoveKey(ul_Trans);	
	logapi.logtrc("CThrListen::UnMAP >> m_MapConexoes : TRANSA��O (TID) %ld REMOVIDA",ul_Trans);

	if ( m_MapSOCKET.Lookup(ul_Trans,wp_SOCKET) ) {
		logapi.logtrc("CTrhListen >> SOCKET %ld closed.",wp_SOCKET);
		closesocket(wp_SOCKET); // fecha a conex�o socket
	}
	m_MapSOCKET.RemoveKey(ul_Trans);
	logapi.logtrc("CThrListen::UnMAP >> m_MapSOCKET : TRANSA��O (TID) %ld REMOVIDA",ul_Trans);

	// O Thread pode j� ter sido encerrado.
	if ( p_Transac )
		SetEvent( ((CThrTransacao *)p_Transac)->h_FimThr );	

	LeaveCriticalSection( &m_CriticalSection);

	return;
}
