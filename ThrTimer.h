#if !defined(AFX_THRTIMER_H__B0BAF8E4_4A8D_44BD_9426_1D7968EB0EC8__INCLUDED_)
#define AFX_THRTIMER_H__B0BAF8E4_4A8D_44BD_9426_1D7968EB0EC8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ThrTimer.h : header file
//

// 1 MINUTO
#define TIMEOUT	60

/////////////////////////////////////////////////////////////////////////////
// CThrTimer thread

class CThrTimer : public CWinThread
{
	DECLARE_DYNCREATE(CThrTimer)
public:
	CThrTimer(CWinThread *p_Transacao);
	CThrTimer();           // protected constructor used by dynamic creation
	virtual ~CThrTimer();
	void Loop();
	void Reset();
	void Kill();
// Attributes
public:
	CWinThread* p_Thr;
	long l_Counter;
	HANDLE h_EvtKill;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThrTimer)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:	

	// Generated message map functions
	//{{AFX_MSG(CThrTimer)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THRTIMER_H__B0BAF8E4_4A8D_44BD_9426_1D7968EB0EC8__INCLUDED_)
