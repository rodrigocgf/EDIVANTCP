#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "StdAfx.h"

class CConfig  
{
public:
	const char *diretorioRaiz();
	const char *Programa(const char* prg);
	bool ServerAssina;
	char LogError;
	char LogWarning;
	bool LogTrafego;
	int RapiPort;
	char RapiServer[60];
	CConfig(const char* prgPath);
	virtual ~CConfig();

	char BdUsuario[_MAX_PATH];
	char BdSenha[_MAX_PATH];
	char BdName[_MAX_PATH];
	char BdServer[_MAX_PATH];
	
	int  RestartMinFileSize;
	bool bPermiteCompressao;
	bool bPermiteRestart;

private:
	void LeConfig(const char* prgPath);
	void NomeArquivoConfiguracao(const char *prgPath);	
	char dirRaiz[_MAX_PATH + 1];
	char NomePrograma[_MAX_PATH + 1]; // [002]
	char fnCfgName[_MAX_PATH + 1];
};

#endif // 
