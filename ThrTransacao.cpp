// ThrTransacao.cpp : implementation file
//

#include "stdafx.h"
#include "EDIVANTCP.h"
#include "ThrTransacao.h"
#include "Conexao.h"
#include "LogApiFunc.h"
#include "ThrListen.h"
#include "ThrTimer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



extern CLogApiFunc logapi;

/////////////////////////////////////////////////////////////////////////////
// CThrTransacao

IMPLEMENT_DYNCREATE(CThrTransacao, CWinThread)

CThrTransacao::CThrTransacao()
{
}

CThrTransacao::CThrTransacao(WPARAM wParam)
{
	sock_Trans = wParam;
	h_FimThr = CreateEvent(NULL,TRUE,FALSE,NULL);	
	
}

BOOL CThrTransacao::InitInstance()
{
	
	logapi.logtrc(LOG_LEVEL_DEFAULT,"<--- IN�CIO DA TRANSA��O --->");
	TrataTransacoes();
	return TRUE;
}

CThrTransacao::~CThrTransacao()
{
	CloseHandle(h_FimThr);
	logapi.logtrc(LOG_LEVEL_DEFAULT,"<--- FIM DA TRANSA��O --->");
}

int CThrTransacao::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CThrTransacao, CWinThread)
	//{{AFX_MSG_MAP(CThrTransacao)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThrTransacao message handlers
void CThrTransacao::TrataTransacoes()
{	
	int    nTransacao;	

	p_ConexaoCliente = new CConexaoCliente(sock_Trans);
	p_ConexaoCliente->p_Parent = this;

	// Verifica a chave de entrada
	if(!p_ConexaoCliente->ConsisteChaveEntrada()){
		logapi.logtrc("CThrTransacao >> Erro consistindo chave de entrada.");

		FINALIZA();
		
		PostThreadMessage(WM_QUIT, 0, 0);
		return ;
	}	
	
	// Verifica usuario e senha
	Logged = p_ConexaoCliente->ConsisteUsuarioSenha();

	if(!Logged)
	{
		logapi.logtrc("CThrTransacao >> Conex�o encerrada para o usu�rio %s",
			p_ConexaoCliente->getUsuario());

		FINALIZA();

		PostThreadMessage(WM_QUIT,0,0);
		return ;
	}

	char now[30];
	memset(now , 0x00 , sizeof(now) );

	logapi.logtrc("CThrTransacao >> USU�RIO e SENHA CORRETOS !");
	while( (WaitForSingleObject(h_FimThr,0) != WAIT_OBJECT_0) ) {		
 		//Sleep(150);		

		if((nTransacao = p_ConexaoCliente->AguardaTransacao(1)) == 0)
			continue;

		//logapi.logtrc("TrataTransacoes >> Transacao %d",nTransacao);
		// Verifica o comando recebido
		switch(nTransacao)
		{
		case CMD_TRANSACAO_ENVIA:			
			logapi.logtrc(
				"CThrTransacao >> Recebeu comando de envio de arquivo transacional.");
			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->EnviaTrn();
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_TRANS,"EnviaTrn",now,now,0);
			if(p_ConexaoCliente->ObtemUltimoErroCon())
			{
				if(p_ConexaoCliente->ObtemErroSocket())
				{
					FINALIZA();
				}
			}
			break;
		case CMD_TRANSACAO_RECEBE:			
			logapi.logtrc(
				"CThrTransacao >> Recebeu comando para receber arquivo transacional.");				

			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->RecebeTrn();
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_TRANS,"RecebeTrn",now,now,0);
			if(p_ConexaoCliente->ObtemUltimoErroCon())
			{
				if( p_ConexaoCliente->ObtemErroSocket() ) 
				{
					FINALIZA();					
				}
			}
			break;
		case CMD_RECEBEARQUIVO:			
			logapi.logtrc(
				"CThrTransacao >> Recebeu comando para receber arquivo.");				

			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->RecebeArq();
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_TRANS,"RecebeArq",now,now,0);
			if(p_ConexaoCliente->ObtemUltimoErroCon())
			{

				if(p_ConexaoCliente->ObtemErroSocket())
				{
					FINALIZA();					
				}
			}
			break;
		case CMD_ENVIAARQUIVO:			
			logapi.logtrc(
				"CThrTransacao >> Recebeu comando para enviar arquivo.");				

			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->EnviaArquivo();
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_TRANS,"EnviaArquivo",now,now,0);
			if(p_ConexaoCliente->ObtemUltimoErroCon())
			{
				if(p_ConexaoCliente->ObtemErroSocket())
				{
					FINALIZA();					
				}
			}
			break;			
		case CMD_ATUALIZACXPOSTAL:
			logapi.logtrc(
				"CThrTransacao >> Recebeu comando para atualizar caixa postal.");

			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->AtualizaCxPostal();
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_ATUALIZACAO,"AtualizaCxPostal",now,now,0);
			if(p_ConexaoCliente->ObtemUltimoErroCon())
			{
				if(p_ConexaoCliente->ObtemErroSocket())
				{
					FINALIZA();					
				}
			}
			break;			
		case CMD_ATUALIZACLIENTE:
			logapi.logtrc(
				"CThrTransacao >> Recebeu comando para cadastrar novo usuario na central.");
			
			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->AtualizaCliente();
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_ATUALIZACAO,"AtualizaCliente",now,now,0);
			if(p_ConexaoCliente->ObtemUltimoErroCon())
			{
				if(p_ConexaoCliente->ObtemErroSocket())
				{
					FINALIZA();					
				}
			}
			break;			
		case CMD_RELACIONA:
			logapi.logtrc(
				"CThrTransacao >> Recebeu comando para criar permissao em caixa postal.");				

			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->Relaciona();
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_ATUALIZACAO,"Relaciona",now,now,0);
			if(p_ConexaoCliente->ObtemUltimoErroCon())
			{
				if(p_ConexaoCliente->ObtemErroSocket())
				{
					FINALIZA();					
				}
			}
			break;			
		case CMD_EXCLUI_RELACAO:
			logapi.logtrc(
				"CThrTransacao >> Recebeu comando para deletar permissao em caixa postal.");				

			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->ExcluiRelacao();
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_ATUALIZACAO,"ExcluiRelacao",now,now,0);
			if(p_ConexaoCliente->ObtemUltimoErroCon())
			{
				if(p_ConexaoCliente->ObtemErroSocket())
				{
					FINALIZA();					
				}
			}
			break;			
		case CMD_ALTERASENHA:
			logapi.logtrc(
				"CThrTransacao >> Recebeu comando para alterar dados do usuario.");				

			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->AlteraSenha();
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_ATUALIZACAO,"AlteraSenha",now,now,0);
			if(p_ConexaoCliente->ObtemUltimoErroCon())
			{
				if(p_ConexaoCliente->ObtemErroSocket())
				{
					FINALIZA();					
				}
			}
			break;			
		case CMD_ALTERACLIENTE:
			logapi.logtrc(
				"CThrTransacao >> Recebeu comando para alterar dados do usuario.");				

			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->AlteraCliente();
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_ATUALIZACAO,"AlteraCliente",now,now,0);
			if(p_ConexaoCliente->ObtemUltimoErroCon())
			{
				if(p_ConexaoCliente->ObtemErroSocket())
				{
					FINALIZA();					
				}
			}
			break;			
		case CMD_EXCLUICLIENTE:
			logapi.logtrc(
				"CThrTransacao >> Recebeu comando para excluir dados do usuario.");				

			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->ExcluiCliente();
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_ATUALIZACAO,"ExcluiCliente",now,now,0);
			if(p_ConexaoCliente->ObtemUltimoErroCon())
			{
				if(p_ConexaoCliente->ObtemErroSocket())
				{
					FINALIZA();					
				}
			}
			break;
		case CMD_FINALIZA_CONEXAO:
			p_ConexaoCliente->GetDataHoraMySql(now);
			p_ConexaoCliente->GravaLogNavegacao(GRUPO_ACESSO,"Logout",now,now,0);
			logapi.logtrc("CThrTransacao >> Usuario desconectou.");
			FINALIZA();
			break;
		default:			
			logapi.logtrc("CThrTransacao >> Recebeu comando invalido.");
			FINALIZA();
			break;
		}
	}	
	
	// Destr�i ThrTransacao
	PostThreadMessage(WM_QUIT,0,0);
	return;
}

void CThrTransacao::FINALIZA()
{
	int i_c1;
	CThrTransacao *p_Transac;
	unsigned long	l_TimerId;
	CThrTimer *p_Timer;

	EnterCriticalSection( &((CThrListen *)p_Parent)->m_CS );

	// Desmapeia templates antes de matar os threads
	((CThrListen *)p_Parent)->m_MapConexoes.Lookup(this->m_nThreadID,l_TimerId);

	((CThrListen *)p_Parent)->m_MapConexoes.RemoveKey(this->m_nThreadID);
	logapi.logtrc("CThrTransacao::FINALIZA >> m_MapConexoes : TRANSA��O (TID) %ld REMOVIDA",this->m_nThreadID);

	((CThrListen *)p_Parent)->m_MapSOCKET.RemoveKey(this->m_nThreadID);
	logapi.logtrc("CThrTransacao::FINALIZA >> m_MapSOCKET : TRANSA��O (TID) %ld REMOVIDA",this->m_nThreadID);
	

	for ( i_c1 = 0 ; i_c1 < ((CThrListen *)p_Parent)->m_ThrList.GetSize() ; i_c1++ ) {
		p_Transac = (CThrTransacao *)((CThrListen *)p_Parent)->m_ThrList.GetAt(i_c1);
		if ( p_Transac ) {
			if (  p_Transac->m_nThreadID == this->m_nThreadID ) {
				logapi.logtrc("CThrTransacao::FINALIZA >> m_ThrList : Remove (TID) %ld" , 
					((CThrTransacao *)((CThrListen *)p_Parent)->m_ThrList.GetAt(i_c1))->m_nThreadID );	
				((CThrListen *)p_Parent)->m_ThrList.RemoveAt(i_c1);				
				break;
			}
		}
	}
	
	for ( i_c1 = 0 ; i_c1 < ((CThrListen *)p_Parent)->m_TimerList.GetSize() ; i_c1++ ) {
		p_Timer = (CThrTimer *)((CThrListen *)p_Parent)->m_TimerList.GetAt(i_c1);
		if ( p_Timer ) {
			if (  ((CThrTimer *)p_Timer)->m_nThreadID == l_TimerId ) {

				// Destr�i o Timer
				//((CThrListen *)p_Parent)->p_Timer->Kill();
				p_Timer->Kill();

				logapi.logtrc("CThrTransacao::FINALIZA >> m_TimerList : Remove (TID) %ld" , 
					(((CThrListen *)p_Parent)->m_TimerList.GetAt(i_c1))->m_nThreadID );	
				((CThrListen *)p_Parent)->m_TimerList.RemoveAt(i_c1);				
				break;
			}
		}
	}

	
	// Destr�i a CConexaoCliente
	delete p_ConexaoCliente;
	
	logapi.logtrc("CThrTransacao::FINALIZA >> SOCKET %ld closed.",sock_Trans);
	//shutdown(sock_Trans, SD_SEND);		
	closesocket(sock_Trans);			

	SetEvent(h_FimThr);

	LeaveCriticalSection( &((CThrListen *)p_Parent)->m_CS );
	
	return;
}
