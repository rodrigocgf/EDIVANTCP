#include "StdAfx.h"
#include "Download.h"
#include "erros.h"
#include "Config.h"

// No download o restartPt � relativo ao arquivo Origem
// o TamanhoDestino � o tam do arquivo no cliente
Download::Download(int idArquivo, CConfig *cfg) : Arquivo(cfg)
{
	existe = Existe(idArquivo, 'D');
}

Download::~Download()
{
}

int Download::getStatus
(
	const char *ModoOperacao, 
	int TamanhoDestino,
	const char *NomeDestino
)
{
   if(!existe) 
		return  EDI_MSG_ERRO_ARQUIVO_NAO_CADASTRADO;

   if(strcmp(ModoOperacao, "Nor") == 0) // Transmiss�o normal
   {
      if(TotalmenteTransmitido())
         return EDI_MSG_ARQ_TOTALMENTE_TRANSMITIDO;
   }
   restartPt = 0; // Default � retransmiss�o
   TamanhoArquivoOrigem = TamanhoDisco();

   if(TamanhoArquivoOrigem == -1) // o tamanho do arquivo na base de dados
	{
      return EDI_MSG_ERRO_TAMANHO_BD_DIFERE_FISICO; // est� incosistente com o do disco
	}
	else
	{                                            
		if(TamanhoDestino != this->TamanhoDestino) 
			restartPt = 0;
		if(restartPt == 0) 
			this->TamanhoDestino=0;  
		StatusArquivo = 'A'; // Em Recepcao
		strncpy(this->ModoOperacao, ModoOperacao, 4);
		strncpy(this->NomeDestino, NomeDestino, sizeof(this->NomeDestino));
		return salvarEstado();
	}
	
}

int Download::getTamanhoArquivoOrigem()
{
	return TamanhoArquivoOrigem;
}

int Download::getRestartPt()
{
	// O tamanho destino corresponde ao que
	// o EDI j� considera como gravado na m�quina
	// destino e ser� o ponto de restart para o
	// cliente
    return TamanhoDestino;
} 

int Download::getBloco(char *buff, int size, int *nofrb)
{
	int erro = 0, pos = 0, nRead;

   if(!existe)
		return EDI_MSG_ERRO_ARQUIVO_NAO_CADASTRADO;

   AbreArquivo(false, false);

   if(!Aberto())
		return EDI_MSG_ERRO_ABRINDO_ARQ_FISICO;

   PosicionaArquivo();
   while(erro == 0 && pos < size) 
   {
	   nRead = Read(&buff[pos], size - pos);
		if(nRead < 0) // nRead <0 --> Erro
		{
			erro = -1;	      
		} 
		else if(nRead > 0) 
		{
			pos += nRead;
		} 
		else // nRead == 0 ---> Terminou o download
		{
         break;
		}
	}
   *nofrb = pos;

	FechaArquivo(false);  

   if(!erro)
      return salvarEstado();
   else
      return erro;
}

int Download::termina()
{	
	
	StatusArquivo = 'C';
	return salvarEstado(); 
}
