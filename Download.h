#ifndef _DOWNLOAD_H_
#define _DOWNLOAD_H_

#include "StdAfx.h"
#include "Arquivo.h"
#include "Config.h"

class Download: public Arquivo
{
public:
	Download(int idArquivo, CConfig* cfg);
	virtual ~Download();

	int termina();
	int getBloco(char *buff, int size, int *nofrb);
	int getRestartPt();
	int getTamanhoArquivoOrigem();
	int getStatus
	(
			const char *ModoOperacao,
			int TamanhoDestino,
			const char *NomeDestino
	);

private:
	int existe;
	int TamanhoArquivoOrigem;
	bool TransmissaoNormal;
	bool Retransmissao;

};

#endif

