#include "StdAfx.h"
#include "Config.h"
#include "MySQL7.h"
#include "LogApiFunc.h"
#include "Util.h"

extern CLogApiFunc logapi;

CConfig::CConfig(const char *prgPath)
{
	LeConfig(prgPath);	

	MySQL7 *sql7 = new MySQL7(BdServer, BdUsuario, BdSenha, BdName);
	dirRaiz[0] = 0;
	
	if(sql7->ExecSQL("select DirRaiz from ConfiguracaoEdivan"))
		if(sql7->getRowCount() > 0)
			strncpy(dirRaiz, sql7->getString("DirRaiz"), _MAX_PATH);
		delete sql7;
}

CConfig::~CConfig()
{
}


// Nome do programa bin�rio com a extens�o .ini
// se o programa n�o tiver extens�o acrescenta .ini
// se tiver substitui por .ini (compatibilidade com
// execut�veis Unix)
void CConfig::NomeArquivoConfiguracao(const char *prgPath)
{
	char *p, *q;
	if(strlen(prgPath) == 0)
		return;
	strcpy(fnCfgName, prgPath);

	p = q = fnCfgName + strlen(fnCfgName) -1;
	while((p >= fnCfgName) && (*p != '/') &&
		    (*p != '\\') && (*p != ':') && (*p != '.')) p--;
	if(*p == '.') 
		strcpy(p + 1, "ini");
	else
		strcpy(q + 1, ".ini");
}

void CConfig::LeConfig(const char* prgPath)
{

	GetPrivateProfileString("RAPI","RapiServer","localhost",RapiServer , sizeof(RapiServer) , "EDIVANTCP.INI");
	RapiPort = GetPrivateProfileInt("RAPI","RapiPort",3002,"EDIVANTCP.INI");

	GetPrivateProfileString("MYSQL","BdServer"	,"localhost",BdServer	,sizeof(BdServer)	, "EDIVANTCP.INI");
	GetPrivateProfileString("MYSQL","BdName"	,"localhost",BdName		,sizeof(BdName)		, "EDIVANTCP.INI");
	GetPrivateProfileString("MYSQL","BdSenha"	," "		,BdSenha	,sizeof(BdSenha)	, "EDIVANTCP.INI");
	GetPrivateProfileString("MYSQL","BdUsuario"	," "		,BdUsuario	,sizeof(BdUsuario)	, "EDIVANTCP.INI");
	
    bPermiteCompressao = GetPrivateProfileInt( "EDI","Compressao"		, 1    , "EDIVANTCP.INI" ) == 1 ? true : false;
    bPermiteRestart    = GetPrivateProfileInt( "EDI","Restart"			, 1    , "EDIVANTCP.INI" ) == 1 ? true : false;
    RestartMinFileSize = GetPrivateProfileInt( "EDI","RestartMinSize"	, 10000, "EDIVANTCP.INI" );

	return;
}


const char* CConfig::Programa(const char *prg)		// [002] 
{

	int i, j;
	char ini_buf[202];
	FILE *ini_fd;
	char *buf;

	strcpy(NomePrograma, "");

	if((ini_fd = fopen(fnCfgName, "r")) == NULL)
		return false;

	/* Leitura propriamente dita. */
	j = 0;
	while(fgets(ini_buf, sizeof(ini_buf), ini_fd))
	{
		buf = ini_buf;
		while(*buf == ' ' || *buf == 0x9) /* branco ou tab */
			buf++;
		i = strlen(buf);
		if(i < 7 || *buf == ';')
			continue;
		if(*(buf + i - 1) == 0xa)
			*(buf + i - 1) = 0;
		for(i = 6; ;i++) /* acha  o fim do par�metro */
			if(*(buf + i) == 0 || *(buf + i) == ';') /* terminou ou � coment�rio ? */
			{
				i--; /* volta para antes do null ou # */
				break;
			}
		while(i > 0 && (*(buf + i) == ' ' || *(buf + i) == 0x9))
			i--;
		*(buf + i + 1) = 0; /*  fecha o par�metro */

		if(strncmp(buf, prg, strlen(prg)) == 0)
		{
			if(strlen(buf + strlen(prg) + 1) < sizeof(NomePrograma) - 1)
			{
				strcpy(NomePrograma,buf+strlen(prg)+1);
			}
			continue;
		}
	}
	fclose(ini_fd);
	return NomePrograma;
}

const char *CConfig::diretorioRaiz()
{
	return dirRaiz;
}
