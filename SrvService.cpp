// SrvService.cpp: implementation of the CSrvService class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EDIVANTCP.h"
#include "SrvService.h"
#include "LogApiFunc.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CEDIVANTCPApp theApp;
extern CSrvService theSrv;
extern CLogApiFunc logapi;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSrvService::CSrvService():CNTService("EDIVANTCP v1.0")
{
}

CSrvService::~CSrvService()
{
}

BOOL CSrvService::OnInit()
{
	BOOL rc;
	strcpy ( logapi.m_file, "EDIVANTCP");
	theSrv.m_bService = TRUE;
	logapi.logerr("CSrvService: OnInit()");
	//rc = theApp.InitInstance();
	rc = theApp.IniciaAplicacao();

	return rc;
}

void CSrvService::Run()
{
   logapi.logerr("CSrvService: Run()");
   SetStatus(SERVICE_RUNNING);
   AfxGetApp()->Run();
}

void CSrvService::OnStop()
{
   logapi.logerr("CSrvService: OnStop()");
   SetStatus(SERVICE_STOP_PENDING);
   theApp.FinalizaAplicacao();
   SetStatus(SERVICE_STOPPED);
}
