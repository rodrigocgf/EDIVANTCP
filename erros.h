/********************************************************************************\
                                7COMm Informatica
    Projeto.....: EDI7Web
    Modulo......: erros.h
    Descricao...: Lista codigos de erro 
    Referencias.: Baseado em    pcalog.h (Edi7Web BCN)
    Autor.......: Jos� Eug�nio
    Atualizacoes: 
						[MZ001]  -  Marcelo Zanin
										07/04/2003
										Inclusao da Msg de erro: 2514

						[002]    -  Cristiane Cohen
										02/06/2003
										Inclusao da Msg de erro: 2515

\********************************************************************************/

/********************************************************************************/
/* Defini��o de c�digos de erro                                                 */
/*                                                                              */
/* As mensagens com codigo 4xx e 7xx foram criadas especificamente para a       */
/* vers�o Web. Todos os outros c�digos s�o das vers�es anteriores do EDI7.      */
/********************************************************************************/
#define EDI_MSG_OK                                            0
#define EDI_MSG_ERRO_ABRINDO_ARQ_FISICO                    2501

// Primeira vez que tenta acessar cadastro
#define EDI_MSG_ERRO_ARQUIVO_NAO_CADASTRADO                2502

// Ja estava com um id de arquivo v�lido que n�o re-encontrou
#define EDI_MSG_ERRO_NO_CADASTRO_ARQUIVO                   2503

#define EDI_MSG_ERRO_NA_DESCOMPACTACAO_ZLIB				   2504
#define EDI_MSG_ERRO_NA_COMPACTACAO_ZLIB				   2505
#define EDI_MSG_ERRO_CRIANDO_ARQUIVO				       2506
#define EDI_MSG_ERRO_PRG_CONSISTENCIA_NAO_ENCONTRADO       2507
#define EDI_MSG_ERRO_GRAVANDO_ARQUIVO				       2508
#define EDI_MSG_ERRO_CLIENTE_DOMINIO                       2509
#define EDI_MSG_ERRO_COMANDO_SQL                           2510
#define EDI_MSG_ERRO_TAMANHO_BD_DIFERE_FISICO              2511
#define EDI_MSG_ERRO_CLIENTE_PRODUTO_FORMATO               2512

/*-------------------------------------------------------------------*/
// Modulo AutBank - Extract240AutBank
#define EDI_MSG_ERRO_PRG_EXTRATO_NAO_ENCONTRADO			   2513	
/*-------------------------------------------------------------------*/

#define EDI_MSG_ERRO_CLIENTE_DESTINO_NAO_ENCONTRADO		   2514			// [MZ001]

#define EDI_MSG_ERRO_PRG_NAO_ENCONTRADO					   2515			// [002]

#define EDI_MSG_ARQ_TOTALMENTE_TRANSMITIDO				   2002
#define EDI_MSG_ARQ_JA_PROCESSADO						   2003
#define EDI_MSG_TRANSMISSAO_CANCELADA           		   2005

// RAPI
#define EDI_MSG_WSASTARTUP								   2601
#define EDI_MSG_GETHOSTBYNAME							   2602
#define EDI_MSG_SOCKET									   2603
#define EDI_MSG_CONNECT									   2604

#define EDI_MSG_REQUISICAO_HTTP_INVALIDA                   2701
#define EDI_MSG_VERSAO_INVALIDA_APPLET                     2703

// Conex�o com Banco de dados
#define EDI_MSG_ERRO_CONECTANDO_BD                         2801

/* Dados a inserir na tabela de Ocorrencia
insert into Ocorrencia values 
('02501','Erro abrindo arquivo no Host.'),            
('02502','Arquivo n�o existe no cadastro.'),                                        
('02503','Identificador de arquivo n�o existe no cadastro.'),                       
('02504','Erro descompactando bloco do arquivo.'),                                  
('02505','Erro compactando bloco do arquivo.'),                                     
('02506','Erro criando arquivo.'),                                                  
('02507','Programa de consist�ncia n�o encontrado.'),                               
('02508','Erro gravando arquivo.'),                                                 
('02509','Cliente n�o encontrado no dom�nio.'),                                     
('02510','Erro na execu��o de comando sql.'),                                       
('02511','Arquivo com erros no Host.'),                                             
('02512','Produto/Formato n�o cadastrado'),
('02514','Cliente Destino n�o encontrado'),					// [MZ001]
('02002','Arquivo j� foi transmitido.'),                                            
('02003','Arquivo j� foi processado.'),                                             
('02005','Transmissao cancelada.'),                                                 
('02601','Erro inicializando da api Remota (WSA).'),                                
('02602','Servidor da api Remota n�o encontrado.'),                                 
('02603','Erro iniciando a comunica��o com a api Remota.'),                         
('02604','Erro de conex�o com a api Remota.'),                                      
('02701','Requisi��o HTTP inv�lida.'),                                              
('02702','Comando inv�lido.'),                                                      
('02703','Vers�o inv�lida da applet.')                                    

// Modulo AutBank - Extract240AutBank - Inserir na tabela Ocorrencia 
('02513','Programa de consist�ncia n�o encontrado.'),

*/
