#include "StdAfx.h"
#include "Socket32.h"
#include "Util.h"
#include "LogApiFunc.h"

extern CLogApiFunc logapi;

///  Constructor do objeto
CTcpSocket::CTcpSocket()
{
	WSASetLastError(0);
	mError = 0;
	mSocket = NULL;
	mIsSocketOk = FALSE;
}

///  Destructor do objeto
CTcpSocket::~CTcpSocket()
{
//	if(bSocketOk)
//		Close();
}

///  Retorna o c�digo do �ltimo erro
int CTcpSocket::ObtemUltimoErro()
{		
	return mError;
}

///  Retorna o handle do socket
CTcpSocket::operator SOCKET()
{
   return mSocket;
}

///  Atacha um socket ao objeto
BOOL CTcpSocket::Attach(SOCKET nSocket)
{
   mSocket = nSocket;
   mIsSocketOk = TRUE;
   return TRUE;
}

///  Cria um novo socket e o prepara para
///  ser um servidor 
///
///  Parametros: 
///     nPort - Numero da porta (para atender as novas conexoes)
///     nType - Tipo do socket (UDP/TCP)
BOOL CTcpSocket::Create(UINT nPort, int nType)
{  
	mIsSocketOk = FALSE;
   struct sockaddr_in LocalAddress;
   
   mError = 0;
   //  Cria o socket
   if((mSocket = socket(AF_INET, nType, 0)) == INVALID_SOCKET){
      mError = WSAGetLastError();
      return FALSE;
   }
   mIsSocketOk = TRUE;
   
   memset(&LocalAddress, 0, sizeof(LocalAddress));
   LocalAddress.sin_family = AF_INET;
   LocalAddress.sin_addr.s_addr = htonl(INADDR_ANY);
   LocalAddress.sin_port = htons(nPort);

   //  Associa o socket criado a uma porta local
   if(bind(mSocket, (LPSOCKADDR) &LocalAddress, sizeof(LocalAddress)) == SOCKET_ERROR)
	{
      mError = WSAGetLastError();
      return FALSE;
   }
   return TRUE;
}

///  Cria um novo socket e o prepara para
///  ser um cliente
///
///  Parametros: 
///     nPort - Numero da porta (para atender as novas conexoes)
///     nType - Tipo do socket (UDP/TCP)
BOOL CTcpSocket::CreateToConnect(int nType)
{
   mIsSocketOk = FALSE;
   
   mError = 0;
   //  Cria o socket
   if((mSocket = socket(AF_INET, nType, 0)) == INVALID_SOCKET)
	{
      mError = WSAGetLastError();
      return FALSE;
   }              
   mIsSocketOk = TRUE;
   return TRUE;
}

///  Configura o socket
void CTcpSocket::Config()
{
   struct sockaddr_in Addr;
   int AddrLen = sizeof(Addr);
   LPSTR pAddress;

   // Configura o socket para trabalhar em modo blocado
   DWORD NonBlockingMode = FALSE;
   IOCtl(FIONBIO, &NonBlockingMode);

   //  Pega o endere�o IP do cliente
   memset(mClientAddress, 0, sizeof(mClientAddress));
   getpeername(mSocket, (LPSOCKADDR) &Addr, &AddrLen);
   
   if((pAddress = inet_ntoa(Addr.sin_addr)) != NULL)
	{
      if(strlen(pAddress) < sizeof(mClientAddress))
         strcpy(mClientAddress, pAddress);
      else
         memcpy(mClientAddress, pAddress, sizeof(mClientAddress) - 1);
   }
	else
	{
      strcpy(mClientAddress, "DESCONHECIDO");
   }
   //  Pega o endere�o IP do servidor
   memset(mHostAddress, 0, sizeof(mHostAddress));
   getsockname(mSocket, (LPSOCKADDR) &Addr, &AddrLen);

   if((pAddress = inet_ntoa(Addr.sin_addr)) != NULL)
	{
      if(strlen(pAddress) < sizeof(mHostAddress))
         strcpy(mHostAddress, pAddress);
      else
         memcpy(mHostAddress, pAddress, sizeof(mHostAddress) - 1);
   }
	else
	{
      strcpy(mHostAddress, "DESCONHECIDO");
   }
}

///  Verifica se pode enviar dados pela rede
///
///  Parametros: 
///     nTimeOut - Tempo de espera (em segundos)
int CTcpSocket::WaitToSend(int nTimeOut)
{
	FD_SET fdsSend;
	FD_SET fdsExcept;
	TIMEVAL tvSend = {nTimeOut, 0};
	int nSockets;
	
	mError = 0;
	
	FD_ZERO(&fdsSend);
	FD_ZERO(&fdsExcept);
	FD_SET(mSocket, &fdsSend);
	FD_SET(mSocket, &fdsExcept);
	
	//  Verifica se pode enviar dados pela rede
	nSockets = select(NULL, NULL, &fdsSend, &fdsExcept, &tvSend);
	
	//  Saiu por timeout
	if(nSockets == 0) {		
		mError = WSAETIMEDOUT;		
		return 0;
	}
	
	//  Verifica se ocorreu algum erro
	if( (nSockets == SOCKET_ERROR) || FD_ISSET(mSocket, &fdsExcept) ) {
		mError = WSAGetLastError();
		//if((mError = WSAGetLastError()) == 0)
		//	mError = WSAECONNRESET;
		return 0;
	}
	return 1;
}

///  Verifica se tem alguma coisa para receber
///
///  Parametros: 
///     nTimeOut - Tempo de espera (em segundos)
int CTcpSocket::WaitToReceive(int nTimeOut)
{
	FD_SET fdsReceive;
	FD_SET fdsExcept;
	TIMEVAL tvReceive = {nTimeOut, 0};
	int nSockets;
	
	mError = 0;
	
	FD_ZERO(&fdsReceive);
	FD_ZERO(&fdsExcept);
	FD_SET(mSocket, &fdsReceive);
	FD_SET(mSocket, &fdsExcept);
	
	//  Verifica se tem alguma coisa para receber
	nSockets = select(NULL, &fdsReceive, NULL, &fdsExcept, &tvReceive);
	
	//  Saiu por timeout
	if (nSockets == 0) {
		mError = WSAETIMEDOUT;		
		return 0;
	}
	
	
	//  Verifica se ocorreu algum erro
	if( (nSockets == SOCKET_ERROR) || FD_ISSET(mSocket, &fdsExcept) ) {		
		mError = WSAGetLastError();		
		return 0;
	}

	return 1;
}

///  Envia dados pela rede
///
///  Parametros: 
///     Buffer  - Area de dados a ser enviada
///     Tamanho - Tamanho da area de dados
///
///
int CTcpSocket::Send(const char *Buffer, int Tamanho)
{
	int BytesEnviados;
	int TotalEnviado = 0;
	DWORD dw_res;
	
	mError = 0;
	
	while(TotalEnviado < Tamanho)
	{
		if( !WaitToSend(TIMEOUT_SENDRECEIVE) ) {
			if(mError)  return 0;
			else		continue;
		}

		BytesEnviados = send(mSocket, &Buffer[TotalEnviado], 
			Tamanho - TotalEnviado, 0);

		if ( BytesEnviados == SOCKET_ERROR ) {
			dw_res = WSAGetLastError();
			if ( dw_res != WSAEWOULDBLOCK ) {
				mError = dw_res;
				return BytesEnviados;
			}
		}

		/*
		dw_res = WSAGetLastError();
		if( (BytesEnviados == SOCKET_ERROR) && (dw_res != WSAEWOULDBLOCK) ){			
			mError = WSAGetLastError();
			return BytesEnviados;
		}
		*/

		if(!BytesEnviados) 
			continue;

		TotalEnviado += BytesEnviados;
	}
	return TotalEnviado;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///
///  Recebe dados da rede
///
///  Parametros: 
///     Buffer  - Area para receber os dados
///     Tamanho - Quantidade de bytes a receber
///
///
int CTcpSocket::Receive(char *Buffer, int Tamanho)
{
	int BytesRecebidos;
	int TotalRecebido = 0;
	DWORD dw_res;
	
	mError = 0;
	
	while(TotalRecebido < Tamanho)
	{
		if( !WaitToReceive(TIMEOUT_SENDRECEIVE) ) {
			if(mError)	return 0;
			else		continue;
		}
		
		BytesRecebidos = recv(	mSocket, &Buffer[TotalRecebido],	\
								(Tamanho - TotalRecebido) ,			\
								0);
	
		/*
		dw_res = WSAGetLastError();		
		if( (BytesRecebidos == SOCKET_ERROR) && (dw_res != WSAEWOULDBLOCK) ) {			
			mError = WSAGetLastError();
			return BytesRecebidos;
		}
		*/
		if ( BytesRecebidos == SOCKET_ERROR ) {
			logapi.logtrc("CTcpSocket::Receive >> SOCKET_ERROR no socket %d.\r\n", mSocket);
			dw_res = WSAGetLastError();
			if ( dw_res != WSAEWOULDBLOCK ) {
				mError = dw_res;
				return BytesRecebidos;
			}
		}

		// N�o recebeu nada, a outra ponta desconectou
		//if(!BytesRecebidos)
		//{
		//	mError = ERR_INT_DESCONEXAO;
		//	return 0;
		//}
		TotalRecebido += BytesRecebidos;
	}
	return TotalRecebido;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///
///  Recebe o endere�o do cliente que acabou de se conectar
///  tamb�m recebe um novo handle para o socket conectado
///
///  Parametros: 
///     Address - Endere�o do cliente
///
///
SOCKET CTcpSocket::Accept(LPSTR Address)
{
   LPSTR wAddress;
   struct sockaddr_in Addr;
   int AddrLen = sizeof(Addr);

   mError = 0;

   SOCKET newSock = accept(mSocket, (LPSOCKADDR)&Addr, &AddrLen);

   if (newSock == INVALID_SOCKET) 
      mError = WSAGetLastError();

   if ((wAddress = inet_ntoa(Addr.sin_addr)) != NULL)
      strcpy(Address, wAddress);

   return newSock;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///
///  Configura o socket para aceitar novas conexoes
///
///  Parametros: 
///     nBackLog - Tamanho da fila de conex�es pendentes
///
///
BOOL CTcpSocket::Listen(int nBackLog)
{
   if (listen(mSocket, nBackLog) == SOCKET_ERROR)
   {
      mError = WSAGetLastError();
      return FALSE;
   } 
   else
      mError = 0;

   return TRUE;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///
///  Recebe o endere�o do cliente que acabou de se conectar
///  tamb�m recebe um novo handle para o socket conectado
///
///  Parametros: 
///     sAddress - Endere�o do servidor 
///     nPort    - Porta para a conex�o
///
///
BOOL CTcpSocket::Connect(LPCSTR sAddress, UINT nPort){
   LPHOSTENT Host;
   struct sockaddr_in Addr;

   memset( (void*)&Addr, 0, sizeof( Addr ) );

   Addr.sin_family = AF_INET;
   Addr.sin_port = htons(nPort);

   int AddrLen = sizeof(Addr);

/*
   A ordem de procura foi invertida pois a funcao gethostbyname
   pode ser lenta dependendo da configuracao do dns. Assim,
   primeiro usamos a funcao inet_addr que e' rapida e so' depois
   tentamos o gethostbyname   JT 22.11.98

   if ((Host = gethostbyname(sAddress)) != NULL)
      memcpy((void*)&Addr.sin_addr, (void*)Host->h_addr, Host->h_length);
   else
      Addr.sin_addr.s_addr = inet_addr(sAddress);
*/
   if ((Addr.sin_addr.s_addr = inet_addr(sAddress)) == INADDR_NONE)
      if ((Host = gethostbyname(sAddress)) != NULL)
         memcpy((void*)&Addr.sin_addr, (void*)Host->h_addr, Host->h_length);


   if (connect(mSocket, (LPSOCKADDR)&Addr, sizeof(Addr)) == SOCKET_ERROR){
      mError = WSAGetLastError();
      return FALSE;
   } else
      mError = 0;

   return TRUE;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///
///  Configura o socket
///
///
BOOL CTcpSocket::IOCtl(long lCommand, DWORD* lpArgument){
   if (ioctlsocket(mSocket, lCommand, lpArgument) == SOCKET_ERROR){
      mError = WSAGetLastError();
      return FALSE;
   } else
      mError = 0;

   return TRUE;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///
///  Configura o socket
///
///
BOOL CTcpSocket::SetSockOpt(int nOption, const void* lpValue, int nLen, int nLevel){
   if (setsockopt(mSocket, nLevel, nOption, (const char*)lpValue, nLen) == SOCKET_ERROR){  
      mError = WSAGetLastError();
      return FALSE;
   } else
      mError = 0;

   return TRUE;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///
///  Obtem a configura��o do socket
///
///
BOOL CTcpSocket::GetSockOpt(int nOption, void* lpValue, int* nLen, int nLevel){
   if (getsockopt(mSocket, nLevel, nOption, (char*)lpValue, nLen) == SOCKET_ERROR){  
      mError = WSAGetLastError();
      return FALSE;
   }
   else
      mError = 0;

   return TRUE;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///
///  Encerra a conex�o
///
///
void CTcpSocket::Close(){
   mIsSocketOk = FALSE;
   //shutdown(mSocket, SD_SEND);
   closesocket(mSocket);
   mSocket = INVALID_SOCKET;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///
///  Encerra a conex�o
///
///
BOOL CTcpSocket::IsSocketConnected(){
   return mIsSocketOk;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///
///  Retorna o handle do socket
///
///
int CTcpSocket::ObtemNumeroSocket(){
   return mSocket;
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
///
///  Recebe Arquivo
///
///
BOOL CTcpSocket::RecebeArquivo(LPTSTR lpArqDestino, LONG lTamArq){
	HANDLE hArquivo;
	HANDLE hEncerraPrograma;
	LONG lTamanhoBloco = 0;
	BYTE sBuffer[5120];
	DWORD BytesGravados = 0;
	LONG lBytesRecebidos = 0;
	SBlocoArquivo BlocoArquivo;

	// Abre o evento para monitorar o fim do programa
	hEncerraPrograma = OpenEvent(EVENT_ALL_ACCESS,
								 TRUE, EVENT_FINALIZA);

	if (hEncerraPrograma == NULL){
		return FALSE;
	}

	//Cria Arquivo 
	hArquivo = CreateFile(lpArqDestino, 
                          GENERIC_WRITE, 
                          NULL, NULL, 
                          CREATE_ALWAYS, 
                          FILE_ATTRIBUTE_NORMAL, NULL);

	if (hArquivo == INVALID_HANDLE_VALUE){
		CloseHandle(hArquivo);
		CloseHandle(hEncerraPrograma);
		return FALSE;
	}    

	while(lBytesRecebidos <= lTamArq){
		ProcessMessages();

		// Verifica se terminou a execu��o do programa
		if(WaitForSingleObject(hEncerraPrograma, 0) == WAIT_OBJECT_0){
			CloseHandle(hArquivo);			// Graziela - 24/5/2000 - Retornava FALSE sem fechar Handle
			CloseHandle(hEncerraPrograma);	// Graziela - 24/5/2000 - Retornava FALSE sem fechar Handle
			return FALSE;
		}	

		// Recebe o tamanho do bloco trafegado
		if(Receive((LPTSTR) BlocoArquivo.Tamanho, 15) <= 0){
			
			CloseHandle(hArquivo);
			CloseHandle(hEncerraPrograma);
			return FALSE;
		}

		// Avisa que a operacao foi bem sucedida
		Send(SUCESSO_NA_OPERACAO, 3);
   
		lTamanhoBloco = atol((LPTSTR)&BlocoArquivo.Tamanho[0]);

		// Recebe um bloco do arquivo
		memset((LPTSTR)&sBuffer[0], '\0', sizeof(sBuffer));

		if(Receive((LPTSTR)&BlocoArquivo.sBuffer[0], lTamanhoBloco) <= 0){
			
			CloseHandle(hArquivo);
			CloseHandle(hEncerraPrograma);
			return FALSE;
		}

		// Avisa que a operacao foi bem sucedida
		Send(SUCESSO_NA_OPERACAO, 3);

		// Grava dados no arquivo
		if(!WriteFile(hArquivo,
					   (LPTSTR)&BlocoArquivo.sBuffer[0], 
					   lTamanhoBloco,
					   &BytesGravados, 
					   NULL)){
			Send(ERRO_NA_OPERACAO, 3);
			CloseHandle(hArquivo);
			CloseHandle(hEncerraPrograma);
			return FALSE;
		}
	}

	CloseHandle(hArquivo);
	CloseHandle(hEncerraPrograma);

	return TRUE;
}

int CTcpSocket::SendCrypt(const char * Buffer, int Tamanho)
{
   char *BufferCrypt;
   int Result;

   mError = 0;
   if ((BufferCrypt=(char *)malloc(Tamanho)) == NULL)
   {
      mError = ERR_INT_MALLOC;
      return 0;
   }
   Encripta(BufferCrypt,Buffer,Tamanho);
   Result = Send(BufferCrypt,Tamanho);
   free(BufferCrypt);
   return Result;
}

int CTcpSocket::ReceiveCrypt(char *Buffer, int Tamanho)
{
	char *BufferCrypt;
	int Result;
	
	mError = 0;
	if((BufferCrypt = (char *) malloc(Tamanho)) == NULL)
	{
		mError = ERR_INT_MALLOC;
		return 0;
	}
	if((Result = Receive(BufferCrypt, Tamanho)) == 0)
	{
		free(BufferCrypt);
		return Result;
	}
	Encripta(Buffer, BufferCrypt, Tamanho);
	free(BufferCrypt);
	return Result;
}
